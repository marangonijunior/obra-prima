import {Component, Injector} from '@angular/core';
import {ActionSheetController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {GenericDetailsPage} from "../generic-details/generic-details";
import {CivilWorkOrderProvider} from "../../providers/civil-work-order/civil-work-order";
import {PouchdbSync} from "../../providers/pouchdb/pouchdb-sync";
import {CivilWorkOrderItensProvider} from "../../providers/civil-work-order-itens/civil-work-order-itens";

/**
 * Generated class for the CivilWorkOrderDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-order-details',
    templateUrl: 'civil-work-order-details.html',
})
export class CivilWorkOrderDetailsPage extends GenericDetailsPage {

    item;
    isLoaded = false;
    // used for draft state
    addCallbackFunction;

    constructor(public navCtrl: NavController,
                private civilWorkOrderProvider: CivilWorkOrderProvider,
                private civilWorkOrderItensProvider: CivilWorkOrderItensProvider,
                protected injector: Injector,
                public navParams: NavParams) {
        super(navCtrl, navParams, injector);
        this.editPage = "CivilWorkOrderAddEditPage";
        this.addCallbackFunction = null;
    }


    ionViewDidEnter() {
        if (!this.addCallbackFunction) {
            this.item = this.navParams.get("item");
        }

        this.getData();
    }

    edit() {
        if (this.item.situacao.id_situacao == "draftApp") {
            this.addCallbackFunction = (item) => {
                this.item = item;
            }

            this.navCtrl.push(this.editPage, {item: this.item, addCallbackFunction: this.addCallbackFunction});
        }
        else {
            this.navCtrl.push(this.editPage, {item: this.item});
        }

    }

    delete() {


        this.civilWorkOrderProvider.remove(this.item, true).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                this.deleteItemFromDb({
                    obj: this.item,
                    type: "civilWorkOrder"
                }).then((data) => {
                    this.requestHelpersProvider.showMessage("Operação realizada com sucesso");
                    this.navCtrl.pop();
                })

            }
        }, (error) => {
            this.deleteOff();
        })
    }

    getData() {
        this.getDataFromLocalDatabase(true);
    }

    /// Salvar anexos.


    getDataFromApi(showLoading) {
        this.civilWorkOrderProvider.getItems(this.item, showLoading).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                var item = {type: "civilWorkOrderItems", obj: data.objects};
                this.pouchControll.bulkDoc(item).then((data) => {
                    this.checkDeletedItens("civilWorkOrderItems", "id_item", this.civilWorkOrderItensProvider);
                    this.getDataFromLocalDatabase(false);
                })
            }
            this.completeRefresh();

        }, (error) => {
            this.completeRefresh();
            this.getDataFromLocalDatabase(false);
        })
    }

    checkDeletedItens(type, idField, provider) {

        this.pouchControll.pouchdbOff.customFind({
            type: 'civilWorkOrderItems',
            selector: {}
        }).then((data) => {

            let idList = data.docs.map((item) => {
                return item[idField];
            })

            provider.getDeleted(idList, false).subscribe((data: any) => {
                if (this.requestHelpersProvider.successAndHasObject(data)) {


                    var item = {
                        type: type,
                        selector: {$or: []}
                    };

                    data.object.forEach((id, index) => {
                        item.selector.$or.push({id_diaDia: id});
                    })


                    return this.pouchControll.pouchdbOff.customFind(item).then((data) => {
                        if (data.docs) {
                            data = data.docs.map((item, index) => {
                                item._deleted = true;
                                return item;
                            })

                            this.pouchControll.pouchdbOff.bulkDoc({
                                type: type,
                                obj: data
                            }).then(() => {
                                this.getDataFromLocalDatabase(false);
                            })
                        }

                        console.log(data);
                    });

                }
                this.completeRefresh();

            }, (error) => {
            })

        });

    }


    updateItensCount() {
        this.item.itens = this.item.itensArray ? this.item.itensArray.length : 0;
        this.pouchControll.bulkDoc({
            obj: [this.item],
            type: "civilWorkOrder"
        }).then((data) => {
        })
    }

    getItems(fetchFromApi?) {
        let item = {
            type: "civilWorkOrderItems",
            find: {
                id_solicitacao: this.item.id_solicitacao ? this.item.id_solicitacao : this.item._id,
                deleted: {'$ne': true}
            }
        };
        this.pouchdbOff.getFind(item).then((data) => {
            // console.log("civilWorkOrderItems", data)
            let objects = data.docs.map(row => {
                return row;
            });

            this.item.itensArray = objects;
            this.isLoaded = true;

            // console.log("get data from localDb bring " + this.list.length + " results");

            if (objects == null || objects.length == 0) {
                console.log("no data from localdb saved, fetching from API");
            }

            // after getting itens from localDB it will try to fetch from api (even if it has data on localDb)
            // showLoading = will show loading spinner if theres no data on db (its probably the first time the user is using the app), else will fetch in background (no loading spinner).
            var showLoading = (objects == null || objects.length == 0) ? true : false;

            if (fetchFromApi) {
                this.getDataFromApi(showLoading);
            }
            else {
                this.updateItensCount();
            }


        }).catch(error => {

        })
    }

    getDataFromLocalDatabase(fetchFromApi?) {
        let item = {
            type: "civilWorkOrder",
            find: {}
        };

        item.find = this.item.id_solicitacao ? {id_solicitacao: this.item.id_solicitacao} : {_id: this.item._id}

        this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Order getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            return objects;


        }).then((objects) => {
            if (objects && objects.length > 0) {
                this.item = objects[0];
                return this.getItems(fetchFromApi);
            }
        }).catch(error => {

        })
    }

    deleteOff() {

        this.item.deleted = true;

        return this.pouchControll.insert({
            obj: this.item,
            type: "civilWorkOrder"
        }).then((data) => {
            if (data && data.length) {
                this.item.id_diaDia = this.item.id_diaDia ? this.item.id_diaDia : data[0].id;

                this.requestHelpersProvider.showMessage("Operação realizada com sucesso");
                this.navCtrl.pop();
            }
        })


    }

}
