import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CivilWorkOrderDetailsPage } from './civil-work-order-details';
import {ComponentsModule} from "../../components/components.module";
import {DirectivesModule} from "../../directives/directives.module";

@NgModule({
  declarations: [
    CivilWorkOrderDetailsPage,
  ],
  imports: [
    ComponentsModule,
    DirectivesModule,
    IonicPageModule.forChild(CivilWorkOrderDetailsPage),
  ],
})
export class CivilWorkOrderDetailsPageModule {}
