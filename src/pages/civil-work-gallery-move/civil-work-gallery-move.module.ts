import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CivilWorkGalleryMovePage } from './civil-work-gallery-move';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    CivilWorkGalleryMovePage,
  ],
  imports: [
    IonicPageModule.forChild(CivilWorkGalleryMovePage),
    ComponentsModule
  ],
})
export class CivilWorkGalleryMovePageModule {}
