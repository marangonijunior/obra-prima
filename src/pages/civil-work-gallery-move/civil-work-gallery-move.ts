import {Component, Injector, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FileManagerComponent} from "../../components/file-manager/file-manager";
import {GenericListPage} from "../generic-list/generic-list";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {CivilWorkGalleryProvider} from "../../providers/civil-work-gallery/civil-work-gallery";
import * as _ from 'underscore';

/**
 * Generated class for the CivilWorkGalleryMovePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-gallery-move',
    templateUrl: 'civil-work-gallery-move.html',
})
export class CivilWorkGalleryMovePage extends GenericListPage {

    @ViewChild('fileManager') fileManager: FileManagerComponent;

    folder: any = {};
    selected;

    constructor(public navCtrl: NavController,
                public civilWorkGalleryProvider: CivilWorkGalleryProvider,
                public requestHelpersProvider: RequestHelpersProvider,
                protected injector: Injector,
                public navParams: NavParams) {
        super(navCtrl, navParams, injector);
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
        this.selected = this.navParams.get("selected");
    }

    getData() {
        this.getDataFromLocalDatabase(false);
        //this.getGalleryTeste();
    }

    getDataFromApiAndSync(showLoading) {
        this.civilWorkGalleryProvider.getAllByCivilWork(this.civilWorkProvider.selected, showLoading).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                data.objects.forEach((item, index) => {
                    data.objects[index].root = true;
                });

                data.objects = this.pouchControll.getGalleriesRecursive(data.objects);


                // let item = {
                //     type: "civilWorkGallery",
                //     find: {id_galeria: this.folder.id_galeria}
                // };
                this.saveOffline("civilWorkGallery", data.objects).then((data) => {
                    this.getDataFromLocalDatabase(false);
                })
            }

            this.completeRefresh();

        }, (error) => {
            // se der erro de conexão ou n tiver internet cai aqui. Chama a função pra buscar os dados no banco de dados.
            this.getDataFromLocalDatabase(false);
            this.completeRefresh();
        })
    }

    getDataFromLocalDatabase(fetchFromApi?) {
        console.log("getDataFromLocalDatabase")
        let item = {
            type: "civilWorkGallery",
            find: {id_obra: this.civilWorkProvider.selected.id_obra, pastaMae: {$exists: false}, deleted: {'$ne': true}}
        };
        this.pouchdbOff.getFind(item).then((data) => {
            // console.log(data)
            let objects = data.docs.map(row => {
                return row;
            });


            var folder = {
                galeriasFilhas: objects,
                nome: "Raiz",
                rootFolder: true,
                id_empresa: this.userLocalProvider.company.id_empresa,
                id_obra: this.civilWorkProvider.selected.id_obra
            }

            this.folder = folder;

            this.fileManager.showFiles = true;
            this.fileManager.showFolders = true;

            // console.log("get data from localDb bring " + this.list.length + " results");

            if (objects == null || objects.length == 0) {
                console.log("no data from localdb saved, fetching from API");
            }

            console.log(objects);

            // after getting itens from localDB it will try to fetch from api (even if it has data on localDb)
            // showLoading = will show loading spinner if theres no data on db (its probably the first time the user is using the app), else will fetch in background (no loading spinner).
            var showLoading = (objects == null || objects.length == 0) ? true : false;

            if (fetchFromApi) {
                this.getDataFromApiAndSync(showLoading);
            }


        }).catch(error => {
            console.log(error);
        })
    }

    move() {
        if (this.selected && this.selected.length) {

            var sameFolderWithGalleryId = _.findWhere(this.selected,{id_galeria: this.fileManager.folder.id_galeria ? this.fileManager.folder.id_galeria : this.fileManager.folder._id});
            var sameFolderWithLocalId = _.findWhere(this.selected,{_id: this.fileManager.folder.id_galeria ? this.fileManager.folder.id_galeria : this.fileManager.folder._id});

            if (sameFolderWithGalleryId || sameFolderWithLocalId) {
                this.requestHelpersProvider.showMessage("Não é permitido mover uma pasta para dentro dela mesma");
                return;
            }


            var index = 0;
            var onDone = (file) => {
                if (this.selected.length - 1 > index) {
                    index = index + 1;
                    var file = this.selected[index];
                    if (this.fileManager.folder.id_galeria || this.fileManager.folder._id) {
                        file.pastaMae = this.fileManager.folder.id_galeria ? this.fileManager.folder.id_galeria : this.fileManager.folder._id;
                    }

                    this.fileManager.addFile(file, onDone);
                }
                else {
                    this.requestHelpersProvider.showMessage("Operação realizada com sucesso.");
                    this.navCtrl.pop();
                }
            }

            var file = this.selected[index];
            if (this.fileManager.folder.id_galeria || this.fileManager.folder._id) {
                file.pastaMae = this.fileManager.folder.id_galeria ? this.fileManager.folder.id_galeria : this.fileManager.folder._id;
            }
            console.log(file);
            this.fileManager.addFile(file, onDone);

        }
    }

    cancel() {
        this.navCtrl.pop();
    }

}
