import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the CivilWorkGalleryFiltersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-civil-work-gallery-filters',
  templateUrl: 'civil-work-gallery-filters.html',
})
export class CivilWorkGalleryFiltersPage {

  filter = {
      name: null,
      folders: true,
      files: true,
      visible: true,
      invisible: true,
  };

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              public navParams: NavParams) {

      if (this.viewCtrl.data.searchObject){
          this.filter = this.viewCtrl.data.searchObject;
      }


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CivilWorkGalleryFiltersPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  doFilter() {
    console.log(this.filter);
    this.viewCtrl.dismiss({filter: this.filter});
  }

  returnFilter(filter:any){
    console.log(filter)

  }

}
