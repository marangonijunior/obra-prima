import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CivilWorkGalleryFiltersPage} from './civil-work-gallery-filters';
import {DisplayFlexDirective} from "../../directives/display-flex/display-flex";
import {DirectivesModule} from "../../directives/directives.module";
import {FormsModule} from "@angular/forms";

@NgModule({
    declarations: [
        CivilWorkGalleryFiltersPage,
    ],
    imports: [
        IonicPageModule.forChild(CivilWorkGalleryFiltersPage),
        FormsModule,
        DirectivesModule
    ],
})
export class CivilWorkGalleryFiltersPageModule {
}
