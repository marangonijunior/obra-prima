import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CivilWorkOrderListPage} from './civil-work-order-list';
import {ComponentsModule} from "../../components/components.module";
import {DirectivesModule} from "../../directives/directives.module";
import {NgPipesModule} from "ngx-pipes";

@NgModule({
    declarations: [
        CivilWorkOrderListPage,
    ],
    imports: [
        ComponentsModule,
        DirectivesModule,
        IonicPageModule.forChild(CivilWorkOrderListPage),
        NgPipesModule
    ],
})
export class CivilWorkOrderListPageModule {
}
