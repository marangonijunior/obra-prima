import {Component, Injector} from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {GenericListPage} from "../generic-list/generic-list";
import {CivilWorkOrderProvider} from "../../providers/civil-work-order/civil-work-order";
import {PouchdbOff} from "../../providers/pouchdb/pouchdb-off";
import {PouchControll} from "../../providers/pouchdb/pouchdb-controll";
import moment from "moment-timezone";

/**
 * Generated class for the CivilWorkOrderListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-order-list',
    templateUrl: 'civil-work-order-list.html',
})
export class CivilWorkOrderListPage extends GenericListPage {

    item;

    filter;

    constructor(public navCtrl: NavController,
                private civilWorkOrderProvider: CivilWorkOrderProvider,
                public navParams: NavParams,
                public pouchdbOff: PouchdbOff,
                protected injector: Injector) {
        super(navCtrl, navParams, injector);

        this.item = this.civilWorkProvider.selected;

        this.detailsPage = "CivilWorkOrderDetailsPage";
        this.filterPage = "CivilWorkOrderFilterPage";
    }

    add() {
        this.navCtrl.push('CivilWorkOrderAddEditPage');
    }


    returnFilter(filter: any) {

        if (filter) {
            this.filterModel = {};
            this.filterModel["$and"] = [{id_obra: this.civilWorkProvider.selected.id_obra,  deleted: {'$ne': true}}];
            this.filterModel["$or"] = [];

            console.log("filter", filter)
            let keysFilter = Object.keys(filter);

            if (filter.step) {
                let keysFilterStep = Object.keys(filter.step);
                keysFilterStep.map(x => {
                    if (filter.step[x]) {
                        keysFilter.push(x)
                    }
                    // keysFilter.push(x)
                })
            }

            console.log("keysFilter", keysFilter);

            this.recurInsertFind(0, false, filter, keysFilter, [], [], (isDate, arr, arrDate) => {


                if (this.filterModel) {

                    if (this.filterModel["$or"] && this.filterModel["$or"].length) {
                        this.filterModel["$and"].push({"$or": this.filterModel["$or"]});
                        delete this.filterModel["$or"];
                    }

                    let item = {
                        type: "civilWorkOrder",
                        selector: this.filterModel
                    }
                    this.pouchdbOff.customFind(item).then((data: any) => {

                        let objects = data.docs.map(row => {
                            return row;
                        });

                        console.log(data);
                        this.list = objects;
                    });
                    return;
                }

                console.log("callback")
                if (isDate) {
                    let item = {
                        type: "civilWorkOrder",
                        findDate: arrDate,
                        findNoDate: arr
                    }
                    console.log("civilWorkOrder", item)
                    this.pouchdbOff.findWithDate(item).then((data: any) => {
                        console.log("findWithDate", data)
                        if (arr.length > 0) {
                            this.pouchdbOff.findNoDate(item).then((data_: any) => {
                                this.list = [];
                                data.docs.forEach(item_a => {
                                    data_.docs.forEach(item_b => {
                                        if (item_b.id_solicitacao == item_a.id_solicitacao) {
                                            this.list.push(item_a);
                                        }
                                    })
                                });
                            }).catch(err => {
                                this.list = [];
                            })
                        } else {
                            let objects = data.docs.map(row => {
                                return row;
                            });
                            this.list = objects;
                        }
                    })
                } else {
                    let item = {
                        type: "civilWorkOrder",
                        findNoDate: arr
                    }
                    console.log("civilWorkOrder", item)
                    this.pouchdbOff.findNoDate(item).then((data: any) => {
                        console.log("findNoDate", data)
                        let objects = data.docs.map(row => {
                            return row;
                        });

                        if (filter.need) {
                            objects = objects.filter((item) => {
                                return item.numero.toString().indexOf(filter.need) != -1;
                            })
                        }

                        this.list = objects;
                    })
                }

            });
        } else {
            this.getDataFromLocalDatabase(false)
        }
    }

    recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback) {

        if (i < arrKey.length) {

            switch (arrKey[i]) {
                case "endDate":
                    let endDate = new Date(obj.endDate).toISOString();
                    arrDate.push({necessidade: {$lt: endDate}});
                    this.filterModel.$and.push({necessidade: {$lt: moment(endDate).add(1, "day")}});
                    onData = true;
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "startDate":
                    let startDate = new Date(obj.startDate).toISOString();
                    arrDate.push({necessidade: {$gt: moment(startDate).subtract(1, "day")}});
                    this.filterModel.$and.push({necessidade: {$gt: startDate}});
                    onData = true;
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "need":
                    let need = ("000000" + obj.need).slice(-5);
                    arr.push({numero: {$regex: need}});
                    this.filterModel.$and.push({numero: {$regex: need}});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "user":
                    arr.push({'usuario.nome': {$regex: RegExp(obj.user, "i")}});
                    this.filterModel.$and.push({'usuario.nome': {$regex: RegExp(obj.user, "i")}});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "Rascunho":
                    arr.push({'situacao.nome': "Rascunho"});
                    this.filterModel.$or.push({'situacao.nome': "Rascunho"});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "Aberta":
                    arr.push({'situacao.nome': "Aberta"});
                    this.filterModel.$or.push({'situacao.nome': "Aberta"});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "Parcial":
                    arr.push({'situacao.nome': "Parcial"});
                    this.filterModel.$or.push({'situacao.nome': "Parcial"});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "Cotada":
                    arr.push({'situacao.nome': "Cotada"});
                    this.filterModel.$or.push({'situacao.nome': "Cotada"});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "Rejeitada":
                    arr.push({'situacao.nome': "Rejeitada"});
                    this.filterModel.$or.push({'situacao.nome': "Rejeitada"});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                default:
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
            }

        } else {
            callback(onData, arr, arrDate);
        }

    }


    getDataFromApi(showLoading) {
        this.civilWorkOrderProvider.getAllByCivilWork(this.civilWorkProvider.selected, showLoading).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                this.saveOffline("civilWorkOrder", data.objects).then((data) => {
                    this.getDataFromLocalDatabase(false);
                   // this.checkDeletedItens("civilWorkOrder", "id_solicitacao", this.civilWorkOrderProvider);
                })
            }
            this.completeRefresh();

        }, (error) => {
            // se der erro de conexão ou n tiver internet cai aqui. Chama a função pra buscar os dados no banco de dados.
            this.getDataFromLocalDatabase(false);
            this.completeRefresh();
        })
    }

    getDataFromLocalDatabase(fetchFromApi?) {
        let item = {
            type: "civilWorkOrder",
            find: {id_obra: this.civilWorkProvider.selected.id_obra,  deleted: {'$ne': true}}
        };
        this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Order getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            this.list = objects;

            // console.log("get data from localDb bring " + this.list.length + " results");

            if (objects == null || objects.length == 0) {
                console.log("no data from localdb saved, fetching from API");
            }

            // after getting itens from localDB it will try to fetch from api (even if it has data on localDb)
            // showLoading = will show loading spinner if theres no data on db (its probably the first time the user is using the app), else will fetch in background (no loading spinner).
            var showLoading = (objects == null || objects.length == 0) ? true : false;

            if (fetchFromApi) {
                this.getDataFromApi(showLoading);
            }

        }).catch(error => {

        })
    }


}
