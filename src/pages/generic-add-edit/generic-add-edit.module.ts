import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GenericAddEditPage } from './generic-add-edit';
import {NgPipesModule} from "ngx-pipes";

@NgModule({
  declarations: [
    GenericAddEditPage,
  ],
  imports: [
    IonicPageModule.forChild(GenericAddEditPage),
      NgPipesModule
  ],
})
export class GenericAddEditPageModule {}
