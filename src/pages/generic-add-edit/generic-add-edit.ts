import {Component, Injector} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {GenericPage} from "../generic/generic";
import * as _ from 'underscore';

/**
 * Generated class for the GenericAddEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-generic-add-edit',
  templateUrl: 'generic-add-edit.html',
})
export class GenericAddEditPage extends GenericPage{

  item;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              protected injector: Injector) {
    super(navCtrl, navParams, injector);
  }

  ionViewDidLoad() {
    this.item = this.navParams.get("item") ? this.navParams.get("item") : {};
  }

  onAddClick(form) {
    console.log(form);
    if (form.valid) {
      this.add();
    } else {
      _.map(form.form.controls, function (val, key) {
        form.form.controls[key].markAsDirty();
      });
    }

  }

  add() {

  }

  onSuccess() {
    this.showMessage('Operação realizada com sucesso.');

    this.navCtrl.pop();
  }


}
