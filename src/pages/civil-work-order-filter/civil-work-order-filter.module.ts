import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CivilWorkOrderFilterPage } from './civil-work-order-filter';

@NgModule({
  declarations: [
    CivilWorkOrderFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(CivilWorkOrderFilterPage),
  ],
})
export class CivilWorkOrderFilterPageModule {}
