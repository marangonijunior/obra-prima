import {Component, Injector} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {CivilWorkOrderProvider} from "../../providers/civil-work-order/civil-work-order";
import {GenericLocalDataProvider} from "../../providers/generic-local-data/generic-local-data";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {GenericFilterPage} from "../generic-filter/generic-filter";

/**
 * Generated class for the CivilWorkOrderFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-order-filter',
    templateUrl: 'civil-work-order-filter.html',
})
export class CivilWorkOrderFilterPage extends GenericFilterPage {


    steps: Array<any> = [
        // {
        //     id: 132,
        //     nome: "Aberta",
        //     cor_hex: '#01579B'
        // },
        // {
        //
        //     id: 133,
        //     nome: "Parcial",
        //     cor_hex: "#4A148C"
        // },
        // {
        //     id: 134,
        //     nome: "Cotada",
        //     cor_hex: "#05A705"
        // },
        // {
        //     id: 135,
        //     nome: "Rejeitada",
        //     cor_hex: "#D50000"
        // }
    ];


    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public civilWorkOrderProvider: CivilWorkOrderProvider,
                public genericLocalDataProvider: GenericLocalDataProvider,
                protected injector: Injector,
                public requestHelpersProvider: RequestHelpersProvider,
                public navParams: NavParams) {

        super(navCtrl, navParams, viewCtrl, injector);
    }

    ionViewDidLoad() {
        super.ionViewDidEnter();
        if (!this.searchObject) {
            this.searchObject = {
                step: {}
            }
        }

        this.getSituacoes();
    }


    getDraftSituation() {
        return {
            cor_hex: "#d0d0d0",
            id_situacao: "draftApp",
            nome: "Rascunho"
        };
    }

    getSituacoes() {

        this.genericLocalDataProvider.getSituacoes().then((objects) => {

            this.steps = objects
            this.steps.push(this.getDraftSituation());
            this.steps.forEach((item, index) => {
                if (this.searchObject.step[item.nome] == undefined) {
                    this.searchObject.step[item.nome] = true;
                }
            })


        })

    }


}
