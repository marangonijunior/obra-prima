import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CivilWorkDayFilterPage} from './civil-work-day-filter';
import {FormsModule} from "@angular/forms";
import {DirectivesModule} from "../../directives/directives.module";
import {NgPipesModule} from "ngx-pipes";

@NgModule({
    declarations: [
        CivilWorkDayFilterPage,
    ],
    imports: [
        IonicPageModule.forChild(CivilWorkDayFilterPage),
        FormsModule,
        DirectivesModule,
        NgPipesModule
    ],
})
export class CivilWorkDayFilterPageModule {
}
