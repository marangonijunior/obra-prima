import {Component, Injector} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {GenericPage} from "../generic/generic";
import {GenericFilterPage} from "../generic-filter/generic-filter";
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";

/**
 * Generated class for the CivilWorkDayFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-day-filter',
    templateUrl: 'civil-work-day-filter.html',
})
export class CivilWorkDayFilterPage extends GenericFilterPage {

    steps = [
    ];

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public civilWorkProvider: CivilWorkProvider,
                public navParams: NavParams,
                protected injector: Injector) {

        super(navCtrl, navParams, viewCtrl, injector);


    }


    ionViewDidEnter() {
        super.ionViewDidEnter();
        if (!this.searchObject) {
            this.searchObject = {
                step: null,
                visible: true,
                invisible: true,
                files: true,
                nofiles: true,
            };
        }

        this.getFases();
    }

    beforeSend() {
        if (this.searchObject.step && this.searchObject.step == "all") {
            this.searchObject.step = null;
        }
    }

    getFases() {

        this.genericLocalDataProvider.getFasesByCivilWork().then((objects) => {
            this.steps = objects;
        });

    }

}
