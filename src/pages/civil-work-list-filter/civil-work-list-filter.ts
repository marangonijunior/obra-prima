import {Component, Injector} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {GenericFilterPage} from "../generic-filter/generic-filter";
import {PouchdbOff} from "../../providers/pouchdb/pouchdb-off";
import * as _ from 'underscore';


/**
 * Generated class for the CivilWorkListFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-list-filter',
    templateUrl: 'civil-work-list-filter.html',
})
export class CivilWorkListFilterPage extends GenericFilterPage {


    cities: Array<any> = [
        {
            id: 0,
            title: "Curitiba"
        },
        {
            id: 1,
            title: "São Paulo"
        }
    ];

    situations: Array<any> = [
        {
            id: 1,
            title: "Não iniciada"
        },
        {
            id: 2,
            title: "Em andamento"
        },
        {
            id: 3,
            title: "Paralisada"
        },
        {
            id: 4,
            title: "Cancelada"
        },
        {
            id: 5,
            title: "Finalizada"
        }
    ];

    types: Array<any> = [
        {
            id: 0,
            title: "Construção Casa"
        },
        {
            id: 1,
            title: "Construção Prédio"
        },
        {
            id: 3,
            title: "Reforma"
        }
    ];

    civilWorks = [];


    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public navParams: NavParams,
                protected injector: Injector) {

        super(navCtrl, navParams, viewCtrl, injector);
    }


    ionViewDidEnter() {
        super.ionViewDidEnter();
        if (!this.searchObject) {
            this.searchObject = {
                step: null,
                citiesArr: this.cities,
                situationsArr: this.situations,
                typesArr: this.types
            };
        }

        this.getCivilWorks().then(data => {
            this.getCities();
            this.getSituations();
            this.getTypes();
        })
    }


    beforeSend() {
        if (this.searchObject.city && this.searchObject.city == "all") {
            this.searchObject.city = null;
        }
        if (this.searchObject.type && this.searchObject.type == "all") {
            this.searchObject.type = null;
        }
        if (this.searchObject.situation && this.searchObject.situation == "all") {
            this.searchObject.situation = null;
        }
    }

    getCities() {


        this.cities = _.uniq(this.civilWorks, function (item, key) {
            return item.cidade;
        });

        this.cities = this.cities.filter((item, index) => {
            return item.cidade;
        });
    }

    getTypes() {

        this.types = _.uniq(this.civilWorks, function (item, key) {
            return item.categoria;
        });

        this.types = this.types.filter((item, index) => {
            return item.categoria;
        });
    }

    getSituations() {

        this.situations = _.uniq(this.civilWorks, function (item, key) {
            return item.situacao;
        });

        this.situations = this.situations.filter((item, index) => {
            return item.situacao;
        });
    }

    getCivilWorks() {
        let item = {
            type: "civilWork",
            find: {id_empresa: this.userLocalProvider.company.id_empresa}
        };
        return this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work List getDataFromLocalDatabase ### ", data)
            let objects = data.docs.map(row => {
                return row;
            });

            this.civilWorks = objects;

        }).catch(error => {
            console.log(error);
        })
    }

}
