import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CivilWorkListFilterPage } from './civil-work-list-filter';
import {NgPipesModule} from "ngx-pipes";

@NgModule({
  declarations: [
    CivilWorkListFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(CivilWorkListFilterPage),
      NgPipesModule
  ],
})
export class CivilWorkListFilterPageModule {}
