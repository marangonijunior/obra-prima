import {Component, Injector, ViewChild} from '@angular/core';
import {
    ActionSheet, ActionSheetController, AlertController, FabContainer, IonicPage, NavController,
    NavParams
} from 'ionic-angular';
import * as _ from 'underscore';
import {FileManagerComponent} from "../../components/file-manager/file-manager";
import {GenericListPage} from "../generic-list/generic-list";
import {CivilWorkGalleryProvider} from "../../providers/civil-work-gallery/civil-work-gallery";
import {FileHelperProvider} from "../../providers/file-helper/file-helper";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import moment from "moment-timezone";

/**
 * Generated class for the CivilWorkGalleryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-gallery',
    templateUrl: 'civil-work-gallery.html',
})
export class CivilWorkGalleryPage extends GenericListPage {

    @ViewChild('fileManager') fileManager: FileManagerComponent;
    @ViewChild('fab') fabContainer: FabContainer;

    folder: any = {};

    constructor(public navCtrl: NavController,
                public civilWorkGalleryProvider: CivilWorkGalleryProvider,
                public requestHelpersProvider: RequestHelpersProvider,
                public fileHelperProvider: FileHelperProvider,
                protected injector: Injector,
                public navParams: NavParams) {
        super(navCtrl, navParams, injector);

        this.filterPage = "CivilWorkDayFilterPage";
    }

    loadFromFiles() {
        this.fileManager.closeFabIfNeeded();

        if (this.fileManager.folder && this.fileManager.folder.rootFolder) {
            this.requestHelpersProvider.showMessage("Não é possivel adicionar arquivos na pasta raiz");
            return;
        }

        this.fileHelperProvider.loadFile().then((data) => {

            if (data) {
                var $this = this;
                setTimeout(() => {
                    $this.fileManager.setFileVisibilityBeforeSend(data);
                }, 500);

            }
            else {
                console.log("error trying to get file data, data is null");
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }

        }).catch((err) => {
            if (err.message == "Tipo de arquivo inválido") {
                err
                this.requestHelpersProvider.showMessage("Tipo de arquivo inválido.");
            }
            else {
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }


            console.log("error", err);
        });
    }


    loadFromCamera() {
        this.fileManager.closeFabIfNeeded();

        if (this.fileManager.folder && this.fileManager.folder.rootFolder) {
            this.requestHelpersProvider.showMessage("Não é possivel adicionar arquivos na pasta raiz");
            return;
        }

        this.fileHelperProvider.loadImage(this.fileHelperProvider.camera.PictureSourceType.CAMERA).then((data) => {

            if (data) {
                delete data.img;
                this.fileManager.setFileVisibilityBeforeSend(data);
            }
            else {
                console.log("error trying to get file data, data is null");
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }

        }).catch((err) => {
            console.log("error", err);
            if (err.message != "No Image Selected" && err != "No Image Selected") {
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }

        });
    }

    loadFromGallery() {
        this.fileManager.closeFabIfNeeded();

        if (this.fileManager.folder && this.fileManager.folder.rootFolder) {
            this.requestHelpersProvider.showMessage("Não é possivel adicionar arquivos na pasta raiz");
            return;
        }

        this.fileHelperProvider.loadImage(this.fileHelperProvider.camera.PictureSourceType.PHOTOLIBRARY).then((data) => {

            if (data) {
                delete data.img;
                this.fileManager.setFileVisibilityBeforeSend(data);
            }
            else {
                console.log("error trying to get file data, data is null");
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }

        }).catch((err) => {
            console.log("error", err);
            if (err.message != "No Image Selected" && err != "No Image Selected") {
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }

        });
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();
        console.log('ionViewDidLoad CivilWorkGalleryPage');
        this.fileManager.fabContainer = this.fabContainer;
        this.fileManager.afterSetFilter = (filter) => {
            console.log("afterSetFilter", filter);
            this.returnFilter(filter);
        }
    }

    ionViewDidEnter() {
        if (!this.fileManager.filter) {
            this.getData();
            this.setRefreshEvent();
        }
        if (!this.fileManager.selected || this.fileManager.selected.length == 0) {
            this.fileManager.refresh();
        }

    }

    showFilterModal() {

        this.fileManager.showFilterModal();
    }

    doRefresh(refresher) {
        this.refresher = refresher;
        refresher.complete();
        this.fileManager.setFilter(null);
        this.getDataFromLocalDatabase(false);
    }

    // returnFilter(filter: any) {
    //     console.log("work gallery", filter)
    //
    //
    //     if (filter) {
    //
    //         this.filterModel = {};
    //         this.filterModel["$and"] = [];
    //         this.filterModel["$or"] = [];
    //
    //
    //         this.fileManager.showFiles = filter.files;
    //         this.fileManager.showFolders = filter.folders;
    //         this.fileManager.showVisible = filter.visible;
    //         this.fileManager.showInvisible = filter.invisible;
    //
    //         if (filter.name === null || (filter.name && filter.name.length == 0)) {
    //             delete filter["name"];
    //         }
    //
    //         let keysFilter = Object.keys(filter);
    //         if (filter.step) {
    //             let keysFilterStep = Object.keys(filter.step);
    //             keysFilterStep.map(x => {
    //                 keysFilter.push(x)
    //             })
    //         }
    //
    //         console.log("keysFilter", keysFilter)
    //
    //         let item = {
    //             type: "civilWorkGallery",
    //             find: {id_obra: this.civilWorkProvider.selected.id_obra}
    //         }
    //         this.pouchdbOff.getFind(item).then((data: any) => {
    //             console.log("getFind", data);
    //             this.recurGallery(0, filter, data.docs, [], (resp_) => {
    //                 console.log("recurGallery", resp_)
    //
    //                 let objects = resp_.map(row => {
    //                     return row;
    //                 });
    //                 //this.list = objects;
    //                 this.folder.galeriasFilhas = objects;
    //                 this.folder.nome = "Raiz";
    //
    //             })
    //         })
    //
    //     } else {
    //         console.log("else")
    //         this.getDataFromLocalDatabase(false)
    //     }
    // }


    returnFilter(filter: any) {
        console.log("work gallery", filter)


        if (filter) {

            this.fileManager.showFiles = filter.files;
            this.fileManager.showFolders = filter.folders;

            this.filterModel = {};
            this.filterModel["$and"] = [{id_obra: this.civilWorkProvider.selected.id_obra, deleted: {'$ne': true}}];
            this.filterModel["$or"] = [];

            let keysFilter = Object.keys(filter);


            console.log("keysFilter", keysFilter)

            this.recurInsertFind(0, false, filter, keysFilter, [], [], (isDate, arr, arrDate) => {


                    if (this.filterModel) {
                        this.fileManager.filterModel = this.filterModel;
                        this.fileManager.customFind();
                        return;
                    }

                    console.log("callback")
                    if (isDate) {
                        let item = {
                            type: "civilWorkGallery",
                            findDate: [],
                            findNoDate: arr,
                            find: {id_obra: this.civilWorkProvider.selected.id_obra}
                        }
                        this.pouchdbOff.getFind(item).then((data: any) => {
                            console.log("getFind", data);
                            this.recurGallery(0, filter, data.docs, [], (resp_) => {
                                console.log("recurGallery", resp_)
                                if (arr.length > 0) {
                                    this.pouchdbOff.findNoDate(item).then((data_: any) => {
                                        this.list = [];
                                        resp_.forEach(item_a => {
                                            data_.docs.forEach(item_b => {
                                                if (item_b.id_galeria == item_a.id_galeria) {
                                                    this.list.push(item_a);
                                                    this.folder.galeriasFilhas.push(item_a);
                                                    this.folder.nome = "Raiz";
                                                }
                                            })
                                        });
                                    }).catch(err => {
                                        this.list = [];
                                        this.folder.galeriasFilhas = [];
                                        this.folder.nome = "Raiz";
                                    })
                                } else {
                                    let objects = resp_.map(row => {
                                        return row;
                                    });
                                    this.list = objects;
                                    this.folder.galeriasFilhas = objects;
                                    this.folder.nome = "Raiz";
                                }
                            })
                        })
                    } else {
                        let item = {
                            type: "civilWorkGallery",
                            findNoDate: arr,
                            find: {id_obra: this.civilWorkProvider.selected.id_obra}
                        }
                        console.log("civilWorkGallery", item)
                        this.pouchdbOff.findNoDate(item).then((data: any) => {
                            console.log("findNoDate", data)
                            let objects = data.docs.map(row => {
                                return row;
                            });
                            this.list = objects;
                            this.folder.galeriasFilhas = objects;
                            this.folder.nome = "Raiz";
                        })
                    }

                }
            );
        } else {
            this.getDataFromLocalDatabase(false)
        }
    }

    recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback) {

        if (i < arrKey.length) {

            switch (arrKey[i]) {
                case "endDate":
                    let endDate = new Date(obj.endDate).toISOString();
                    arrDate.push({data: {$lte: endDate}})
                    this.filterModel.$and.push({data: {$lte: moment(endDate).add(1, "day")}});
                    onData = true;
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "startDate":
                    let startDate = new Date(obj.startDate).toISOString();
                    arrDate.push({data: {$gte: startDate}})
                    this.filterModel.$and.push({data: {$gte: moment(startDate).subtract(1, "day")}});
                    onData = true;
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "invisible":
                    if (obj.invisible && !obj.visible) {
                        arr.push({visivelCliente: !obj.invisible});
                        this.filterModel.$and.push({visivelCliente: !obj.invisible});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "visible":
                    if (obj.visible && !obj.invisible) {
                        arr.push({visivelCliente: obj.visible});
                        this.filterModel.$and.push({visivelCliente: obj.visible});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "name":
                    if (obj.name) {
                        arr.push({nome: obj.name});
                        this.filterModel.$and.push({nome: {$regex: RegExp(obj.name, "i")}});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                default:
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
            }

        } else {
            callback(onData, arr, arrDate);
        }

    }


    recurGallery(i, obj, arr, arrNew, callback) {
        if (i < arr.length) {
            if (obj.hasOwnProperty('name')) {
                //&& (arr[i].nome.indexOf(obj.name) > -1)
                if (arr[i]['galeriasFilhas']) {
                    if (arr[i].galeriasFilhas.length > 0) {
                        let dateItem = new Date(arr[i].data);
                        if (
                            arr[i].nome.search(new RegExp(obj.name, "i")) > -1 &&
                            dateItem >= new Date(obj.startDate) &&
                            dateItem <= new Date(obj.endDate)
                        ) {
                            arrNew.push(arr[i])
                            this.recurGallery(0, obj, arr[i].galeriasFilhas, [], (arrResp: Array<any>) => {
                                if (arrResp.length > 0) {
                                    arrResp.map((resp) => {
                                        arrNew.push(resp);
                                    })
                                    i = i + 1;
                                    this.recurGallery(i, obj, arr, arrNew, callback);
                                } else {
                                    i = i + 1;
                                    this.recurGallery(i, obj, arr, arrNew, callback);
                                }
                            });
                        } else {
                            this.recurGallery(0, obj, arr[i].galeriasFilhas, [], (arrResp: Array<any>) => {
                                if (arrResp.length > 0) {
                                    arrResp.map((resp) => {
                                        arrNew.push(resp);
                                    })
                                    i = i + 1;
                                    this.recurGallery(i, obj, arr, arrNew, callback);
                                } else {
                                    i = i + 1;
                                    this.recurGallery(i, obj, arr, arrNew, callback);
                                }
                            });
                        }
                    } else {
                        let dateItem = new Date(arr[i].data);
                        if (obj.startDate && obj.endDate) {
                            if (
                                arr[i].nome.search(new RegExp(obj.name, "i")) > -1 &&
                                dateItem >= new Date(obj.startDate) &&
                                dateItem <= new Date(obj.endDate)
                            ) {
                                arrNew.push(arr[i])
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            } else {
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            }
                        } else if (obj.startDate) {
                            if (
                                arr[i].nome.search(new RegExp(obj.name, "i")) > -1 &&
                                dateItem >= new Date(obj.startDate)
                            ) {
                                arrNew.push(arr[i])
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            } else {
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            }
                        } else if (obj.endDate) {
                            if (
                                arr[i].nome.search(new RegExp(obj.name, "i")) > -1 &&
                                dateItem <= new Date(obj.endDate)
                            ) {
                                arrNew.push(arr[i])
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            } else {
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            }
                        } else {
                            if (
                                arr[i].nome.search(new RegExp(obj.name, "i")) > -1
                            ) {
                                arrNew.push(arr[i])
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            } else {
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            }
                        }
                    }
                } else {
                    let dateItem = new Date(arr[i].data);
                    if (obj.startDate && obj.endDate) {
                        if (
                            arr[i].nome.search(new RegExp(obj.name, "i")) > -1 &&
                            dateItem >= new Date(obj.startDate) &&
                            dateItem <= new Date(obj.endDate)
                        ) {
                            arrNew.push(arr[i])
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        } else {
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        }
                    } else if (obj.startDate) {
                        if (
                            arr[i].nome.search(new RegExp(obj.name, "i")) > -1 &&
                            dateItem >= new Date(obj.startDate)
                        ) {
                            arrNew.push(arr[i])
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        } else {
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        }
                    } else if (obj.endDate) {
                        if (
                            arr[i].nome.search(new RegExp(obj.name, "i")) > -1 &&
                            dateItem <= new Date(obj.endDate)
                        ) {
                            arrNew.push(arr[i])
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        } else {
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        }
                    } else {
                        if (
                            arr[i].nome.search(new RegExp(obj.name, "i")) > -1
                        ) {
                            arrNew.push(arr[i])
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        } else {
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        }
                    }
                }
            } else {
                if (arr[i]['galeriasFilhas']) {
                    if (arr[i].galeriasFilhas.length > 0) {
                        let dateItem = new Date(arr[i].data);
                        if (
                            dateItem >= new Date(obj.startDate) &&
                            dateItem <= new Date(obj.endDate)
                        ) {
                            arrNew.push(arr[i])
                            this.recurGallery(0, obj, arr[i].galeriasFilhas, [], (arrResp: Array<any>) => {
                                if (arrResp.length > 0) {
                                    arrResp.map((resp) => {
                                        arrNew.push(resp);
                                    })
                                    i = i + 1;
                                    this.recurGallery(i, obj, arr, arrNew, callback);
                                } else {
                                    i = i + 1;
                                    this.recurGallery(i, obj, arr, arrNew, callback);
                                }
                            });
                        } else {
                            this.recurGallery(0, obj, arr[i].galeriasFilhas, [], (arrResp: Array<any>) => {
                                if (arrResp.length > 0) {
                                    arrResp.map((resp) => {
                                        arrNew.push(resp);
                                    })
                                    i = i + 1;
                                    this.recurGallery(i, obj, arr, arrNew, callback);
                                } else {
                                    i = i + 1;
                                    this.recurGallery(i, obj, arr, arrNew, callback);
                                }
                            });
                        }
                    } else {
                        let dateItem = new Date(arr[i].data);
                        if (obj.startDate && obj.endDate) {
                            if (
                                dateItem >= new Date(obj.startDate) &&
                                dateItem <= new Date(obj.endDate)
                            ) {
                                arrNew.push(arr[i])
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            } else {
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            }
                        } else if (obj.startDate) {
                            if (
                                dateItem >= new Date(obj.startDate)
                            ) {
                                arrNew.push(arr[i])
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            } else {
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            }
                        } else if (obj.endDate) {
                            if (
                                dateItem <= new Date(obj.endDate)
                            ) {
                                arrNew.push(arr[i])
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            } else {
                                i = i + 1;
                                this.recurGallery(i, obj, arr, arrNew, callback);
                            }
                        } else {
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        }
                    }
                } else {
                    let dateItem = new Date(arr[i].data);
                    if (obj.startDate && obj.endDate) {
                        if (
                            dateItem >= new Date(obj.startDate) &&
                            dateItem <= new Date(obj.endDate)
                        ) {
                            arrNew.push(arr[i])
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        } else {
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        }
                    } else if (obj.startDate) {
                        if (
                            dateItem >= new Date(obj.startDate)
                        ) {
                            arrNew.push(arr[i])
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        } else {
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        }
                    } else if (obj.endDate) {
                        if (
                            dateItem <= new Date(obj.endDate)
                        ) {
                            arrNew.push(arr[i])
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        } else {
                            i = i + 1;
                            this.recurGallery(i, obj, arr, arrNew, callback);
                        }
                    } else {
                        i = i + 1;
                        this.recurGallery(i, obj, arr, arrNew, callback);
                    }
                }
            }
        } else {
            callback(arrNew)
        }
    }

    checkDeletedItens(type, idField, provider) {


        this.pouchControll.pouchdbOff.customFind({
            type: 'civilWorkGallery',
            selector: {}
        }).then((data) => {

            let idList = data.docs.map((item) => {
                return item[idField];
            })

            provider.getDeleted(idList, false).subscribe((data: any) => {
                if (this.requestHelpersProvider.successAndHasObject(data)) {


                    var item = {
                        type: type,
                        selector: {$or: []}
                    };

                    data.objects.forEach((id, index) => {
                        item.selector.$or.push({id_diaDia: id});
                    })


                    return this.pouchControll.pouchdbOff.customFind(item).then((data) => {
                        if (data.docs) {
                            data = data.docs.map((item, index) => {
                                item._deleted = true;
                                return item;
                            })

                            this.pouchControll.pouchdbOff.bulkDoc({
                                type: type,
                                obj: data
                            }).then(() => {
                                this.getDataFromLocalDatabase(false);
                            })
                        }

                        console.log(data);
                    });

                }
                this.completeRefresh();

            }, (error) => {
            })

        });


    }

    getDataFromApi(showLoading) {
        this.civilWorkGalleryProvider.getAllByCivilWork(this.civilWorkProvider.selected, showLoading).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                // var folder = {
                //     galeriasFilhas: data.objects,
                //     nome: "Raiz"
                // }
                // this.folder = folder;
                // this.fileManager.history = [];


                data.objects.forEach((item, index) => {
                    data.objects[index].root = true;
                });

                data.objects = this.pouchControll.getGalleriesRecursive(data.objects);


                // let item = {
                //     type: "civilWorkGallery",
                //     find: {id_galeria: this.folder.id_galeria}
                // };
                this.saveOffline("civilWorkGallery", data.objects).then((data) => {
                  //  this.getDataFromLocalDatabase(false);
                  //  this.checkDeletedItens("civilWorkGallery", "id_galeria", this.civilWorkGalleryProvider);


                })
            }

            this.completeRefresh();

        }, (error) => {
            // se der erro de conexão ou n tiver internet cai aqui. Chama a função pra buscar os dados no banco de dados.
            this.getDataFromLocalDatabase(false);
            this.completeRefresh();
        })
    }

    getDataFromLocalDatabase(fetchFromApi?) {
        this.fileManager.getRootFolder();
    }

    showOrderAndFilterOptions() {
        this.fabContainer.close();

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Opções',

            buttons: [
                {
                    icon: 'md-reorder',
                    text: 'Ordenar arquivos',
                    handler: () => {
                        this.fileManager.showOrderByOptions()
                    }
                },
                {
                    icon: 'md-funnel',
                    text: 'Filtrar arquivos',
                    handler: () => {
                        this.showFilterModal();
                    }
                }
            ]
        });
        actionSheet.present();
    }

    showSelectedFilesOptions() {

        this.fabContainer.close();

        var buttons: Array<any> = [{
            text: 'Selecionar tudo',
            icon: 'md-checkbox',
            handler: () => {
                _.each(this.fileManager.folder.galeriasFilhas, (file: any, index) => {
                    this.fileManager.folder.galeriasFilhas[index].selected = true;
                    this.fileManager.selected = _.without(this.fileManager.selected, _.findWhere(this.fileManager.selected, {
                        id_galeria: file.id_galeria
                    }));
                    this.fileManager.selected.push(file);
                });
            }
        }];

        if (this.fileManager.selected.length) {
            buttons.push({
                text: 'Cancelar seleção',
                icon: 'md-close-circle',
                handler: () => {
                    this.fileManager.unselectAll();
                }
            });
        }

        if (this.fileManager.selected.length == 1) {
            buttons.push({
                text: 'Renomear',
                icon: 'md-text',
                handler: () => {
                    this.fileManager.renameFile(this.fileManager.selected[0]);
                }
            });

            buttons.push({
                text: 'Excluir',
                icon: 'md-trash',
                handler: () => {
                    this.onDeleteClick(this.fileManager.selected[0]);
                }
            });


        }


        buttons.push({
                text: 'Mover para...',
                icon: 'md-folder',
                handler: () => {

                    var selected = this.fileManager.selected;
                    this.fileManager.unselectAll();
                    this.navCtrl.push("CivilWorkGalleryMovePage", {selected: selected});

                }
            },
            {
                text: 'Compartilhar com o cliente',
                icon: 'md-eye',
                handler: () => {
                    this.fileManager.changeVisibility(true);
                }
            },

            {
                text: 'NÂO exibir no Portal do cliente',
                icon: 'md-eye-off',
                handler: () => {
                    this.fileManager.changeVisibility(false);
                }
            }, {
                text: 'Cancelar',
                icon: 'md-close',
                role: "cancel"
            });

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Ações',

            buttons: buttons
        });
        actionSheet.present();
    }

    onDeleteClick(file) {
        this.showAlert("Excluir", "Tem certeza que deseja excluir o item?", () => {
            this.fileManager.deleteFile(file);
        })
    }


}
