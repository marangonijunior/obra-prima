import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CivilWorkGalleryPage } from './civil-work-gallery';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    CivilWorkGalleryPage,
  ],
  imports: [
    IonicPageModule.forChild(CivilWorkGalleryPage),
    ComponentsModule
  ],
})
export class CivilWorkGalleryPageModule {}
