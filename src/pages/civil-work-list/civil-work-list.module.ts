import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CivilWorkListPage} from './civil-work-list';
import {DirectivesModule} from "../../directives/directives.module";
import {ComponentsModule} from "../../components/components.module";
import {NgPipesModule} from "ngx-pipes";
import {IonicImageLoader} from "ionic-image-loader";

@NgModule({
    declarations: [
        CivilWorkListPage,
    ],
    imports: [
        IonicPageModule.forChild(CivilWorkListPage),
        DirectivesModule,
        IonicImageLoader,
        ComponentsModule,
        NgPipesModule
    ],
})
export class CivilWorkListPageModule {
}
