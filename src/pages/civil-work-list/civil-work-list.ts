import {Component, Injector, NgZone} from '@angular/core';
import {ActionSheetController, Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {GenericListPage} from "../generic-list/generic-list";
import {PouchdbOff} from "../../providers/pouchdb/pouchdb-off";
import {PouchControll} from "../../providers/pouchdb/pouchdb-controll";
import {FullSyncControll} from "../../providers/pouchdb/full-sync-controll";

/**
 * Generated class for the CivilWorkListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-list',
    templateUrl: 'civil-work-list.html',
})
export class CivilWorkListPage extends GenericListPage {
    list;
    filter;

    citiesArr;
    situationsArr;
    typesArr;
    filterModel: any = {};

    constructor(public navCtrl: NavController,
                public pouchControll: PouchControll,
                private zone: NgZone,
                private fullSyncControll: FullSyncControll,
                public events: Events,
                protected injector: Injector,
                public navParams: NavParams) {
        super(navCtrl, navParams, injector);
        this.detailsPage = "CivilWorkDetailsPage";
        this.filterPage = "CivilWorkListFilterPage";
    }

    onAdd() {
        console.log('onadd');
    }

    ionViewDidLoad() {
        this.events.subscribe('company:selected', () => {
            this.getData();
        });

    }

    openCivilWorkPage(page, item) {

        this.civilWorkProvider.setSelected(item).then((data) => {
            this.navCtrl.push(page, {civilWork: item});
        });
    }


    getData() {
        this.getDataFromLocalDatabase(true);

    }

    returnFilter(filter: any) {
        console.log("returnFilter", filter)

        if (filter) {
            this.filterModel = {};
            this.filterModel["$and"] = [{id_empresa: this.userLocalProvider.company.id_empresa}];
            this.filterModel["$or"] = [];

            this.list = [];

            this.citiesArr = this.filter.citiesArr;
            this.situationsArr = this.filter.situationsArr;
            this.typesArr = this.filter.typesArr;

            let keysFilter = Object.keys(filter);
            this.recurInsertFind(0, false, filter, keysFilter, [], [], (isDate, arr, arrDate) => {


                if (this.filterModel) {

                    if (this.filterModel["$or"] && this.filterModel["$or"].length) {
                        this.filterModel["$and"].push({"$or": this.filterModel["$or"]});
                        delete this.filterModel["$or"];
                    }


                    let item = {
                        type: "civilWork",
                        selector: this.filterModel
                    }
                    this.pouchdbOff.customFind(item).then((data: any) => {

                        let objects = data.docs.map(row => {
                            return row;
                        });

                        console.log(data);
                        this.list = objects;
                    });
                    return;
                }

                console.log(isDate, arr, arrDate)
                let item = {
                    type: "civilWork",
                    findNoDate: arr
                }
                this.pouchdbOff.findNoDate(item).then((data_: any) => {
                    console.log("findNoDate", data_)
                    let objects = data_.docs.map(row => {
                        return row;
                    });

                    this.list = objects;


                }).catch(err => {
                    this.list = [];
                })
            })
        } else {
            this.getDataFromLocalDatabase(true);
        }

    }

    recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback) {

        if (i < arrKey.length) {

            switch (arrKey[i]) {
                case "city":
                    if (obj.city) {
                        arr.push(obj.city == 'none' ? {'cidade': {$exists: false}} : {'cidade': {$regex: RegExp(obj.city, "i")}});
                        this.filterModel.$and.push(obj.city == 'none' ? {'cidade': ""} : {'cidade': {$regex: RegExp(obj.city, "i")}});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "civilwork":
                    if (obj.civilwork) {
                        arr.push({nome: {$regex: RegExp(obj.civilwork, "i")}});
                        this.filterModel.$and.push({nome: {$regex: RegExp(obj.civilwork, "i")}});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "userName":
                    arr.push({'nome_cliente': {$regex: RegExp(obj.userName, "i")}});
                    this.filterModel.$and.push({'nome_cliente': {$regex: RegExp(obj.userName, "i")}});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "type":
                    if (obj.type) {
                        arr.push({categoria: {$regex: RegExp(obj.type, "i")}});
                        this.filterModel.$and.push(obj.type == 'none' ? {categoria: ""} :{categoria: {$regex: RegExp(obj.type, "i")}});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "situation":
                    if (obj.situation) {
                        arr.push({situacao: {$regex: RegExp(obj.situation, "i")}});
                        this.filterModel.$and.push(obj.situation == 'none' ? {situacao: ""} : {situacao: {$regex: RegExp(obj.situation, "i")}});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                default:
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
            }

        } else {
            callback(onData, arr, arrDate);
        }

    }


    getDataFromApi(showLoading) {

        if (!this.fullSyncControll.backgroundSync) {
            this.sync(showLoading);         // HERE HERE HERE HERE ACTIVATE HERE
            return;
        }

        // this.civilWorkProvider.getDataFromApiAndSyncToLocalDatabase(showLoading)
        //     .subscribe((data) => {
        //         console.log("getDataFromApiaa", data);
        //     });
    }


    // sync all bds data
    sync(showLoading) {

        if (this.fullSyncControll.backgroundSync) {
            return;
        }


        if (showLoading) {
            this.loadingProvider.present();
        }

        this.fullSyncControll.doSync().subscribe((savedSuccessful: any) => {
            this.completeRefresh();
            this.getDataFromLocalDatabase(false);
            //this.checkDeletedItens("civilWork", "id_obra", this.civilWorkProvider);
            if (showLoading) {
                this.loadingProvider.dismiss();
            }
            this.fullSyncControll.startSyncIfNeed();

        }, (error) => {
            // se der erro de conexão ou n tiver internet cai aqui. Chama a função pra buscar os dados no banco de dados.
            if (showLoading) {
                this.loadingProvider.dismiss();
            }
            this.getDataFromLocalDatabase(false);
            this.fullSyncControll.startSyncIfNeed();
            this.completeRefresh();
        })
    }


    test() {

    }

    getDataFromLocalDatabase(fetchFromApi?) {
        let item = {
            type: "civilWork",
            find: {id_empresa: this.userLocalProvider.company.id_empresa}
        };
        this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work List getDataFromLocalDatabase ### ", data)
            let objects = data.docs.map(row => {
                return row;
            });

            this.list = objects;

            // console.log("get data from localDb bring " + this.list.length + " results");

            if (objects == null || objects.length == 0) {
                console.log("no data from localdb saved, fetching from API");
            }

            // after getting itens from localDB it will try to fetch from api (even if it has data on localDb)
            // showLoading = will show loading spinner if theres no data on db (its probably the first time the user is using the app), else will fetch in background (no loading spinner).
            var showLoading = (objects == null || objects.length == 0) ? true : false;

            if (fetchFromApi) {
                this.getDataFromApi(showLoading);
            }


        }).catch(error => {
            console.log(error);
        })
    }


    onClick(item) {

        this.civilWorkProvider.setSelected(item).then((data) => {
            this.navCtrl.push("CivilWorkDetailsPage", {item: item});
        });

    }

}
