import {Component} from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";
import {UserProvider} from "../../providers/user/user";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {UserLocalProvider} from "../../providers/user-local/user-local";
import {GenericLocalDataProvider} from "../../providers/generic-local-data/generic-local-data";
import {FullSyncControll} from "../../providers/pouchdb/full-sync-controll";

/**
 * Generated class for the CompanySelectListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-company-select-list',
    templateUrl: 'company-select-list.html',
})
export class CompanySelectListPage {

    modal;
    companies;
    deleteUser;
    user;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                public userProvider: UserProvider,
                public userLocalProvider: UserLocalProvider,
                public events: Events,
                public civilWorkProvider: CivilWorkProvider,
                private fullSyncControll: FullSyncControll,
                public genericLocalDataProvider: GenericLocalDataProvider,
                public requestHelpersProvider: RequestHelpersProvider,
                public viewCtrl: ViewController) {
        this.modal = this.navParams.get("modal");
    }

    ionViewDidEnter() {
        this.fullSyncControll.stopSyncIfNeed();
        this.deleteUser = this.navParams.get("deleteUser");
        this.user = this.navParams.get("user") ? this.navParams.get("user") : this.userLocalProvider.user;
        console.log('ionViewDidLoad CompanySelectListPage');
        this.getData();
    }

    onCompanyClick(company) {
        this.civilWorkProvider.removeSelected();

        if (this.user) {
            this.userLocalProvider.addOrEdit(this.user);
            this.userLocalProvider.setUserOnLocalDb(this.user).then(() => {
            });

        }


        this.userLocalProvider.setSelectedCompany(company).then(() => {


            if (!this.modal) {
                this.navCtrl.setRoot("CivilWorkListPage");
            }
            else {
                this.events.publish('company:selected');
                this.close();
            }

        });


    }

    close() {
        this.viewCtrl.dismiss();
    }

    private getData() {
        this.userProvider.getCompanies(this.user).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                this.companies = data.objects;
                this.genericLocalDataProvider.addOrEdit("companies_" + this.user.id_usuario, data.objects);

                if (this.companies && this.companies.length == 1) {
                    this.onCompanyClick(this.companies[0]);
                }
            }
        }, (error) => {
            this.genericLocalDataProvider.getByKey("companies_" + this.user.id_usuario).then((objects) => {
                if (!objects || objects.length == 0) {
                    this.requestHelpersProvider.showTryAgainDialog(() => {
                        this.getData();
                    })
                }
                else {
                    this.companies = objects[0].data;

                    if (this.companies && this.companies.length == 1) {
                        this.onCompanyClick(this.companies[0]);
                    }

                }
            })
        });
    }

}
