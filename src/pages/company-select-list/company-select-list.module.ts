import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanySelectListPage } from './company-select-list';

@NgModule({
  declarations: [
    CompanySelectListPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanySelectListPage),
  ],
})
export class CompanySelectListPageModule {}
