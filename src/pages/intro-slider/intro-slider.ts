import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Slides, ViewController} from 'ionic-angular';
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {UserProvider} from "../../providers/user/user";
import {GeneralProvider} from "../../providers/general/general";
import {GenericLocalDataProvider} from "../../providers/generic-local-data/generic-local-data";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the IntroSliderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-intro-slider',
    templateUrl: 'intro-slider.html',
})
export class IntroSliderPage {

    @ViewChild('introSlider') slider: Slides;

    slides;

    constructor(public navCtrl: NavController,
                public requestHelpersProvider: RequestHelpersProvider,
                public generalProvider: GeneralProvider,
                public storage: Storage,
                public genericLocalDataProvider: GenericLocalDataProvider,
                public navParams: NavParams, public viewCtrl: ViewController) {
    }

    onSkipClick() {
        this.slider.slideTo(this.slider.length() - 1);
    }

    onContinueclick() {
        this.storage.set("showedSlider", true).then((data) => {
            this.viewCtrl.dismiss();
        });


    }

    private getData() {
        this.generalProvider.getSlides().subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                this.slides = data.objects;
                this.genericLocalDataProvider.addOrEdit("slides", data.objects);
                if (!this.slides.length) {
                    this.viewCtrl.dismiss();
                }

            }
            else {
                this.viewCtrl.dismiss();
            }
        }, (error) => {
            this.genericLocalDataProvider.getByKey("slides").then((objects) => {
                if (!objects || objects.length == 0) {
                    // this.requestHelpersProvider.showTryAgainDialog(() => {
                    //     this.getData();
                    // })
                    this.onContinueclick();
                }
                else {
                    this.slides = objects[0].data;
                }
            })
        });
    }


    ionViewDidEnter() {
        this.getData();
    }

}
