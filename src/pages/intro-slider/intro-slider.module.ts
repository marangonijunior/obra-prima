import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IntroSliderPage } from './intro-slider';
import {IonicImageLoader} from "ionic-image-loader";

@NgModule({
  declarations: [
    IntroSliderPage,
  ],
  imports: [
    IonicImageLoader,
    IonicPageModule.forChild(IntroSliderPage),
  ],
})
export class IntroSliderPageModule {}
