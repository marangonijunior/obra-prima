import {Component, Injector} from '@angular/core';
import {ActionSheetController, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {CivilWorkDayProvider} from "../../providers/civil-work-day/civil-work-day";
import {UserProvider} from "../../providers/user/user";
import {GenericListPage} from "../generic-list/generic-list";
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {PouchControll} from "../../providers/pouchdb/pouchdb-controll";
import {PouchdbSyncControlSingle} from "../../providers/pouchdb/pouchdb-sync-control-single";
import {fromPromise} from "rxjs/observable/fromPromise";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import {FullSyncControll} from "../../providers/pouchdb/full-sync-controll";
import moment from "moment-timezone";
/**
 * Generated class for the CivilWorkDayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-day-list',
    templateUrl: 'civil-work-day-list.html',
})
export class CivilWorkDayListPage extends GenericListPage {


    list = [];
    showWithFiles = true;
    showWithoutFiles = true;


    constructor(public navCtrl: NavController,
                private civilWorkDayProvider: CivilWorkDayProvider,
                public pouchControll: PouchControll,
                public pouchdbSyncControlSingle: PouchdbSyncControlSingle,
                public navParams: NavParams,
                public fullSyncControll: FullSyncControll,
                protected injector: Injector) {
        super(navCtrl, navParams, injector);

        this.detailsPage = "CivilWorkDayDetailsPage";
        this.filterPage = "CivilWorkDayFilterPage";
    }


    ionViewDidEnter() {
        if (!this.filter) {
            this.getData();
        }

        this.setRefreshEvent();

    }

    doRefresh(refresher) {
        this.refresher = refresher;
        refresher.complete();
        this.setFilter(null);
        this.getDataFromLocalDatabase(false);
    }

    showItem(item) {
        if (this.showWithoutFiles && this.showWithFiles) {
            return true;
        }
        else if (this.showWithoutFiles) {
            return item.anexos == 0
        }
        else if (this.showWithFiles) {
            return item.anexos > 0
        }
        else {
            return true;
        }
    }

    add() {
        this.navCtrl.push('CivilWorkDayAddEditPage');
    }

    returnFilter(filter: any) {


        if (filter) {
            this.filterModel = {};
            this.filterModel["$and"] = [{id_obra: this.civilWorkProvider.selected.id_obra, deleted: {'$ne': true}}];
            this.filterModel["$or"] = [];

            // this.showWithFiles = filter.files;
            // this.showWithoutFiles = filter.nofiles;

            console.log("filter", filter)
            let keysFilter = Object.keys(filter);
            this.recurInsertFind(0, false, filter, keysFilter, [], [], (isDate, arr, arrDate) => {

                if (this.filterModel) {
                    let item = {
                        type: "civilWorkDay",
                        selector: this.filterModel
                    }
                    this.pouchdbOff.customFind(item).then((data: any) => {

                        let objects = data.docs.map(row => {
                            return row;
                        });

                        console.log(data);
                        this.list = objects;
                    });
                    return;
                }

                if (isDate) {
                    arrDate.push({id_obra: this.civilWorkProvider.selected.id_obra})
                    let item = {
                        type: "civilWorkDay",
                        findDate: arrDate,
                        findNoDate: arr
                    }
                    this.pouchdbOff.findWithDate(item).then((data: any) => {

                        if (arr.length > 0) {
                            this.pouchdbOff.findNoDate(item).then((data_: any) => {
                                this.list = [];
                                data.docs.forEach(item_a => {
                                    data_.docs.forEach(item_b => {
                                        if (item_b.id_diaDia == item_a.id_diaDia) {
                                            this.list.push(item_a);
                                        }
                                    })
                                });
                            }).catch(err => {
                                this.list = [];
                            })
                        } else {
                            let objects = data.docs.map(row => {
                                return row;
                            });
                            this.list = objects;
                        }
                    })
                } else {
                    let item = {
                        type: "civilWorkDay",
                        findNoDate: arr
                    }
                    if (arr.length > 0) {
                        this.pouchdbOff.findNoDate(item).then((data: any) => {
                            console.log("findNoDate", data)
                            let objects = data.docs.map(row => {
                                return row;
                            });

                            if (filter.step) {
                                objects = objects.filter((item) => {
                                    return item.fase_obra && item.fase_obra.nome.indexOf(filter.step) != -1;
                                })
                            }

                            this.list = objects;
                        })
                    } else {
                        this.getDataFromLocalDatabase(false)
                    }
                }

            });
        } else {
            this.getDataFromLocalDatabase(false)
        }
    }

    recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback) {

        if (i < arrKey.length) {

            switch (arrKey[i]) {
                case "endDate":
                    let endDate = new Date(obj.endDate).toISOString();
                    arrDate.push({data: {$lte: moment(endDate).add(1, "day")}})
                    this.filterModel.$and.push({data: {$lte: moment(endDate).add(1, "day")}});
                    onData = true;
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "startDate":
                    let startDate = new Date(obj.startDate).toISOString();
                    arrDate.push({data: {$gte: moment(startDate).subtract(1, "day")}})
                    this.filterModel.$and.push({data: {$gte: moment(startDate).subtract(1, "day")}});
                    onData = true;
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "nofiles":
                    if (obj.nofiles && !obj.files) {
                        arr.push({anexos: 0})
                        this.filterModel.$and.push({anexos: 0});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "files":
                    if (obj.files && !obj.nofiles) {
                        arr.push({anexos: {$gt: 0}})
                        this.filterModel.$and.push({anexos: {$gt: 0}});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "user":
                    arr.push({'usuario.nome': {$regex: RegExp(obj.user, "i")}});
                    this.filterModel.$and.push({'usuario.nome': {$regex: RegExp(obj.user, "i")}});
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
                case "step":
                    if (obj.step) {

                        arr.push({'fase_obra.nome': {$regex: RegExp(obj.step, "i")}});
                        this.filterModel.$and.push(obj.step == 'none' ? {'fase_obra': {'$exists': false}} : {'fase_obra.nome': {$regex: RegExp(obj.step, "i")}});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "invisible":
                    if (obj.invisible && !obj.visible) {
                        arr.push({exibirCliente: !obj.invisible})
                        this.filterModel.$and.push({exibirCliente: !obj.invisible});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                case "visible":
                    if (obj.visible && !obj.invisible) {
                        arr.push({exibirCliente: obj.visible})
                        this.filterModel.$and.push({exibirCliente: obj.visible});
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    } else {
                        i = i + 1;
                        this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                        break;
                    }
                default:
                    i = i + 1;
                    this.recurInsertFind(i, onData, obj, arrKey, arr, arrDate, callback);
                    break;
            }

        } else {
            callback(onData, arr, arrDate);
        }

    }


    getDataFromApi(showLoading) {
        this.civilWorkDayProvider.getAllByCivilWork(this.civilWorkProvider.selected, showLoading).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                // this.list = data.objects;
                this.saveOffline("civilWorkDay", data.objects).then((data) => {
                    this.getDataFromLocalDatabase(false);
                }).then((data) => {
                   // this.checkDeletedItens("civilWorkDay", "id_diaDia", this.civilWorkDayProvider);
                })
            }
            this.completeRefresh();

        }, (error) => {
            // se der erro de conexão ou n tiver internet cai aqui. Chama a função pra buscar os dados no banco de dados.
            this.getDataFromLocalDatabase(false);
            this.completeRefresh();
        })
    }


    getDataFromLocalDatabase(fetchFromApi ?) {
        let item = {
            type: "civilWorkDay",
            find: {id_obra: this.civilWorkProvider.selected.id_obra, deleted: {'$ne': true}}
        };
        this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Day List getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            this.list = objects;

            //console.log("get data from localDb bring " + this.list.length + " results");

            if (objects == null || objects.length == 0) {
                console.log("no data from localdb saved, fetching from API");
            }

            // after getting itens from localDB it will try to fetch from api (even if it has data on localDb)
            // showLoading = will show loading spinner if theres no data on db (its probably the first time the user is using the app), else will fetch in background (no loading spinner).
            var showLoading = (objects == null || objects.length == 0) ? true : false;

            if (fetchFromApi) {
                this.getDataFromApi(showLoading);
            }

        }).catch(error => {
            this.completeRefresh();

        })

    }


}
