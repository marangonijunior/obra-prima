import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CivilWorkDayListPage} from "./civil-work-day-list";
import {DirectivesModule} from "../../directives/directives.module";
import {ComponentsModule} from "../../components/components.module";
import {NgPipesModule} from "ngx-pipes";

@NgModule({
    declarations: [
        CivilWorkDayListPage,
    ],
    imports: [
        IonicPageModule.forChild(CivilWorkDayListPage),
        DirectivesModule,
        ComponentsModule,
        NgPipesModule
    ],
})
export class CivilWorkDayListPageModule {
}
