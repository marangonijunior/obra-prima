import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CivilWorkDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-civil-work-details',
  templateUrl: 'civil-work-details.html',
})
export class CivilWorkDetailsPage {

  item;
  options;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.options = [
      {
        title: "Dia a dia",
        icon: "date_range",
        onClick: () => {
          this.navCtrl.push("CivilWorkDayListPage", {civilWork: this.item});
        }
      },

      {
        title: "Galeria",
        icon: "photo",
        onClick: () => {
          this.navCtrl.push("CivilWorkGalleryPage", {civilWork: this.item});
        }
      },
      {
        title: "Solicitações de compra",
        icon: "shopping_cart",
        onClick: () => {
          this.navCtrl.push("CivilWorkOrderListPage", {civilWork: this.item});
        }
      },
      // {
      //   title: "Medição física",
      //   icon: "timeline",
      //   onClick: () => {
      //
      //   }
      // },

    ];

  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad CivilWorkDetailsPage');
    this.item = this.navParams.get("item");
  }

}
