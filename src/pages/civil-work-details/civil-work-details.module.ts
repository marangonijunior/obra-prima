import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CivilWorkDetailsPage } from './civil-work-details';
import {MatTabsModule} from '@angular/material/tabs';
import {DirectivesModule} from "../../directives/directives.module";
import {IonicImageViewerModule} from "ionic-img-viewer";
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    CivilWorkDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CivilWorkDetailsPage),
    MatTabsModule,
    DirectivesModule,
    ComponentsModule
  ],
})
export class CivilWorkDetailsPageModule {}
