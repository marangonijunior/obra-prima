import {Component, Injector} from '@angular/core';
import {ActionSheetController, Events, IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";
import {GenericPage} from "../generic/generic";
import {CivilWorkGalleryProvider} from "../../providers/civil-work-gallery/civil-work-gallery";

/**
 * Generated class for the GenericListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-generic-list',
    templateUrl: 'generic-list.html',
})
export class GenericListPage extends GenericPage {

    detailsPage;
    filterPage;
    filter;
    list;
    filterModel: any = {};


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                protected injector: Injector) {
        super(navCtrl, navParams, injector);


    }


    checkDeletedItens(type, idField, provider) {

        let idList = this.list.map((item) => {
            return item[idField];
        })

        provider.getDeleted(idList, false).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {


                var item = {
                    type: type,
                    selector: {$or: []}
                };

                data.object.forEach((id, index) => {

                    var dataObj = {};
                    dataObj[idField] = id;
                    item.selector.$or.push(dataObj);

                })


                return this.pouchControll.pouchdbOff.customFind(item).then((data) => {
                    if (data.docs) {
                        data = data.docs.map((item, index) => {
                            item._deleted = true;
                            return item;
                        })

                        this.pouchControll.pouchdbOff.bulkDoc({
                            type: type,
                            obj: data
                        }).then(() => {
                            this.getDataFromLocalDatabase(false);
                        })
                    }

                    console.log(data);
                });

            }
            this.completeRefresh();

        }, (error) => {
            console.log("error deleting", error);
        })
    }

    getData() {
        this.getDataFromLocalDatabase(false);

    }


    setRefreshEvent() {
        this.events.subscribe('refresh', (fromApi) => {
            if (!fromApi) {
                this.getDataFromLocalDatabase(false);
            }
            else {
                this.getDataFromApi(true);
            }
        });
    }

    getDataFromApi(showLoading) {

    }

    ionViewDidEnter() {
        if (!this.filter) {
            this.getData();
        }

        this.setRefreshEvent();

    }

    getDataFromLocalDatabase(fetchFromApi?) {

    }

    onClick(item) {
        this.navCtrl.push(this.detailsPage, {item: item});
    }

    doRefresh(refresher) {
        this.refresher = refresher;
        refresher.complete();
        this.setFilter(null);
        this.getDataFromLocalDatabase(false);
    }

    showFilterModal() {


        let modal = this.modalCtrl.create(this.filterPage, {searchObject: this.filter});
        modal.onDidDismiss(data => {
            console.log("onDidDismiss");
            console.log(data);
            if (data) {
                this.setFilter(data.searchObject);
            }

        });
        modal.present();
    }

    setFilter(filter) {
        console.log(filter);
        console.log("setFilter");
        this.filter = filter;
        this.returnFilter(this.filter)

    }

    returnFilter(filter: any) {

    }

    onSearchBadgeClick() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Filtro ativo',

            buttons: [
                {
                    text: 'Remover filtro',
                    handler: () => {
                        this.setFilter(null);
                    }
                },
                {
                    text: 'Editar filtro',
                    handler: () => {
                        this.showFilterModal();
                    }
                }
            ]
        });
        actionSheet.present();
    }

}
