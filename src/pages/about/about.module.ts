import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutPage } from './about';
import {IonicImageLoader} from "ionic-image-loader";

@NgModule({
  declarations: [
    AboutPage,
  ],
  imports: [
    IonicImageLoader,
    IonicPageModule.forChild(AboutPage),
  ],
})
export class AboutPageModule {}
