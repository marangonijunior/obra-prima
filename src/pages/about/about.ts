import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {GeneralProvider} from "../../providers/general/general";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {GenericLocalDataProvider} from "../../providers/generic-local-data/generic-local-data";

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-about',
    templateUrl: 'about.html',
})
export class AboutPage {

    about = {};

    constructor(public navCtrl: NavController,
                public requestHelpersProvider: RequestHelpersProvider,
                private generalProvider: GeneralProvider,
                public genericLocalDataProvider: GenericLocalDataProvider,
                public navParams: NavParams) {
    }

    ionViewDidEnter() {
        this.getData();
    }

    getData() {
        this.generalProvider.getAbout().subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                this.about = data.object;
                this.genericLocalDataProvider.addOrEdit("about", data.object);
            }
        }, (error) => {
            this.genericLocalDataProvider.getByKey("about").then((objects) => {
                if (!objects || objects.length == 0) {
                    this.requestHelpersProvider.showTryAgainDialog(() => {
                        this.getData();
                    })
                }
                else {
                    this.about = objects[0].data;
                }
            })
        });
    }

}
