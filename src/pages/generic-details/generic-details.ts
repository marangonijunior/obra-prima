import {Component, Injector} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {GenericPage} from "../generic/generic";

/**
 * Generated class for the GenericDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-generic-details',
    templateUrl: 'generic-details.html',
})
export class GenericDetailsPage extends GenericPage {

    item;
    editPage;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                protected injector: Injector) {
        super(navCtrl, navParams, injector);
    }

    edit() {
        this.navCtrl.push(this.editPage, {item: this.item});
    }

    onDeleteClick() {
        this.showAlert("Excluir", "Tem certeza que deseja excluir o item?", () => {
            this.delete();
        })
    }

    delete() {

    }

    ionViewDidEnter() {
        this.item = this.navParams.get("item");
        this.getData();
    }

    showOptions() {


        let buttons = [
            {
                text: 'Editar',
                icon: "md-create",
                handler: () => {
                    console.log('Destructive clicked');
                    this.edit();
                }
            },
            {
                text: 'Excluir',
                icon: "md-close",
                handler: () => {
                    console.log('Archive clicked');
                    this.onDeleteClick();
                }
            }
        ]


        let actionSheet = this.actionSheetCtrl.create({
            title: 'Opções',
            buttons: buttons
        });
        actionSheet.present();

    }


}
