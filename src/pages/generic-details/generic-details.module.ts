import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GenericDetailsPage } from './generic-details';

@NgModule({
  declarations: [
    GenericDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(GenericDetailsPage),
  ],
})
export class GenericDetailsPageModule {}
