import {Component, Injector} from '@angular/core';
import {
    ActionSheetController, AlertController, Events, IonicPage, ModalController, NavController, NavParams,
    ToastController
} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";
import {LoadingProvider} from "../../providers/loading/loading";
import {PouchdbOff} from "../../providers/pouchdb/pouchdb-off";
import {UserLocalProvider} from "../../providers/user-local/user-local";
import {PouchControll} from "../../providers/pouchdb/pouchdb-controll";
import {GenericLocalDataProvider} from "../../providers/generic-local-data/generic-local-data";
import {PouchdbSync} from "../../providers/pouchdb/pouchdb-sync";


/**
 * Generated class for the GenericPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-generic',
    templateUrl: 'generic.html',
})
export class GenericPage {

    actionSheetCtrl;
    userProvider;
    public userLocalProvider;
    requestHelpersProvider;
    civilWorkProvider: CivilWorkProvider;
    toastController;
    loadingProvider;
    modalCtrl;
    alertCtrl;
    events;
    refresher;
    pouchdbOff: PouchdbOff;
    pouchdbSync: PouchdbSync;
    protected pouchControll: PouchControll;
    protected genericLocalDataProvider: GenericLocalDataProvider;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                protected injector: Injector) {
        this.actionSheetCtrl = this.injector.get(ActionSheetController);
        this.userProvider = this.injector.get(UserProvider);
        this.userLocalProvider = this.injector.get(UserLocalProvider);
        this.requestHelpersProvider = this.injector.get(RequestHelpersProvider);
        this.civilWorkProvider = this.injector.get(CivilWorkProvider);
        this.modalCtrl = this.injector.get(ModalController);
        this.events = this.injector.get(Events);
        this.toastController = this.injector.get(ToastController);
        this.loadingProvider = this.injector.get(LoadingProvider);
        this.alertCtrl = this.injector.get(AlertController);
        this.pouchdbOff = this.injector.get(PouchdbOff);
        this.pouchControll = this.injector.get(PouchControll);
        this.genericLocalDataProvider = this.injector.get(GenericLocalDataProvider);
        this.pouchdbSync = this.injector.get(PouchdbSync);
    }

    saveOffline(type, objects) {
        var item = {type: type, obj: objects};
        return this.pouchControll.bulkDoc(item);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad GenericPage');

    }

    deleteItemFromDb(item: any) {

        return this.pouchdbOff.delete(item).then(
            resp => {
                return resp;

            }).catch((error) => {
            console.log("deleteItemFromDb", error);
           // this.requestHelpersProvider.showMessage("Houve um erro, por favor tente novamente.");
        })

    }

    deleteOff(item) {

        item.obj.deleted = true;
        return this.pouchControll.insert(item);

    }

    getData() {

    }

    getDataFromApi(showLoading) {

    }

    setData() {

    }

    doRefresh(refresher) {
        this.refresher = refresher;
        this.getDataFromApi(true);
    }

    completeRefresh() {
        if (this.refresher) {
            this.refresher.complete();
        }
    }

    showMessage(message, duration?) {
        let toast = this.toastController.create({
            message: message,
            duration: duration ? duration : 2000
        });
        toast.present();
    }

    showAlert(title, message, onOkClick) {

        var confirmAlert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancelar',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        console.log('yes clicked');
                        if (onOkClick) {
                            onOkClick();
                        }
                    }
                }
            ]
        });
        confirmAlert.present();

    }


}
