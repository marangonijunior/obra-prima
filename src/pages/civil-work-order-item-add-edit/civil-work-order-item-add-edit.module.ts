import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CivilWorkOrderItemAddEditPage} from './civil-work-order-item-add-edit';
import {NgPipesModule} from "ngx-pipes";
import {ComponentsModule} from "../../components/components.module";
import {DirectivesModule} from "../../directives/directives.module";

@NgModule({
    declarations: [
        CivilWorkOrderItemAddEditPage,
    ],
    imports: [
        DirectivesModule,
        ComponentsModule,
        IonicPageModule.forChild(CivilWorkOrderItemAddEditPage),
        NgPipesModule
    ],
})
export class CivilWorkOrderItemAddEditPageModule {
}
