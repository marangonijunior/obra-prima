import {Component, Injector} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import * as _ from 'underscore';
import {GenericAddEditPage} from "../generic-add-edit/generic-add-edit";

/**
 * Generated class for the CivilWorkOrderItemAddEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-order-item-add-edit',
    templateUrl: 'civil-work-order-item-add-edit.html',
})
export class CivilWorkOrderItemAddEditPage extends GenericAddEditPage {

    callback;
    order: any = {};

    steps = []
    item: any = {};

    services = []

    constructor(public navCtrl: NavController,
                protected injector: Injector,
                public navParams: NavParams) {
        super(navCtrl, navParams, injector);


    }

    cancel() {
        this.navCtrl.pop();
    }

    add() {
        if (this.item.id_fase && this.item.id_fase == 'none') {
            delete this.item.id_fase;
        }
        if (this.item.id_servico && this.item.id_servico == 'none') {
            delete this.item.id_servico;
        }

        if (!this.item.id && !this.item.localId) {
            this.item.localId = this.order.itensArray && this.order.itensArray.length > 0 ? _.max(this.order.itensArray.map((rec) => {
                return rec.localId
            })) + 1 : 1;
        }
        this.callback(this.item).then(() => {
            this.navCtrl.pop();
        });
    }

    ionViewWillLeave() {

    }

    ionViewDidLoad() {
        super.ionViewDidLoad();

        console.log("frame " + this.item.frame);

        this.callback = this.navParams.get("callback");
        this.order = this.navParams.get("order") ? this.navParams.get("order") : {};
        this.services = this.navParams.get("services") ? this.navParams.get("services") : [];
        this.steps = this.navParams.get("steps") ? this.navParams.get("steps") : [];


        if ((this.item.id_fase || this.item.id_servico)) {
            this.item.frame = true;
            if (this.item.id_fase && (!this.services || !this.services.length)) {
                this.getServicos();
            }
        }
        else {
            this.item.frame = this.order.frame;
            this.item.id_fase = this.order.id_fase;
            this.item.id_servico = this.order.id_servico;
        }
        console.log(this.item);

        this.getFases();
    }

    getFases() {

        this.genericLocalDataProvider.getFasesByCivilWork().then((objects) => {
            this.steps = objects;
        });
    }

    getServicos() {
        if (!this.item.id_fase || this.item.id_fase == 'none') {
            return;
        }

        this.genericLocalDataProvider.getServicosByFase(this.item.id_fase).then((objects) => {
            this.services = objects;
            if (!this.services || !this.services.length) {
                if (!this.services || !this.services.length) {
                    if (this.item.id_fase) {
                        delete this.item.id_fase;
                    }
                }
            }
        })

    }

}
