import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CivilWorkDayAddEditPage} from './civil-work-day-add-edit';
import {FileChooser} from "@ionic-native/file-chooser";
import {Camera} from "@ionic-native/camera";
import {FilePath} from "@ionic-native/file-path";
import {File} from '@ionic-native/file';
import {FormsModule} from "@angular/forms";
import {DirectivesModule} from "../../directives/directives.module";
import {NgPipesModule} from "ngx-pipes";

@NgModule({
    declarations: [
        CivilWorkDayAddEditPage,
    ],
    imports: [
        FormsModule,
        IonicPageModule.forChild(CivilWorkDayAddEditPage),
        DirectivesModule,
        NgPipesModule
    ],
    providers: [
        Camera,
        FileChooser,
        FilePath,
        File
    ]
})
export class CivilWorkDayAddEditPageModule {
}
