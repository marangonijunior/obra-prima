import {Component, Injector} from '@angular/core';
import {ActionSheetController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {Camera, CameraOptions} from "@ionic-native/camera";
import {FileChooser} from "@ionic-native/file-chooser";
import {FilePath} from "@ionic-native/file-path";
import {File} from '@ionic-native/file';
import {LoadingProvider} from "../../providers/loading/loading";
import * as _ from 'underscore';
import {CivilWorkDayProvider} from "../../providers/civil-work-day/civil-work-day";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {UserProvider} from "../../providers/user/user";
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";
import {GenericAddEditPage} from "../generic-add-edit/generic-add-edit";
import {FileHelperProvider} from "../../providers/file-helper/file-helper";
import moment from "moment-timezone";

/**
 * Generated class for the CivilWorkDayAddEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-day-add-edit',
    templateUrl: 'civil-work-day-add-edit.html',
})
export class CivilWorkDayAddEditPage extends GenericAddEditPage {

    item: any = {};
    files = [];


    steps = []

    constructor(public navCtrl: NavController,
                private fileHelperProvider: FileHelperProvider,
                private camera: Camera,
                private civilWorkDayProvider: CivilWorkDayProvider,
                protected injector: Injector,
                public navParams: NavParams) {
        super(navCtrl, navParams, injector);
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();

        if (!this.files) {
            this.files = [];
        }
        console.log('ionViewDidLoad CivilWorkDayAddEditPage');
        this.getFases();

        if (this.item.id_diaDia || this.item._id) {
            this.getDataFromLocalDatabase(true);
        }


        console.log("item", this.item);
    }

    attachFile(index) {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Anexar',
            buttons: [{
                icon: "md-image",
                text: 'Galeria',
                handler: () => {
                    this.loadFromGallery(index);
                }
            }, {
                icon: "md-camera",
                text: 'Câmera',
                handler: () => {
                    this.loadFromCamera(index);
                }
            }, {
                icon: "md-document",
                text: 'Meus arquivos',
                handler: () => {
                    this.loadFile(index);
                }
            }]
        });

        actionSheet.present();
    }

    addFileToFilesArray(index, file) {
        if (index != undefined) {
            file.id = this.files[index].id;
            file.descricao = this.files[index].descricao;
            this.files[index] = file;
        }
        else {
            this.files.push(file);
        }
    }

    addItems() {

        this.addItem(0);

    }

    updateItensCount() {
        this.item.anexos = this.files.length;
        return this.pouchControll.bulkDoc({
            obj: [this.item],
            type: "civilWorkDay"
        });
    }

    addItem(index) {
        var singleItem = this.files[index];
        singleItem.id_diaDia = this.item.id_diaDia;


        //cant edit attachments by now, so we skip files with id
        // if (singleItem.id_anexo) {
        //     if (this.files.length - 1 > index) {
        //         this.addItem(index + 1);
        //     }
        //     else {
        //         this.onSuccess();
        //     }
        //     return;
        // }


        var request = singleItem.id_anexo ? this.civilWorkDayProvider.editAttachment(singleItem, true) : this.civilWorkDayProvider.addAttachment(singleItem, true);

        request.subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                this.files[index].id_anexo = data.object.id_anexo;
                var item = {type: "attchments", obj: [data.object]};
                this.pouchControll.bulkDoc(item);

                if (this.files.length - 1 > index) {
                    this.addItem(index + 1);
                }
                else {
                    this.updateItensCount().then((data) => {
                        this.onSuccess();
                    })

                }
            }
            else {
                this.requestHelpersProvider.showErrorToast(data);
            }
        }, (error) => {

            this.requestHelpersProvider.showTryAgainDialog(() => {
                this.addItem(index);
            })
        });

    }


    private showFilePickerErrorMessage() {
        this.showMessage('Houve um erro, por favor tente novamente.');
    }

    loadFile(index) {

        return this.fileHelperProvider.loadFile().then((data) => {

            if (data) {
                var $this = this;
                let file = data;
                setTimeout(() => {
                    $this.addFileToFilesArray(index, file);
                }, 500);

            }
            else {
                console.log("error trying to get file data, data is null");
                this.showFilePickerErrorMessage();
            }

        }).catch((err) => {
            if (err.message == "Tipo de arquivo inválido") {
                err
                this.requestHelpersProvider.showMessage("Tipo de arquivo inválido.");
            }
            else {
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }


            console.log("error", err);
        });


        // return this.fileChooser.open()
        //     .then(uri => {
        //
        //         return this.getFileTitleFromUri(uri).then((fileName) => {
        //
        //             if (!fileName) {
        //                 this.loadingProvider.dismiss();
        //                 this.showFilePickerErrorMessage();
        //                 return;
        //             }
        //             else if (!this.isValidFileType(fileName)) {
        //                 throw new Error("Tipo de arquivo inválido");
        //             }
        //
        //             var file: any = {nome: fileName, type: "file",};
        //             this.loadingProvider.present();
        //
        //             return this.getFileAsBase64FromUri(uri).then((data) => {
        //                 console.log("FILE LOADED");
        //                 this.loadingProvider.dismiss();
        //                 if (data) {
        //                     file.arquivo = data.split(',')[1];
        //                     this.addFileToFilesArray(index, file);
        //                 }
        //                 else {
        //                     this.showFilePickerErrorMessage();
        //                 }
        //
        //             });
        //
        //
        //         });
        //
        //     })
        //     .catch((err: Error) => {
        //
        //         if (err.message == "Tipo de arquivo inválido") {
        //             this.requestHelpersProvider.showMessage("Tipo de arquivo inválido.");
        //         }
        //         else {
        //             this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
        //         }
        //
        //     });
    }

    getFases() {

        this.genericLocalDataProvider.getFasesByCivilWork().then((objects) => {
            this.steps = objects;
            if (this.item.fase_obra) {
                this.item.id_fase = this.item.fase_obra.id_fase;
            }
        });
    }

    add() {

        if (this.item.id_fase && this.item.id_fase == "none") {
            delete this.item.id_fase;
        }

        this.item.id_pessoa = this.userLocalProvider.user['id_usuario'];
        this.item.id_obra = this.civilWorkProvider.selected["id_obra"];
        this.item.id_empresa = this.userLocalProvider.company.id_empresa;

        var request = this.item.id_diaDia ? this.civilWorkDayProvider.edit(this.item, true) : this.civilWorkDayProvider.add(this.item, true);

        request.subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                data.object._id = this.item._id;
                data.object._rev = this.item._rev;
                this.item = data.object;

                var bdItem = {type: "civilWorkDay", obj: [this.item]};
                this.pouchControll.bulkDoc(bdItem).then((data) => {

                    if (this.files && this.files.length) {
                        this.addItems();
                    }
                    else {
                        this.onSuccess();
                    }

                });


            }
            else {
                this.requestHelpersProvider.showErrorToast(data);
            }
        }, (error) => {

            this.addOffline();

            // this.requestHelpersProvider.showTryAgainDialog(() => {
            //     this.add();
            // })
        });
    }

    addOffline() {
        this.item.usuario = this.userLocalProvider.user;
        this.item.data = moment().tz("America/Sao_Paulo").format();

        this.item.anexos = this.files ? this.files.length : 0;

        this.pouchControll.insert({
            obj: this.item,
            type: "civilWorkDay"
        }).then((data) => {
            if (data && data.length) {
                this.item.id_diaDia = this.item.id_diaDia ? this.item.id_diaDia : data[0].id;
                if (this.files && this.files.length) {
                    this.files = this.files.map((file, index) => {

                        file.id_diaDia = file.id_diaDia ? file.id_diaDia : this.item.id_diaDia;
                        file.id_obra = this.civilWorkProvider.selected["id_obra"];
                        file.id_empresa = this.userLocalProvider.company.id_empresa;
                        return file;
                    });

                    return this.pouchControll.insert({
                        obj: this.files,
                        type: "attchments"
                    }).then((data) => {
                        if (data && data.length) {
                            this.onSuccess();
                        }
                    });

                }
                else {
                    this.onSuccess();
                }
            }
        })
    }

    getAttachments() {
        this.civilWorkDayProvider.getAttachments(this.item.id_diaDia, false).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                this.saveOffline("attchments", data.objects).then((data) => {
                    this.getDataFromLocalDatabase(false);
                }).then((data) => {
                })
            }
            else {
                this.requestHelpersProvider.showErrorToast(data);
            }

            this.completeRefresh();

        }, (error) => {

            // this.requestHelpersProvider.showTryAgainDialog(() => {
            //     this.getAttachments();
            // })
        });
    }

    getDataFromLocalDatabase(fetchFromApi?) {




        let item = {
            type: "attchments",
            find: {id_diaDia: this.item.id_diaDia ? this.item.id_diaDia : this.item._id, deleted: {'$ne': true}}
        };
        this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Order getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            this.files = objects;

            // console.log("get data from localDb bring " + this.item.anexosArray.length + " results");

            if (objects == null || objects.length == 0) {
                console.log("no data from localdb saved, fetching from API");
            }

            // after getting itens from localDB it will try to fetch from api (even if it has data on localDb)
            // showLoading = will show loading spinner if theres no data on db (its probably the first time the user is using the app), else will fetch in background (no loading spinner).
            var showLoading = (objects == null || objects.length == 0) ? true : false;

            if (fetchFromApi) {
                this.getAttachments();
            }

        }).catch(error => {

        })
    }

    getFileImage(file) {
        if (file.caminhoArquivo) {
            return file.caminhoArquivo;
        }
        else if (file.type == 'image' && file.arquivo) {
            return file.arquivo && file.arquivo.indexOf('data:image') == -1 ?  'data:image/jpeg;base64,' + file.arquivo : file.arquivo;
        }
        else {
            return "";
        }
    }

    getFileThumb(file) {
        if (file.caminhoThumb) {
            return file.caminhoThumb;
        }
        else if (file.type == 'image') {
            return file.arquivo && file.arquivo.indexOf('data:image') == -1 ?  'data:image/jpeg;base64,' + file.arquivo : file.arquivo;
        }
        else {
            return "";
        }
    }

    getFileType(file): string {

        if (file.type == 'image') {
            return "image";
        }
        else if (!file.caminhoArquivo) {
            return "file";
        }

        var fileTypes = [
            {
                type: "image",
                extensions:
                    ['jpg', 'png', 'gif', 'jpeg', 'bmp']
            },
            {
                type: "file",
                extensions:
                    ['doc', 'docx', 'txt', 'pdf', 'zip', 'rar', 'psd', 'xls', 'xlsx', 'rtf', 'dwg']
            }
        ];

        var fileType = 'generic';

        var fileExtension = file.caminhoArquivo.split('.').pop();


        fileTypes.forEach((type, index) => {

            if (type.extensions.indexOf(fileExtension) != -1) {
                fileType = type.type;
            }
        });

        return fileType;
    }

    loadFromCamera(index) {

        this.fileHelperProvider.loadImage(this.fileHelperProvider.camera.PictureSourceType.CAMERA).then((data) => {

            if (data) {
                let file = data;
                this.addFileToFilesArray(index, file);
            }
            else {
                this.showFilePickerErrorMessage();
            }

        }).catch((err) => {
            console.log("error", err);
            if (err.message != "No Image Selected" && err != "No Image Selected") {
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }

        });
    }

    loadFromGallery(index) {

        this.fileHelperProvider.loadImage(this.fileHelperProvider.camera.PictureSourceType.PHOTOLIBRARY).then((data) => {

            if (data) {
                let file = data;
                this.addFileToFilesArray(index, file);
            }
            else {
                this.showFilePickerErrorMessage();
            }

        }).catch((err) => {
            console.log("error", err);
            if (err.message != "No Image Selected" && err != "No Image Selected") {
                this.requestHelpersProvider.showMessage("Houve um erro ao carregar o arquivo.");
            }

        });
    }


    deleteAtachment(index, file) {
        this.civilWorkDayProvider.deleteAttachment(file, true).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                this.deleteItemFromDb({
                    obj: file,
                    type: "attchments"
                }).then((data) => {

                    this.files.splice(index, 1);
                    this.item.anexos = this.files.length;
                    this.pouchControll.bulkDoc({
                        obj: [this.item],
                        type: "civilWorkDay"
                    }).then((data) => {

                        this.item.itens = this.files.length;
                        this.item.data = moment().tz("America/Sao_Paulo").format();
                        this.pouchControll.bulkDoc({
                            obj: [this.item],
                            type: "civilWorkOrder"
                        }).then((data) => {
                            this.showMessage('Item deletado com sucesso.');

                        })

                    })

                })

            }
            else {
                this.requestHelpersProvider.showErrorToast(data);
            }
        }, (error) => {


            this.deleteAttchmentOff(index, {
                obj: file,
                type: "attchments"
            });

        });
    }

    deleteAttchmentOff(index, item) {
        return this.deleteOff(item).then((data) => {
            this.files.splice(index, 1);
            this.item.anexos = this.files.length;
            this.item.data = moment().tz("America/Sao_Paulo").format();
            this.pouchControll.bulkDoc({
                obj: [this.item],
                type: "civilWorkDay"
            }).then((data) => {
                this.showMessage('Item deletado com sucesso.');

            })
        })
    }


    deleteItemFromDb(item: any) {

        return this.pouchdbOff.delete(item).then(
            resp => {
                return resp;

            }).catch((error) => {
            console.log(error);
            this.requestHelpersProvider.showMessage("Houve um erro, por favor tente novamente.");
        })

    }

    private deleteFile(index, file) {

        if (file.id_diaDia && file.id_anexo) {
            this.deleteAtachment(index, file);
        }
        else if (file.id_diaDia && !file.id_anexo) {
            this.deleteAttchmentOff(index, {
                obj: file,
                type: "attchments"
            });
        }
        else {
            this.files.splice(index, 1);
        }

    }

}
