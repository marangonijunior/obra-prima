import {Component} from '@angular/core';
import {
    AlertController, IonicPage, MenuController, ModalController, NavController, NavParams,
    Platform
} from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/empty'
import {Observable} from "rxjs/Observable";
import {UserLocalProvider} from "../../providers/user-local/user-local";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {Keyboard} from "@ionic-native/keyboard";
import * as $ from "jquery";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    user = {
        login: "renato.probst@rockapps.com.br",
        senha: "qwe123@"
    }

    users = [
        {
            login: "renato.probst@rockapps.com.br",
            senha: "qwe123@"
        },
        {
            login: "empresaprobst2@01tec.com",
            senha: "1fcf"
        }
    ]

    constructor(public navCtrl: NavController,
                private inAppBrowser: InAppBrowser,
                private keyboard: Keyboard,
                private platform: Platform,
                private modalCtrl: ModalController,
                public userLocalProvider: UserLocalProvider,
                public storage: Storage,
                public navParams: NavParams, private menu: MenuController, private alertCtrl: AlertController,
                public requestHelpersProvider: RequestHelpersProvider,
                public userProvider: UserProvider) {
    }

    ionViewDidEnter() {
        // the root left menu should be disabled on this page
        this.menu.enable(false);
        this.setKeyboardStuff();
        this.checkIfNeedToShowSlide();
    }

    ionViewWillLeave() {
        // enable the root left menu when leaving this page
        this.menu.enable(true);
    }

    onLoginClick(form) {

        if (form.valid) {
            this.login();
        }

    }

    checkIfNeedToShowSlide() {
        this.storage.get("showedSlider").then((showed) => {
            if (!showed) {
                this.showIntroModal();
            }

        })
    }

    showIntroModal() {
        let modal = this.modalCtrl.create("IntroSliderPage");
        modal.present();
    }


    setKeyboardStuff() {
        if (this.platform.is("android")) {
            this.keyboard.onKeyboardShow().subscribe((data) => {
                console.log("onKeyboardShow()");
                $("#login-content").removeClass("img-bg");
                $("#login-content").addClass("dark-bg");
            });

            this.keyboard.onKeyboardHide().subscribe((data) => {
                console.log("onKeyboardHide()");
                $("#login-content").addClass("img-bg");
                $("#login-content").removeClass("dark-bg");
            });
        }
    }


    onLoginSuccess(user) {

        this.navCtrl.push("CompanySelectListPage", {user: user});
    }

    login() {
        this.userProvider.login(this.user)
            .subscribe((data: any) => {
                console.log("result");
                console.log(data);

                if (this.requestHelpersProvider.successAndHasObject(data)) {


                    data.object.login = this.user.login;
                    this.onLoginSuccess(data.object);
                }
                else {
                    this.requestHelpersProvider.showErrorToast(data);
                }

            }, (error) => {
                console.log(error);


                this.userLocalProvider.getByEmail(this.user).then((users) => {
                    if (!users || users.length == 0) {
                        this.requestHelpersProvider.showTryAgainDialog(() => {
                            this.login();
                        });

                    }
                    else {
                        console.log("find saved user", users[0]);
                        this.onLoginSuccess(users[0]);
                    }
                });


            })
    }

    register() {
        this.user = this.user.login == this.users[0].login ? this.users[1] : this.users[0];
        //this.inAppBrowser.create("http://obraprimaweb.com.br", "_system");
    }

    forgetPassword(email) {
        this.userProvider.forgetPassword(email)
            .subscribe((data: any) => {
                console.log("result");
                console.log(data);

                if (this.requestHelpersProvider.successAndHasObject(data)) {

                    this.requestHelpersProvider.showMessage("Você receberá um e-mail com as informações para recuperação da sua senha.");
                }
                else {
                    this.requestHelpersProvider.showErrorToast(data);
                }

            }, (error) => {
                console.log(error);

                this.requestHelpersProvider.showTryAgainDialog(() => {
                    this.forgetPassword(email);
                });


            })
    }

    showForgetPasswordDialog() {
        let prompt = this.alertCtrl.create({
            title: 'Esqueci a senha',
            message: "Informe seu e-mail de acesso",
            inputs: [
                {
                    value: this.user.login,
                    name: 'email',
                    type: 'email',
                    placeholder: 'E-mail'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Enviar',
                    handler: data => {
                        if (data && data.email) {
                            this.forgetPassword(data.email);
                        }
                        console.log('Saved clicked', data);
                    }
                }
            ]
        });
        prompt.present();
    }

    private checkForUser() {
        this.userLocalProvider.getUserFromLocalDb().then((user) => {
            if (user) {
                this.navCtrl.setRoot("CivilWorkListPage");
            }

        });
    }
}
