import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CivilWorkDayDetailsPage } from './civil-work-day-details';
import {IonicImageViewerModule} from "ionic-img-viewer";
import {DirectivesModule} from "../../directives/directives.module";
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    CivilWorkDayDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CivilWorkDayDetailsPage),
    IonicImageViewerModule,
    DirectivesModule,
    ComponentsModule
  ],
})
export class CivilWorkDayDetailsPageModule {}
