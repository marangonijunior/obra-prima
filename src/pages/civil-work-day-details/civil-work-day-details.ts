import {Component, Injector} from '@angular/core';
import {ActionSheetController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CivilWorkDayProvider} from "../../providers/civil-work-day/civil-work-day";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {GenericDetailsPage} from "../generic-details/generic-details";
import {PouchdbSync} from "../../providers/pouchdb/pouchdb-sync";
import {CivilWorkAttachmentsProvider} from "../../providers/civil-work-attachments/civil-work-attachments";


/**
 * Generated class for the CivilWorkDayDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-day-details',
    templateUrl: 'civil-work-day-details.html',
})
export class CivilWorkDayDetailsPage extends GenericDetailsPage {

    isLoaded = false;

    constructor(public navCtrl: NavController,
                private civilWorkDayProvider: CivilWorkDayProvider,
                private civilWorkAttachmentsProvider: CivilWorkAttachmentsProvider,
                private inAppBrowser: InAppBrowser,
                protected injector: Injector,
                public navParams: NavParams) {
        super(navCtrl, navParams, injector);
        this.editPage = "CivilWorkDayAddEditPage";

    }

    updateItensCount() {
        this.item.anexos = this.item.anexosArray ? this.item.anexosArray.length : 0;
        return this.pouchControll.bulkDoc({
            obj: [this.item],
            type: "civilWorkDay"
        });
    }


    ionViewDidEnter() {
        super.ionViewDidEnter();
    }

    getData() {
        this.getDataFromLocalDatabase(true);

    }

    checkDeletedItens(type, idField, provider) {

        this.pouchControll.pouchdbOff.customFind({
            type: 'attchments',
            selector: {}
        }).then((data) => {

            let idList = data.docs.map((item) => {
                return item[idField];
            })

            provider.getDeleted(idList, false).subscribe((data: any) => {
                if (this.requestHelpersProvider.successAndHasObject(data)) {


                    var item = {
                        type: type,
                        selector: {$or: []}
                    };

                    data.object.forEach((id, index) => {
                        item.selector.$or.push({id_diaDia: id});
                    })


                    return this.pouchControll.pouchdbOff.customFind(item).then((data) => {
                        if (data.docs) {
                            data = data.docs.map((item, index) => {
                                item._deleted = true;
                                return item;
                            })

                            this.pouchControll.pouchdbOff.bulkDoc({
                                type: type,
                                obj: data
                            }).then(() => {
                                this.getDataFromLocalDatabase(false);
                            })
                        }

                        console.log(data);
                    });

                }
                this.completeRefresh();

            }, (error) => {
            })

        });

    }

    getDataFromApi(showLoading) {
        this.civilWorkDayProvider.getAttachments(this.item.id_diaDia, showLoading).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                this.item.anexosArray = data.objects;
                var item = {type: "attchments", obj: this.item.anexosArray};
                this.pouchControll.bulkDoc(item).then((data) => {
                    this.checkDeletedItens("attchments", "id_anexo", this.civilWorkAttachmentsProvider);
                })
            }
            this.completeRefresh();
        }, (error) => {
            // se der erro de conexão ou n tiver internet cai aqui.
            this.completeRefresh();
            this.getDataFromLocalDatabase(false);
            // if (this.item.anexos > 0 && (!this.item.anexosArray || this.item.anexosArray == 0)) {
            //     this.requestHelpersProvider.showMessage("Este item possúi anexos que ainda não foram sincronizados do servidor.");
            // }
        })
    }

    getAttchments(fetchFromApi) {
        let item = {
            type: "attchments",
            find: {id_diaDia: this.item.id_diaDia ? this.item.id_diaDia : this.item._id, deleted: {'$ne': true}}
        };


        return this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Order getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            this.item.anexosArray = objects;
            this.isLoaded = true;

            // console.log("get data from localDb bring " + this.item.anexosArray.length + " results");

            if (objects == null || objects.length == 0) {
                console.log("no data from localdb saved, fetching from API");
            }

            // after getting itens from localDB it will try to fetch from api (even if it has data on localDb)
            // showLoading = will show loading spinner if theres no data on db (its probably the first time the user is using the app), else will fetch in background (no loading spinner).
            var showLoading = (objects == null || objects.length == 0) ? true : false;

            if (fetchFromApi) {
                this.getDataFromApi(false);
            }
            else {
                this.updateItensCount();
            }

        }).catch(error => {

        })
    }

    getDataFromLocalDatabase(fetchFromApi?) {
        let item = {
            type: "civilWorkDay",
            find: {}
        };

        item.find = this.item.id_diaDia ? {id_diaDia: this.item.id_diaDia} : {_id: this.item._id}

        this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Order getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            return objects;


        }).then((objects) => {
            if (objects && objects.length > 0) {
                this.item = objects[0];
                return this.getAttchments(fetchFromApi);
            }
        }).catch(error => {

        })
    }

    getFileThumb(file) {
        if (file.caminhoThumb) {
            return file.caminhoThumb;
        }
        else if (file.type == 'image') {
            return file.arquivo && file.arquivo.indexOf('data:image') == -1 ?  'data:image/jpeg;base64,' + file.arquivo : file.arquivo;
        }
        else {
            return "";
        }
    }

    
    getFileImage(file) {
        if (file.caminhoArquivo) {
            return file.caminhoArquivo;
        }
        else if (file.type == 'image' && file.arquivo) {
            return file.arquivo && file.arquivo.indexOf('data:image') == -1 ?  'data:image/jpeg;base64,' + file.arquivo : file.arquivo;
        }
        else {
            return "";
        }
    }

    getFileType(file): string {

        if (file.type == 'image') {
            return "image";
        }
        else if (!file.caminhoArquivo) {
            return "file";
        }

        var fileTypes = [
            {
                type: "image",
                extensions:
                    ['jpg', 'png', 'gif', 'jpeg', 'bmp']
            },
            {
                type: "file",
                extensions:
                    ['doc', 'docx', 'txt', 'pdf', 'zip', 'rar', 'psd', 'xls', 'xlsx', 'rtf', 'dwg']
            }
        ];

        var fileType = 'generic';

        var fileExtension = file.caminhoArquivo.split('.').pop();


        fileTypes.forEach((type, index) => {

            if (type.extensions.indexOf(fileExtension) != -1) {
                fileType = type.type;
            }
        });

        return fileType;
    }

    downloadFile(file) {
        this.inAppBrowser.create(file.caminhoArquivo, "_system");
    }


    delete() {
        this.civilWorkDayProvider.remove(this.item, true).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                this.deleteItemFromDb({
                    obj: this.item,
                    type: "civilWorkDay"
                }).then((data) => {
                    this.requestHelpersProvider.showMessage("Operação realizada com sucesso");
                    this.navCtrl.pop();
                })


            }
        }, (error) => {
            // se der erro de conexão ou n tiver internet cai aqui. Chama a função pra buscar os dados no banco de dados.
            this.deleteOff();
        })
    }

    deleteOff() {

        this.item.deleted = true;

        return this.pouchControll.insert({
            obj: this.item,
            type: "civilWorkDay"
        }).then((data) => {
            if (data && data.length) {
                this.item.id_diaDia = this.item.id_diaDia ? this.item.id_diaDia : data[0].id;

                this.requestHelpersProvider.showMessage("Operação realizada com sucesso");
                this.navCtrl.pop();
            }
        })

    }
}
