import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {CivilWorkOrderAddEditPage} from './civil-work-order-add-edit';
import {DirectivesModule} from "../../directives/directives.module";
import {ComponentsModule} from "../../components/components.module";
import {NgPipesModule} from "ngx-pipes";

@NgModule({
    declarations: [
        CivilWorkOrderAddEditPage,
    ],
    imports: [
        DirectivesModule,
        ComponentsModule,
        IonicPageModule.forChild(CivilWorkOrderAddEditPage),
        NgPipesModule
    ],
})
export class CivilWorkOrderAddEditPageModule {
}
