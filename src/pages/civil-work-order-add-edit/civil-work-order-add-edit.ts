import {Component, Injector} from '@angular/core';
import {ActionSheetController, IonicPage, NavController, NavParams} from 'ionic-angular';
import * as _ from 'underscore';
import {GenericAddEditPage} from "../generic-add-edit/generic-add-edit";
import {CivilWorkOrderProvider} from "../../providers/civil-work-order/civil-work-order";
import moment from "moment-timezone";

/**
 * Generated class for the CivilWorkOrderAddEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-civil-work-order-add-edit',
    templateUrl: 'civil-work-order-add-edit.html',
})
export class CivilWorkOrderAddEditPage extends GenericAddEditPage {

    minDate = moment().format();
    maxDate = moment().add(1, 'y').format("YYYY");
    item: any = {
        itensArray: [],
        frame: false
    };
    test;
    myCallbackFunction: (_params) => Promise<any>;

    steps = []

    services = []

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public civilWorkOrderProvider: CivilWorkOrderProvider,
                protected injector: Injector) {

        super(navCtrl, navParams, injector);
    }


    // save as draft
    onSaveClick(form) {
        if (form.valid) {
            this.addOffline(true);
        } else {
            _.map(form.form.controls, function (val, key) {
                form.form.controls[key].markAsDirty();
            });
        }

    }


    getFases() {
        this.genericLocalDataProvider.getFasesByCivilWork().then((objects) => {
            this.steps = objects;
        })
    }

    getServicos() {
        if (!this.item.id_fase || this.item.id_fase == "none") {
            return;
        }

        this.genericLocalDataProvider.getServicosByFase(this.item.id_fase).then((objects) => {
            this.services = objects;
            if (!this.services || !this.services.length) {
                if (this.item.id_fase) {
                    delete this.item.id_fase;
                    delete this.item.id_servico;
                }
            }
        })
    }

    showItemOptions(item, index) {
        let actionSheet = this.actionSheetCtrl.create({

            buttons: [
                {
                    text: 'Remover item',
                    handler: () => {
                        this.removeItem(item, index);
                    }
                },
                {
                    text: 'Editar item',
                    handler: () => {
                        this.editItem(item);
                    }
                }
            ]
        });
        actionSheet.present();
    }

    ionViewDidLoad() {
        super.ionViewDidLoad();

        this.getFases();
    }


    editItem(item) {
        var $this = this;
        this.myCallbackFunction = function (item) {
            return new Promise((resolve, reject) => {
                $this.addItemToList(item);
                console.log(this.item);
                resolve();
            });
        }

        this.navCtrl.push("CivilWorkOrderItemAddEditPage", {
            callback: this.myCallbackFunction,
            order: this.item,
            item: item
        });
    }

    onAddItemClick() {
        var $this = this;
        this.myCallbackFunction = function (item) {
            return new Promise((resolve, reject) => {
                $this.addItemToList(item);
                console.log(this.item);
                resolve();
            });
        }

        this.navCtrl.push("CivilWorkOrderItemAddEditPage", {
            callback: this.myCallbackFunction,
            order: this.item,
            services: this.services,
            steps: this.steps
        });

    }


    doCallbackFunction() {
        if (this.navParams.get("addCallbackFunction")) {
            var item = JSON.parse(JSON.stringify(this.item));
            if (item.situacao.id_situacao == "draftApp") {
                delete item.id_solicitacao;
            }

            this.navParams.get("addCallbackFunction")(item);
        }
    }

    onSuccess() {
        this.doCallbackFunction();
        super.onSuccess();
    }


    add() {

        if (this.item.id_fase && this.item.id_fase == 'none') {
            delete this.item.id_fase;
        }
        if (this.item.id_servico && this.item.id_servico == 'none') {
            delete this.item.id_servico;
        }

        this.item.id_pessoa = !this.item.id_pessoa ? this.userLocalProvider.user['id_usuario'] : this.item.id_pessoa;
        this.item.id_obra = !this.item.id_obra ? this.civilWorkProvider.selected["id_obra"] : this.item.id_obra;
        this.item.id_empresa = !this.item.id_empresa ? this.userLocalProvider.company.id_empresa : this.item.id_empresa;


        var request = this.item.id_solicitacao && this.item.situacao.id_situacao != "draftApp" ? this.civilWorkOrderProvider.edit(this.item, true) : this.civilWorkOrderProvider.add(this.item, true);

        request.subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                data.object._id = this.item._id;
                data.object.itensArray = this.item.itensArray;
                data.object._rev = this.item._rev;
                this.item = data.object;

                var bdItem = {type: "civilWorkOrder", obj: [this.item]};
                this.pouchControll.bulkDoc(bdItem).then((data) => {
                    console.log("success savingAfterAddEdit", data);

                    if (this.item.itensArray && this.item.itensArray.length) {
                        this.addItems();
                    }
                    else {
                        this.onSuccess();
                    }

                }).catch((error) => {
                    console.log("error SavingAfterAddEdit", error);
                })


            }
            else {
                this.requestHelpersProvider.showErrorToast(data);
            }
        }, (error) => {

            this.addOffline();

            // this.requestHelpersProvider.showTryAgainDialog(() => {
            //     this.add();
            // })
        });

    }

    addOffline(isDraft?) {
        this.item.usuario = !this.item.usuario ? this.userLocalProvider.user : this.item.usuario;
        this.item.data = moment().tz("America/Sao_Paulo").format();


        if (isDraft) {
            this.item.situacao = {
                cor_hex: "#d0d0d0",
                id_situacao: "draftApp",
                nome: "Rascunho"
            };

            this.item.id_pessoa = !this.item.id_pessoa ? this.userLocalProvider.user['id_usuario'] : this.item.id_pessoa;
            this.item.id_obra = !this.item.id_obra ? this.civilWorkProvider.selected["id_obra"] : this.item.id_obra;
            this.item.id_empresa = !this.item.id_empresa ? this.userLocalProvider.company.id_empresa : this.item.id_empresa;
        }
        else if (!this.item.situacao || this.item.situacao.id_situacao == "draftApp") {
            this.item.situacao = this.item.situacao ? this.item.situacao : {
                cor_hex: "#01579B",
                id_situacao: 132,
                nome: "Aberta"
            };
        }

        this.item.itens = this.item.itensArray ? this.item.itensArray.length : 0;

        this.pouchControll.insert({
            obj: this.item,
            type: "civilWorkOrder"
        }).then((data) => {
            if (data && data.length) {
                //this.item.id_solicitacao = this.item.id_solicitacao ? this.item.id_solicitacao : data[0].id;
                this.item._id = data[0].id;
                if (this.item.itensArray && this.item.itensArray.length) {

                    this.item.itensArray = this.item.itensArray.map((file, index) => {

                        file.id_solicitacao = file.id_solicitacao ? file.id_solicitacao : this.item.id_solicitacao ? this.item.id_solicitacao : data[0].id;
                        file.id_usuario = !file.id_usuario ? this.userLocalProvider.user.id_usuario : file.id_usuario;
                        file.id_obra = !file.id_obra ? this.civilWorkProvider.selected["id_obra"] : file.id_obra;
                        file.id_empresa = !file.id_empresa ? this.userLocalProvider.company.id_empresa : file.id_empresa;

                        if (isDraft) {
                            file.draftApp = true;
                        }
                        else {
                            delete file.draftApp;
                        }

                        return file;
                    });

                    return this.pouchControll.insert({
                        obj: this.item.itensArray,
                        type: "civilWorkOrderItems"
                    }).then((data) => {
                        if (data && data.length) {
                            this.onSuccess();
                        }
                    });

                }
                else {
                    this.onSuccess();
                }
            }
        })
    }

    addItems() {

        this.addItem(0);
    }

    addItem(index) {
        var singleItem = this.item.itensArray[index];
        singleItem.id_solicitacao = this.item.id_solicitacao;
        singleItem.id_usuario = this.userLocalProvider.user.id_usuario;

        var request = singleItem.id_item ? this.civilWorkOrderProvider.editItem(singleItem, true) : this.civilWorkOrderProvider.addItem(singleItem, true);

        request.subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                var item = {type: "civilWorkOrderItems", obj: [data.object]};
                this.pouchControll.bulkDoc(item);

                if (this.item.itensArray.length - 1 > index) {
                    this.addItem(index + 1);
                }
                else {

                    this.item.itens = this.item.itensArray.length;
                    this.pouchControll.bulkDoc({
                        obj: [this.item],
                        type: "civilWorkOrder"
                    }).then((data) => {
                        this.onSuccess();
                    });
                }
            }
        }, (error) => {

            this.requestHelpersProvider.showTryAgainDialog(() => {
                this.addItem(index)
            })
        });

    }

    addItemToList(item: any) {

        if (!this.item.itensArray) {
            this.item.itensArray = [];
        }

        if (item._id) {
            var index = _.findIndex(this.item.itensArray, (listItem: any) => {
                if (item._id) {
                    return listItem._id == item._id;
                }
            });

            console.log("addItemToList", index, item)
            if (index != -1) {
                this.item.itensArray[index] = item;
            } else {
                this.item.itensArray.push(item);
            }
        }
        else if (item.localId) {
            var index = _.findIndex(this.item.itensArray, (listItem: any) => {
                if (item.id_solicitacao) {
                    return listItem.id_solicitacao == item.id_solicitacao;
                }
                else if (item.localId) {
                    return listItem.localId == item.localId;
                }
            });

            console.log("addItemToList", index, item)
            if (index != -1) {
                this.item.itensArray[index] = item;
            } else {
                this.item.itensArray.push(item);
            }

        } else {
            this.item.itensArray.push(item);
        }
    }

    removeItem(item: any, index) {
        if (item.id_solicitacao && item.id_item) {
            this.deleteItem(item, index);
        }
        else if (item.id_solicitacao && !item.id_item) {
            this.deleteOrderItemOff(index, {
                obj: item,
                type: "civilWorkOrderItems"
            });
        }
        else if (item.localId) {
            this.item.itensArray.splice(index, 1);
        }
    }

    deleteItemFromDb(item: any) {

        return this.pouchdbOff.delete(item).then(
            resp => {
                return resp;

            }).catch((error) => {
            console.log(error);
            this.requestHelpersProvider.showMessage("Houve um erro, por favor tente novamente.");
        })

    }

    deleteOrderItemOff(index, item) {
        return this.deleteOff(item).then((data) => {
            this.item.itensArray.splice(index, 1);

            this.updateItensCount().then((data) => {
                this.showMessage('Item deletado com sucesso.');

            })
        })
    }

    updateItensCount() {
        this.item.itens = this.item.itensArray.length;
        return this.pouchControll.bulkDoc({
            obj: [this.item],
            type: "civilWorkOrder"
        });
    }


    deleteItem(item, index) {
        this.civilWorkOrderProvider.removeItem(item, true).subscribe((data) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                this.deleteItemFromDb({
                    obj: item,
                    type: "civilWorkOrderItems"
                }).then((data) => {
                    this.item.itensArray.splice(index, 1);

                    this.updateItensCount().then((data) => {
                        this.showMessage('Item deletado com sucesso.');

                    })

                })

            }
            else {
                this.requestHelpersProvider.showErrorToast(data);
            }
        }, (error) => {

            this.deleteOrderItemOff(index, {
                obj: item,
                type: "civilWorkOrderItems"
            });
        });
    }
}
