import {Component, Injector} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {GenericPage} from "../generic/generic";

/**
 * Generated class for the GenericFilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-generic-filter',
    templateUrl: 'generic-filter.html',
})
export class GenericFilterPage extends GenericPage {

    searchObject;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                protected viewCtrl: ViewController,
                protected injector: Injector
    ) {
        super(navCtrl, navParams, injector);
    }

    ionViewDidEnter() {
        if (this.viewCtrl.data.searchObject) {
            this.searchObject = this.viewCtrl.data.searchObject;
        }
        console.log(this.viewCtrl.data);
    }

    close() {
        this.viewCtrl.dismiss();
    }

    doFilter(form) {
        console.log("doFilter");
        this.beforeSend();
        this.viewCtrl.dismiss({searchObject: this.searchObject});
    }

    beforeSend() {

    }

}
