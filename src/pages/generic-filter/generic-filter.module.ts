import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GenericFilterPage } from './generic-filter';
import {NgPipesModule} from "ngx-pipes";

@NgModule({
  declarations: [
    GenericFilterPage,
  ],
  imports: [
    IonicPageModule.forChild(GenericFilterPage),
      NgPipesModule
  ],
})
export class GenericFilterPageModule {}
