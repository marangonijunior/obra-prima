import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, LOCALE_ID, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {UserProvider} from '../providers/user/user';
import {ApiProvider} from '../providers/api/api';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {MyHttpInterceptor} from "../http-interceptor/my-http-interceptor";
import {LoadingProvider} from "../providers/loading/loading";
import {IonicStorageModule} from "@ionic/storage";
import {FormsModule} from "@angular/forms";
import {RequestHelpersProvider} from '../providers/request-helpers/request-helpers';
import {CivilWorkProvider} from '../providers/civil-work/civil-work';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {GeneralProvider} from '../providers/general/general';


import localePt from '@angular/common/locales/pt';
import {registerLocaleData} from "@angular/common";
import {IonicImageViewerModule} from "ionic-img-viewer";
import {NgPipesModule} from "ngx-pipes";
import {CivilWorkDayProvider} from '../providers/civil-work-day/civil-work-day';
import {InAppBrowser} from '@ionic-native/in-app-browser';
import {CivilWorkOrderProvider} from '../providers/civil-work-order/civil-work-order';
import {CivilWorkGalleryProvider} from '../providers/civil-work-gallery/civil-work-gallery';
import {IonicImageLoader} from "ionic-image-loader";


import {PouchdbOff} from "../providers/pouchdb/pouchdb-off";
import {PouchdbSync} from "../providers/pouchdb/pouchdb-sync";
import {PouchControll} from "../providers/pouchdb/pouchdb-controll";

import {Network} from '@ionic-native/network';
import {FileHelperProvider} from '../providers/file-helper/file-helper';
import {UserLocalProvider} from '../providers/user-local/user-local';
import {GenericLocalDataProvider} from '../providers/generic-local-data/generic-local-data';
import {PouchdbSyncControl} from "../providers/pouchdb/pouchdb-sync-control";
import {Keyboard} from "@ionic-native/keyboard";
import {ComponentsModule} from "../components/components.module";
import {CivilWorkAttachmentsProvider} from '../providers/civil-work-attachments/civil-work-attachments';
import {CivilWorkOrderItensProvider} from '../providers/civil-work-order-itens/civil-work-order-itens';
import {PouchdbSyncControlSingle} from "../providers/pouchdb/pouchdb-sync-control-single";
import {FullSyncControll} from "../providers/pouchdb/full-sync-controll";
import {IOSFilePicker} from '@ionic-native/file-picker';
import {HTTP} from "@ionic-native/http";


registerLocaleData(localePt);

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage
    ],
    imports: [
        NgPipesModule,
        IonicImageLoader.forRoot(),
        IonicStorageModule.forRoot(),
        BrowserModule,
        HttpClientModule,
        NoopAnimationsModule,
        ComponentsModule,
        FormsModule,
        IonicModule.forRoot(MyApp, {
            monthNames: ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'],
            monthShortNames: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
            dayNames: ['domingo', 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira'],
            dayShortNames: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex'],
            platforms: {
                ios: {
                    backButtonText: 'Voltar',
                    scrollAssist: false,    // Valid options appear to be [true, false]
                    autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
                }
            }


        }),
        IonicImageViewerModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
    ],
    providers: [
        {provide: LOCALE_ID, useValue: 'pt-BR'},
        StatusBar,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MyHttpInterceptor,
            multi: true
        },
        SplashScreen,
        Network,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        UserProvider,
        LoadingProvider,
        ApiProvider,
        RequestHelpersProvider,
        CivilWorkProvider,
        GeneralProvider,
        CivilWorkDayProvider,
        InAppBrowser,
        CivilWorkOrderProvider,
        CivilWorkGalleryProvider,
        PouchdbOff,
        PouchdbSync,
        PouchControll,
        PouchdbSyncControl,
        PouchdbSyncControlSingle,
        FullSyncControll,
        FileHelperProvider,
        UserLocalProvider,
        GenericLocalDataProvider,
        Keyboard,
        CivilWorkAttachmentsProvider,
        CivilWorkOrderItensProvider,
        IOSFilePicker,
        HTTP
    ]
})
export class AppModule {
}
