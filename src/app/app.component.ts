import {Component, ViewChild} from '@angular/core';
import {AlertController, Events, ModalController, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {UserProvider} from "../providers/user/user";
import {CivilWorkProvider} from "../providers/civil-work/civil-work";

import {PouchdbOff} from "../providers/pouchdb/pouchdb-off";
import {PouchdbSync} from "../providers/pouchdb/pouchdb-sync";
import {RequestHelpersProvider} from "../providers/request-helpers/request-helpers";
import {UserLocalProvider} from "../providers/user-local/user-local";
import {PouchdbSyncControl} from "../providers/pouchdb/pouchdb-sync-control";
import {GeneralProvider} from "../providers/general/general";

import { concat } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {FullSyncControll} from "../providers/pouchdb/full-sync-controll";

@Component({
    templateUrl: 'app.html',
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any;

    pages: Array<any>;
    private showedAlert: boolean;
    private confirmAlert: any;

    constructor(
        public platform: Platform,
                public statusBar: StatusBar,
                public userProvider: UserProvider,
                public userLocalProvider: UserLocalProvider,
                public alertCtrl: AlertController,
                public civilWorkProvider: CivilWorkProvider,
                public generalProvider: GeneralProvider,
                public events: Events,
                public requestHelpersProvider: RequestHelpersProvider,
                public pouchdbOff: PouchdbOff,
                public fullSyncControll: FullSyncControll,
                public modalCtrl: ModalController,
                public splashScreen: SplashScreen) {

        this.pouchdbOff.createPouchDB();


        this.initializeApp();

        this.userLocalProvider.getUserFromLocalDb().then((user) => {
            this.rootPage = user ? "CivilWorkListPage" : "LoginPage";
        })

        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'Obras', icon: "domain", component: "CivilWorkListPage"},
            {title: 'Dia a dias', icon: "comment", component: "CivilWorkDayListPage", needCivilWorkSelected: true},
            {title: 'Dia a dias', icon: "comment", component: "CivilWorkDayListPage", needCivilWorkSelected: true, id: "introSlider"},
            {title: 'Galeria', icon: "insert_photo", component: "CivilWorkGalleryPage", needCivilWorkSelected: true},
            {
                title: 'Solicitações de compras',
                icon: "shopping_cart",
                component: "CivilWorkOrderListPage",
                needCivilWorkSelected: true
            },
            {
                title: 'Alterar empresa/filial',
                icon: "compare_arrows",
                component: "CompanySelectListPage",
                id: "chooseCompany"
            },
            {title: 'Sobre', icon: "info", component: "AboutPage"}
        ];

        events.subscribe('user:logoff', () => {
            this.logOff();
        });


        // if (this.syncing) {
        //     this.fullSyncControll.syncOnBackground();
        // }



        //this.showIntroModal();

    }

    showIntroModal() {
        let modal = this.modalCtrl.create("IntroSliderPage");
        modal.onDidDismiss(data => {
            this.nav.setRoot("LoginPage");
        });
        modal.present();
    }

    openPage(page) {

        if (page.id == "introSlider") {
            this.showIntroModal();
            return;
        }
        else if (page.id == "chooseCompany") {

            let modal = this.modalCtrl.create(page.component, {modal: true});
            modal.onDidDismiss(data => {
                this.nav.setRoot("CivilWorkListPage");
            });
            modal.present();


            return;
        }
        else if (page.needCivilWorkSelected) {
            this.civilWorkProvider.getSelected().then((data) => {

                if (data) {
                    this.nav.push(page.component);
                }
                else {
                    this.nav.setRoot("CivilWorkListPage");
                }


            });
            return;
        }
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            //this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.setBackButtonConfirmation();
        });
    }

    logOff() {
        this.fullSyncControll.stopSyncIfNeed();
        this.userLocalProvider.removeUserFromLocalDb().then((result) => {
            //this.pouchdbOff.remove();
            this.civilWorkProvider.removeSelected();
            this.nav.setRoot("LoginPage");
        })
    }

    sync() {
        this.civilWorkProvider.getDataFromApiAndSyncToLocalDatabase(true).subscribe((savedSuccessful: any) => {
            this.events.publish('refresh', false);
        }, (error) => {
            this.requestHelpersProvider.showTryAgainDialog(() => {
                this.sync();
            })
        })

    }

    fullSync() {
        this.civilWorkProvider.getDataFromApiAndSyncToLocalDatabase(true).subscribe((savedSuccessful: any) => {
            this.events.publish('refresh', false);
        }, (error) => {
            this.requestHelpersProvider.showTryAgainDialog(() => {
                this.sync();
            })
        })

    }

    private setBackButtonConfirmation() {

        // Confirm exit
        this.platform.registerBackButtonAction(() => {
            if (this.nav.length() == 1) {
                if (!this.showedAlert) {
                    this.confirmExitApp();
                } else {
                    this.showedAlert = false;
                    this.confirmAlert.dismiss();
                }
            }
            else {
                this.nav.pop();
            }


        });

    }

    confirmExitApp() {

        this.confirmAlert = this.alertCtrl.create({
            title: "Sair",
            message: "Tem certeza que deseja sair do aplicativo?",
            buttons: [
                {
                    text: 'Cancelar',
                    handler: () => {
                        console.log('Cancel clicked');
                        this.showedAlert = false;
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        console.log('yes clicked');
                        this.platform.exitApp();
                    }
                }
            ]
        });
        this.confirmAlert.present();
        this.showedAlert = true;
    }
}
