import {Component, Input} from '@angular/core';

/**
 * Generated class for the RoundLetterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'round-letter',
  templateUrl: 'round-letter.html'
})
export class RoundLetterComponent {

  @Input() text: string;

  constructor() {
  }

}
