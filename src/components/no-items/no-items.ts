import {Component, Input} from '@angular/core';
import {PouchControll} from "../../providers/pouchdb/pouchdb-controll";
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";

/**
 * Generated class for the NoItemsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'no-items',
    templateUrl: 'no-items.html'
})
export class NoItemsComponent {

    @Input() onAdd: () => void;
    @Input() list: any;
    @Input() type: any;
    lastSyncDate;

    constructor(
        private pouchControll: PouchControll,
        private civilWorkProvider: CivilWorkProvider,

    ) {

    }

    ngOnInit() {
        if (this.type) {
            this.getLastSyncDate();
        }
    }

    getLastSyncDate() {
        if (!this.type) {
            return "";
        }

       // var url = "http://op-des-api.01tec.com.br/api/";
        var url = "http://localhost:8100/api/";
        switch (this.type) {
            case "civilWorkDay":
                url = url + 'diaDias/' + this.civilWorkProvider.selected.id_obra;
                break;
            case "civilWorkOrder":
                url = url + 'solicitacoes/' + this.civilWorkProvider.selected.id_obra;
                break;
            case "civilWorkGallery":
                url = url + 'galerias/' + this.civilWorkProvider.selected.id_obra;
                break;
            case "civilWork":
                url = url + 'diaDias/' + this.civilWorkProvider.selected.id_obra;
                break;
            default:
                break;

        }

        console.log("url", url);


        this.pouchControll.getSyncDate(url).then((lastSyncDate) => {
            this.lastSyncDate = lastSyncDate;
        });

    }

    getLastSync() {
        if (this.type) {

        }
    }

}
