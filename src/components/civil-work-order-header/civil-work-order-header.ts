import {Component, Input} from '@angular/core';

/**
 * Generated class for the CivilWorkOrderHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'civil-work-order-header',
  templateUrl: 'civil-work-order-header.html'
})
export class CivilWorkOrderHeaderComponent {


  @Input() order;
  @Input() full;

  constructor() {
  }

}
