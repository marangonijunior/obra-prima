import {Component, Input} from '@angular/core';

/**
 * Generated class for the CivilWorkDayHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'civil-work-day-header',
  templateUrl: 'civil-work-day-header.html'
})
export class CivilWorkDayHeaderComponent {

  @Input() item;
  @Input() full;

  constructor() {
  }

}
