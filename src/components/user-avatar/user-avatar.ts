import {Component, Input} from '@angular/core';

/**
 * Generated class for the UserAvatarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'user-avatar',
  templateUrl: 'user-avatar.html'
})
export class UserAvatarComponent {


  @Input() img;
  @Input() text;

  constructor() {
    console.log('Hello UserAvatarComponent Component');
    this.text = 'Hello World';
  }

}
