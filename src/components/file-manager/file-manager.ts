import {Component, Input} from '@angular/core';
import {ActionSheetController, AlertController, FabContainer, ModalController, NavController} from "ionic-angular";
import * as _ from 'underscore';
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {CivilWorkGalleryProvider} from "../../providers/civil-work-gallery/civil-work-gallery";
import {RequestHelpersProvider} from "../../providers/request-helpers/request-helpers";
import * as $ from 'jquery';
import {PouchControll} from "../../providers/pouchdb/pouchdb-controll";
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";
import {PouchdbOff} from "../../providers/pouchdb/pouchdb-off";
import {UserProvider} from "../../providers/user/user";
import {UserLocalProvider} from "../../providers/user-local/user-local";
import {ImageViewerController} from "ionic-img-viewer";
import moment from "moment-timezone";


// @Component({
//     selector: 'file-add-modal',
//     templateUrl: 'file-add-modal.html'
// })
// export class FileAddModalComponent {
//     constructor() {
//
//     }
// }

/**
 * Generated class for the FileManagerComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'file-manager',
    templateUrl: 'file-manager.html'
})
export class FileManagerComponent {

    @Input() folder: any = {}
    public selected = [];
    @Input() selectable: boolean = false;
    @Input() showFiles: boolean = true;
    @Input() showFolders: boolean = true;
    showVisible: boolean = true;
    showInvisible: boolean = true;
    @Input() orderAscDesc = '+';
    @Input() orderProperty = 'title';
    history = [];
    filter;
    afterSetFilter: (filter) => void;
    fabContainer: FabContainer;
    filterModel;


    constructor(private actionSheetCtrl: ActionSheetController,
                public modalCtrl: ModalController,
                private inAppBrowser: InAppBrowser,
                private civilWorkGalleryProvider: CivilWorkGalleryProvider,
                private civilWorkProvider: CivilWorkProvider,
                private userProvider: UserProvider,
                private imageViewerCtrl: ImageViewerController,
                private userLocalProvider: UserLocalProvider,
                private pouchdbOff: PouchdbOff,
                private requestHelpersProvider: RequestHelpersProvider,
                private pouchControll: PouchControll,
                private navController: NavController,
                private alertCtrl: AlertController) {
        console.log('Hello FileManagerComponent Component');
    }

    ngOnChanges() {
    }


    openImage(file, imageToView, event) {
        const viewer = this.imageViewerCtrl.create(document.createElement("IMG"), {fullResImage: this.getFileImage(file)});
        viewer.present();
    }

    getFileImage(file) {
        if (file.caminhoArquivo) {
            return file.caminhoArquivo;
        }
        else if (file.type == 'image' && file.arquivo) {
            return file.arquivo && file.arquivo.indexOf('data:image') == -1 ?  'data:image/jpeg;base64,' + file.arquivo : file.arquivo;
        }
        else {
            return "";
        }
    }


    customFind() {
        console.log("filterModel", this.filterModel);
        let item = {
            type: "civilWorkGallery",
            selector: this.filterModel
        }
        this.pouchdbOff.customFind(item).then((data: any) => {

            let objects = data.docs.map(row => {
                return row;
            });

            console.log(data);

            var folder = {
                galeriasFilhas: objects,
                nome: "Raiz"
            }

            this.folder = folder;

        });
        return;
    }

    showFile(file) {
        if (this.showVisible && this.showInvisible) {
            return true;
        }
        else if (this.showInvisible) {
            return !file.visivelCliente
        }
        else if (this.showVisible) {
            return file.visivelCliente
        }
        else {
            return true;
        }
    }

    closeFabIfNeeded() {
        console.log("closingfab");
        if (this.fabContainer) {
            this.fabContainer.close();
        }
    }

    setFilter(filter) {
        if (!filter) {
            this.showFiles = true;
            this.showFolders = true;
            this.getRootFolder();
        }
        this.history = [];
        this.filter = !filter ? null : filter;
        this.afterSetFilter(this.filter);
    }


    showFilterModal() {

        this.closeFabIfNeeded();

        console.log("sending filter", this.filter);
        let modal = this.modalCtrl.create("CivilWorkGalleryFiltersPage", {searchObject: this.filter});
        modal.onDidDismiss(data => {
            if (data) {
                this.setFilter(data.filter);
            }

        });
        modal.present();
    }

    onSearchBadgeClick() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Filtrar',

            buttons: [
                {
                    text: 'Remover filtro',
                    handler: () => {
                        this.filterModel = null;
                        this.setFilter(null);

                        console.log("Remover");

                    }
                },
                {
                    text: 'Editar filtro',
                    handler: () => {
                        this.showFilterModal();
                    }
                }
            ]
        });
        actionSheet.present();
    }

    getRootFolder() {
        let item = {
            type: "civilWorkGallery",
            find: {id_obra: this.civilWorkProvider.selected.id_obra, pastaMae: {$exists: false}, deleted: {'$ne': true}}
        };
        this.pouchdbOff.getFind(item).then((data) => {
            // console.log(data)
            let objects = data.docs.map(row => {
                return row;
            });

            var folder = {
                galeriasFilhas: objects,
                nome: "Raiz",
                rootFolder: true,
                id_empresa: this.userLocalProvider.company.id_empresa,
                id_obra: this.civilWorkProvider.selected.id_obra
            }

            this.folder = folder;

        }).catch(error => {

        })
    }

    refresh() {
        this.getFolder(!this.folder.id_galeria && this.folder._id ? this.folder._id : this.folder.id_galeria);
    }

    onBackClick() {
        this.unselectAll();
        if (this.history.length == 1) {
            if (this.filterModel) {
                this.customFind();
            }
            else {
                this.getRootFolder();
            }

            this.history.splice(this.history.length - 1, 1);
        }
        else if (this.history.length) {
            console.log(this.history.length);

            var gallery = this.history[this.history.length - 1];
            this.getFolder(!gallery.id_galeria && gallery._id ? gallery._id : gallery.id_galeria);
            this.history.splice(this.history.length - 1, 1);
        }
    }


    getGaleriasFilhas(file) {
        let item = {
            type: "civilWorkGallery",
            find: {id_obra: this.civilWorkProvider.selected.id_obra, pastaMae: file.id_galeria, deleted: {'$ne': true}}
        };
        console.log("getFolder", item);
        this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Order getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            console.log(objects);
            this.folder.galeriasFilhas = objects ? objects : [];

        })
    }

    getOfflineFolder(id, pushToHistory?) {

        if (id !== null && typeof id === 'object') {
            console.log('id is object');
            if (!id.id_galeria && id._id) {
                id = id._id;
            }
            else if (id.id_galeria) {
                id = id.id_galeria;
            }
        }

        if (!id) {
            this.getRootFolder();
            return;
        }

        let item = {
            type: "civilWorkGallery",
            find: {id_obra: this.civilWorkProvider.selected.id_obra, id_galeria: id}
        };
        console.log("getFolder", item);
        this.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Order getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            console.log(objects);

            if (objects && objects.length) {
                if (pushToHistory) {
                    this.history.push(this.folder);
                }

                this.folder = objects[0];
                this.getGaleriasFilhas(this.folder);
            }

        })

    }

    getFolder(id, pushToHistory?) {

        // if (id !== null && typeof id === 'object') {
        //     this.getOfflineFolder(id, pushToHistory);
        //     return;
        // }

        if (!id) {
            this.getRootFolder();
            return;
        }

        // let item = {
        //     type: "civilWorkGallery",
        //     find: {id_obra: this.civilWorkProvider.selected.id_obra, id_galeria: id}
        // };

        let item = {
            type: "civilWorkGallery",
            find: {
                $and: [
                    {id_obra: this.civilWorkProvider.selected.id_obra},
                    {$or: [{id_galeria: id}, {_id: id}]}
                ]
            }
        }

        console.log("getFolder", item);
        this.pouchdbOff
            .getFind(item)

            .then(
                (data: any) => {
                    // console.log("Work Order getDataFromLocalDatabase ### ", data)

                    let
                        objects = data.docs.map(row => {
                            if (row._id && !row.id_galeria) {
                                row.id_galeria = row._id;
                                console.log("row with _id");
                            }
                            return row;
                        });

                    console.log(objects);

                    if (objects

                        &&
                        objects
                            .length
                    ) {
                        if (pushToHistory) {
                            this.history.push(this.folder);
                        }

                        objects[0].galeriasFilhas = [];
                        this.folder = objects[0];
                        this.getGaleriasFilhas(this.folder);
                    }

                })

    }


    onFolderClick(folder) {

        console.log(folder);
        this.closeFabIfNeeded();

        if (!this.selected.length) {


            this.getFolder(!folder.id_galeria && folder._id ? folder._id : folder.id_galeria, true);

            //this.history.push(this.folder);
            //this.folder = folder;
        }
        else {
            this.onSelectFile(folder);
        }

    }

    onFileClick(file) {
        this.closeFabIfNeeded();

        if (!this.selected.length) {
            if (file.caminhoArquivo && this.getFileType(file) != 'image') {

                this.inAppBrowser.create(file.caminhoArquivo, "_system");


            }
        }
        else {
            this.onSelectFile(file);
        }
    }


    showAlert(title, message, onOkClick) {

        var confirmAlert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancelar',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        console.log('yes clicked');
                        if (onOkClick) {
                            onOkClick();
                        }
                    }
                }
            ]
        });
        confirmAlert.present();

    }

    deleteFile(file) {

        if (file.fixa) {
            this.requestHelpersProvider.showMessage("Você não pode deletar esta pasta.");
            return;
        }

        this.civilWorkGalleryProvider.remove(file, true).subscribe((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {

                this.deleteItemFromDb({
                    obj: file,
                    type: "civilWorkGallery"
                }).then((data) => {

                    this.requestHelpersProvider.showMessage("Operação realizada com sucesso");

                    this.getFolder(!this.folder.id_galeria && this.folder._id ? this.folder._id : this.folder.id_galeria);
                    this.unselectAll();

                })

            }
            else {
                this.requestHelpersProvider.showMessage("Houve um erro, por favor verifique seus dados e tente novamente.");
            }
        }, (error) => {
            // se der erro de conexão ou n tiver internet cai aqui. Chama a função pra buscar os dados no banco de dados.
            this.unselectAll();
            this.deleteOff(file);
        })
    }


    deleteOff(file) {

        file.deleted = true;

        return this.pouchControll.insert({
            obj: file,
            type: "civilWorkGallery"
        }).then((data) => {
            if (data && data.length) {
                this.requestHelpersProvider.showMessage("Operação realizada com sucesso");
                this.unselectAll();
                this.getFolder(!this.folder.id_galeria && this.folder._id ? this.folder._id : this.folder.id_galeria);
            }
        })

    }

    deleteItemFromDb(item: any) {

        return this.pouchdbOff.delete(item).then(
            resp => {
                return resp;

            }).catch((error) => {
            this.requestHelpersProvider.showMessage("Houve um erro, por favor tente novamente.");
        })

    }

    onSelectFile(file) {
        this.closeFabIfNeeded();

        if (!this.selectable) {
            return;
        }

        var index = _.findIndex(this.folder.galeriasFilhas, (item: any, index) => {

            return item.id_galeria ? item.id_galeria == file.id_galeria : item._id == file._id;
        });

        this.folder.galeriasFilhas[index].selected = !this.folder.galeriasFilhas[index].selected;
        var alreadySelected = _.findWhere(this.selected, {
            id_galeria: file.id_galeria
        });

        if (this.folder.galeriasFilhas[index].selected && !alreadySelected) {
            this.selected.push(file);
        }
        else if (!this.folder.galeriasFilhas[index].selected) {
            this.selected = _.without(this.selected, alreadySelected);
        }

    }

    showOrderByOptions() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Ordenar',

            buttons: [
                {
                    text: 'Por nome (A-Z)',
                    icon: 'md-funnel',
                    handler: () => {
                        this.orderBy("nome", '+');
                    }
                },
                {
                    text: 'Por nome (Z-A)',
                    icon: 'md-funnel',
                    handler: () => {
                        this.orderBy("nome", "-");
                    }
                },
                {
                    text: 'Por data (crescente)',
                    icon: 'md-funnel',
                    handler: () => {
                        this.orderBy("data", "+");
                    }
                },
                {
                    text: 'Por data (decrescente)',
                    icon: 'md-funnel',
                    handler: () => {
                        this.orderBy("data", "-");
                    }
                }
            ]
        });
        actionSheet.present();
    }

    orderBy(property, order) {
        this.orderAscDesc = order;
        this.orderProperty = property;
    }

    changeVisibility(visibility) {

        if (this.folder && this.folder.rootFolder) {
            this.requestHelpersProvider.showMessage("Não é possivel alterar a visibilidade de arquivos na pasta raiz.");
            return;
        }


        var index = 0;
        var onDone = (file) => {
            if (this.selected.length - 1 > index) {
                index = index + 1;
                var file = this.selected[index];
                file.visivelCliente = visibility;

                this.addFile(file, onDone);
            }

            else {
                this.selected = [];
                this.unselectAll();
                this.requestHelpersProvider.showMessage("Operação realizada com sucesso.");
                this.getFolder(!this.folder.id_galeria && this.folder._id ? this.folder._id : this.folder.id_galeria);

            }
        }

        var file = this.selected[index];
        file.visivelCliente = visibility;
        console.log(file);
        this.addFile(file, onDone);


    }


    renameFile(file) {
        console.log(file);
        let prompt = this.alertCtrl.create({
            title: 'Renomear',
            inputs: [
                {
                    name: 'title',
                    placeholder: "Nome do arquivo",
                    value: file.title
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Salvar',
                    handler: data => {
                        if (!data.title || data.title.length < 1) {
                            this.requestHelpersProvider.showMessage("A galeria precisa ter ao menos um caractere.");
                            return false;
                        }

                        var ext = "";
                        try {
                            ext = "." + file.nome.split('.').pop();
                        }
                        catch(error) {

                        }
                        file.nome = data.title + ext;
                        this.addFile(file);

                        this.selected = [];
                    }
                }
            ]
        });
        prompt.present();
    }

    unselectAll() {
        console.log("unselectAll");
        this.folder.galeriasFilhas = _.map(this.folder.galeriasFilhas, (file: any) => {
            file.selected = false;
            return file;
        });
        this.selected = [];
    }

    getFileThumb(file) {
        if (file.caminhoThumb) {
            return file.caminhoThumb;
        }
        else if (file.type == 'image') {
            return file.arquivo && file.arquivo.indexOf('data:image') == -1 ?  'data:image/jpeg;base64,' + file.arquivo : file.arquivo;
        }
        else {
            return "";
        }
    }

    getFileType(file): string {

        if (file.type == 'image') {
            return "image";
        }
        else if (!file.caminhoArquivo) {
            return "file";
        }

        var fileTypes = [
            {
                type: "image",
                extensions:
                    ['jpg', 'png', 'gif', 'jpeg', 'bmp']
            },
            {
                type: "file",
                extensions:
                    ['doc', 'docx', 'txt', 'pdf', 'zip', 'rar', 'psd', 'xls', 'xlsx', 'rtf', 'dwg']
            }
        ];

        var fileType = 'generic';

        var fileExtension = file.caminhoArquivo.split('.').pop();


        fileTypes.forEach((type, index) => {

            if (type.extensions.indexOf(fileExtension) != -1) {
                fileType = type.type;
            }
        });

        return fileType;
    }


    onAddFolderClick() {


        // this.modalCtrl.create(FileManager).present();

        this.closeFabIfNeeded();

        console.log(this.folder);
        var file: any = {};

        let prompt = this.alertCtrl.create({
            title: 'Adicionar pasta',
            inputs: [
                {
                    name: 'title',
                    placeholder: "Nome da pasta",
                    type: "text"
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Salvar',
                    handler: data => {
                        if (data.title && data.title.length) {
                            file.nome = data.title;
                            this.setFileVisibilityBeforeSend(file);
                            return true;
                        }
                        else {
                            this.requestHelpersProvider.showMessage("A galeria precisa ter ao menos um caractere.");
                        }

                        return false;
                    }
                }
            ]
        });
        prompt.present();
    }

    setFileVisibilityBeforeSend(file) {

        let prompt = this.alertCtrl.create({
            title: 'Visibilidade',
            inputs: [
                {
                    name: "visible",
                    value: "visible",
                    label: "Visivel ao cliente",
                    type: "checkbox"
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Salvar',
                    handler: data => {

                        console.log("data", data);
                        file.visivelCliente = data && data.length && data[0] && data[0] == "visible" ? true : false;
                        console.log("file.visivelCliente", file);
                        if (file.id_galeria) {
                            this.addFile(file);
                        }
                        else {
                            this.addFile(file);
                        }

                    }
                }
            ]
        });
        prompt.present();
    }


    addFile(file, onDone ?) {


        if ((!this.selected || this.selected.length == 1) && !onDone) {
            this.unselectAll(); // esta bloqueando modificação da visibilidade de multiplos arquivos
        }

        file.selected = false;

        if (!file.id_galeria) {
            var parentFolder = this.folder;
            if (parentFolder.id_galeria) {
                file.pastaMae = parentFolder.id_galeria;
            }
            else if (parentFolder._id) {
                file.pastaMae = parentFolder._id;
            }
            file.id_empresa = parentFolder.id_empresa;
            file.id_obra = parentFolder.id_obra;
        }
        delete file.galeriasFilhas;


        var request = !file.id_galeria ? this.civilWorkGalleryProvider.add(file) : this.civilWorkGalleryProvider.edit(file);


        request.subscribe((data: any) => {
            console.log(data);
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                if (data.object.id_galeria) {

                    var bdItem = {type: "civilWorkGallery", obj: [data.object]};
                    this.pouchControll.bulkDoc(bdItem);


                    if (!onDone) {
                        this.requestHelpersProvider.showMessage("Operação realizada com sucesso.");
                        // refresh screen
                        if (this.folder.id_galeria) {
                            this.getFolder(this.folder.id_galeria);
                        }
                        else {
                            this.getRootFolder();
                        }
                    }
                    else {
                        onDone(file);
                    }

                }
                else {
                    this.requestHelpersProvider.showMessage("Houve um erro, por favor verifique seus dados e tente novamente");
                }
            }
        }, (error) => {

            this.addOffline(file, onDone);

            // this.requestHelpersProvider.showTryAgainDialog(() => {
            //     this.addFile(file, onDone);
            // })
        });
    }

    addOffline(file, onDone ?) {
        file.usuario = this.userLocalProvider.user;
        file.data = moment().tz("America/Sao_Paulo").format();

        this.pouchControll.insert({
            obj: file,
            type: "civilWorkGallery"
        }).then((data) => {
            if (data && data.length) {
                file.id_galeria = file.id_galeria ? file.id_galeria : data[0].id;

                if (!onDone) {
                    this.requestHelpersProvider.showMessage("Operação realizada com sucesso.");
                    // refresh screen

                    if (this.folder.id_galeria) {
                        this.getFolder(!this.folder.id_galeria && this.folder._id ? this.folder._id : this.folder.id_galeria);
                    }
                    else {
                        this.getRootFolder();
                    }
                }
                else {
                    onDone(file);
                }
            }

        });
    }

}
