import {Component, Input} from '@angular/core';

/**
 * Generated class for the SyncLineComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'sync-line',
  templateUrl: 'sync-line.html'
})
export class SyncLineComponent {

  @Input() item;
  @Input() small;

  constructor() {
  }

}
