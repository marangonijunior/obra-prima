import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FileManagerComponent} from './file-manager/file-manager';
import {IonicModule} from "ionic-angular";
import {CommonModule} from "@angular/common";
import {FilterByImpurePipe, GroupByImpurePipe, NgPipesModule, OrderByImpurePipe} from "ngx-pipes";
import {CivilWorkHeaderComponent} from './civil-work-header/civil-work-header';
import {DirectivesModule} from "../directives/directives.module";
import {NoItemsComponent} from './no-items/no-items';
import {FilterBarComponent} from './filter-bar/filter-bar';
import {CivilWorkDayHeaderComponent} from './civil-work-day-header/civil-work-day-header';
import {CivilWorkOrderHeaderComponent} from './civil-work-order-header/civil-work-order-header';
import {IonicImageLoader} from "ionic-image-loader";
import {FileHelperProvider} from "../providers/file-helper/file-helper";
import {File} from "@ionic-native/file";
import {FileChooser} from "@ionic-native/file-chooser";
import {FilePath} from "@ionic-native/file-path";
import {Camera} from "@ionic-native/camera";
import { SyncLineComponent } from './sync-line/sync-line';
import { RoundLetterComponent } from './round-letter/round-letter';
import { UserAvatarComponent } from './user-avatar/user-avatar';
import {IonicImageViewerModule} from "ionic-img-viewer";

@NgModule({
    declarations: [FileManagerComponent,
        CivilWorkHeaderComponent,
        NoItemsComponent,
        FilterBarComponent,
        FileManagerComponent,
        CivilWorkDayHeaderComponent,
        CivilWorkOrderHeaderComponent,
    SyncLineComponent,
    RoundLetterComponent,
    UserAvatarComponent],
    imports: [
        CommonModule,
        IonicModule,
        IonicImageLoader,
        NgPipesModule,
        IonicImageViewerModule,
        DirectivesModule
    ],
    exports: [FileManagerComponent,
        CivilWorkHeaderComponent,
        FileManagerComponent,
        NoItemsComponent,
        FilterBarComponent,
        CivilWorkDayHeaderComponent,
        CivilWorkOrderHeaderComponent,
    SyncLineComponent,
    RoundLetterComponent,
    UserAvatarComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    providers: [
        FileHelperProvider,
        Camera,
        File,
        FileChooser,
        FilePath,
        OrderByImpurePipe,
        FilterByImpurePipe
    ]
})
export class ComponentsModule {
}
