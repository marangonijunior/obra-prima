import {Component, Input} from '@angular/core';
import {CivilWorkProvider} from "../../providers/civil-work/civil-work";
import {App} from "ionic-angular";

/**
 * Generated class for the CivilWorkHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'civil-work-header',
  templateUrl: 'civil-work-header.html'
})
export class CivilWorkHeaderComponent {

  text: string;
  item : any;
  @Input() small;
  @Input() clickable = true;

  constructor(
      private app : App,
      private civilWorkProvider : CivilWorkProvider
  ) {
    this.civilWorkProvider.getSelected().then((data) => {
      console.log(data);
      this.item = data;
    });
  }

  click() {
    if (this.clickable) {
        this.app.getActiveNav().setRoot("CivilWorkListPage");
    }

  }

}
