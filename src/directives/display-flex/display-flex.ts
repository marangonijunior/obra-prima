import {Directive, ElementRef, Input} from '@angular/core';

/**
 * Generated class for the DisplayFlexDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[displayFlex]' // Attribute selector
})
export class DisplayFlexDirective {

  @Input() alignItems: string = "center";
  @Input() displayFlex: string = "flex";
  @Input() justifyContent: string = "flex-start"
  @Input() flexDirection: string = "";
  @Input() width: string;

  constructor(private el: ElementRef) {
    this.el.nativeElement.style.display = this.displayFlex;
    this.el.nativeElement.style.alignItems = this.alignItems;
    this.el.nativeElement.style.justifyContent = this.justifyContent;
  }

  ngOnInit() {

    if (this.alignItems) {
      this.el.nativeElement.style.alignItems = this.alignItems;
    }
    if (this.displayFlex) {
      this.el.nativeElement.style.display = this.displayFlex;
    }
    if (this.justifyContent) {
      this.el.nativeElement.style.justifyContent = this.justifyContent;
    }
    if (this.flexDirection) {
      this.el.nativeElement.style.flexDirection = this.flexDirection;
    }
    if (this.width) {
      this.el.nativeElement.style.width = this.width;
    }
  }

}
