import { NgModule } from '@angular/core';
import { DisplayFlexDirective } from './display-flex/display-flex';
import { LinkDirective } from './link/link';
import { AutoResizeTextareaDirective } from './auto-resize-textarea/auto-resize-textarea';
@NgModule({
	declarations: [DisplayFlexDirective,
    LinkDirective,
    AutoResizeTextareaDirective],
	imports: [],
	exports: [DisplayFlexDirective,
    LinkDirective,
    AutoResizeTextareaDirective]
})
export class DirectivesModule {}
