import {Directive, HostListener, Input} from '@angular/core';
import {NavController} from "ionic-angular";

/**
 * Generated class for the LinkDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[link]' // Attribute selector
})
export class LinkDirective {

  @Input() link;
  @Input() linkData : any = {};
  @Input() beforeLink : () => {};

  constructor(private navController : NavController) {
    console.log('Hello LinkDirective Directive');
  }

  ngOnInit() {
  }

  @HostListener('click', ['$event']) onClick($event){
    console.info('clicked: ' + $event);
    if (this.beforeLink) {
      this.beforeLink();
    }
    this.navController.push(this.link, this.linkData);
  }

  }
