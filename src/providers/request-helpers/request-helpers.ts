import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AlertController, ToastController} from "ionic-angular";

/*
  Generated class for the RequestHelpersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RequestHelpersProvider {

    constructor(
        private toastController: ToastController,
        private alertController: AlertController
    ) {
        console.log('Hello RequestHelpersProvider Provider');
    }

    successAndHasObjectFromDb(data: any) {
        var success = data && data.obj;
        return success;
    }

    successAndHasObject(data: any) {
        var success = data && (data.response == "success" || data.response === true) && (data.object || data.objects);
        return success;
    }

    showErrorToast(data, customMessage?) {

        var message = customMessage ? customMessage : 'Houve um erro por favor tente novamente.';
        try {
            message = data.error.message;
        }
        catch (error) {
        }


        let toast = this.toastController.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }

    showMessage(message, type?) {


        let toast = this.toastController.create({
            message: message,
            cssClass: type ? type : '',
            duration: 2000
        });
        toast.present();
    }

    showTryAgainDialog(onTryAgainPressed) {

        let alert = this.alertController.create({
            title: 'Houve um erro',
            message: 'Houve um erro, por favor verifique a sua conexão com a internet e tente novamente.',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Retentar',
                    handler: () => {
                        onTryAgainPressed();
                    }
                }
            ]
        });
        alert.present();
    }

}
