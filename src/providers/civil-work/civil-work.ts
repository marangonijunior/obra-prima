import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable, Injector} from '@angular/core';
import {ApiProvider} from "../api/api";
import {Storage} from "@ionic/storage";

import {Network} from '@ionic-native/network';
import {PouchControll} from "../pouchdb/pouchdb-controll";
import {LoadingProvider} from "../loading/loading";
import "rxjs/add/operator/catch";
import {UserLocalProvider} from "../user-local/user-local";
import {fromPromise} from "rxjs/observable/fromPromise";


/*
  Generated class for the CivilWorkProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CivilWorkProvider extends ApiProvider {

    selected;

    constructor(public network: Network,
                public injector: Injector,
                public loadingProvider: LoadingProvider,
                public userLocalProvider: UserLocalProvider,
                public pouchControll: PouchControll,
                protected http: HttpClient, private storage: Storage,) {
        super(http);
        this.getSelected().then((selected) => {
            this.selected = selected;
        })
    }


    getDataFromApiAndSyncToLocalDatabase(showLoading) {

        var company = this.userLocalProvider.company;
        return fromPromise(this.pouchControll.syncAll("civilWork", this.daoConfig.getAPIUrl() + 'obras/', company.id_empresa, showLoading)
                    .catch(error => {
                        return error;
                    })
                );

        // return fromPromise(this.userLocalProvider.getSelectedCompany())
        //     .switchMap((company: any) => {
        //
        //         let req1 = fromPromise(this.pouchControll.syncAll("civilWork", this.daoConfig.getAPIUrl() + 'obras/', company.id_empresa, false)
        //             .catch(error => {
        //                 return error;
        //             })
        //         );
        //         let req2 = fromPromise(this.pouchControll.syncAll("civilWorkDay", this.daoConfig.getAPIUrl() + 'diaDias/', company.id_empresa, false)
        //             .catch(error => {
        //                 return error;
        //             }));
        //         let req3 = fromPromise(this.pouchControll.syncAll("civilWorkGallery", this.daoConfig.getAPIUrl() + 'galerias/', company.id_empresa, false)
        //             .catch(error => {
        //                 return error;
        //             }));
        //         let req4 = fromPromise(this.pouchControll.syncAll("civilWorkOrder", this.daoConfig.getAPIUrl() + 'solicitacoes/', company.id_empresa, false)
        //             .catch(error => {
        //                 return error;
        //             }));
        //         let req5 = this.generalProvider.getAbout(false);
        //         let req6 = this.userProvider.getCompanies(this.userLocalProvider.user, false);
        //
        //
        //         return Observable.concat(req1, req2, req3, req4, req5, req6).toArray();
        //     }).switchMap((data: any) => {
        //         if (showLoading) {
        //             this.loadingProvider.dismiss();
        //         }
        //         return Observable.of(data);
        //     }).catch((e) => {
        //         if (showLoading) {
        //             this.loadingProvider.dismiss();
        //         }
        //         return Observable.throw(e);
        //     });


        // return fromPromise(this.userLocalProvider.getSelectedCompany())
        //     .switchMap((company: any) => {
        //
        //
        //         // cai na função de erro do getData no civilWorkList se não tiver internet
        //         if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
        //             throw Observable.throw(null);
        //         }
        //
        //         return fromPromise(this.pouchControll.syncAll("civilWork", this.daoConfig.getAPIUrl() + 'obras/', company.id_empresa, showLoading).then(
        //             resp => {
        //                 return this.pouchControll.syncAll("civilWorkDay", this.daoConfig.getAPIUrl() + 'diaDias/', company.id_empresa, showLoading).then(
        //                     resp => {
        //                         return this.pouchControll.syncAll("civilWorkGallery", this.daoConfig.getAPIUrl() + 'galerias/', company.id_empresa, showLoading).then(
        //                             resp => {
        //                                 return this.pouchControll.syncAll("civilWorkOrder", this.daoConfig.getAPIUrl() + 'solicitacoes/', company.id_empresa, showLoading).then(
        //                                     resp => {
        //                                         console.log(resp);
        //                                         console.log("Dados carregados")
        //                                     }
        //                                 ).catch(err => {
        //                                     console.log(err);
        //                                     if (showLoading) {
        //                                         this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'");
        //                                     }
        //                                     return false;
        //                                 });
        //                             }
        //                         ).catch(err => {
        //                             console.log(err);
        //                             if (showLoading) {
        //                                 this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'");
        //                             }
        //                             return false;
        //                         });
        //                     }
        //                 ).catch(err => {
        //                     console.log(err);
        //                     if (showLoading) {
        //                         this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'");
        //                     }
        //                     return false;
        //                 });
        //             }
        //         ).catch(err => {
        //             console.log(err);
        //             if (showLoading) {
        //                 this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'");
        //             }
        //             return false;
        //         }));
        //
        //         //     .catch(err => {
        //         //     console.log(err);
        //         //     this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
        //         // });
        //
        //     });
    }


    getFases(showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'obra');

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };
        return this.http.get(this.daoConfig.getAPIUrl() + 'orcamentoFases/' + this.selected.id_obra, requestOptions);

    }

    getFasesByCompany(showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'empresa');

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };
        return this.http.get(this.daoConfig.getAPIUrl() + 'orcamentoFases/' + this.userLocalProvider.company.id_empresa, requestOptions);

    }

    getServicos(idFase) {


        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'fase');

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'orcamentoServicos/' + idFase, requestOptions);


    }

    getServicosByCompany(showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'empresa');

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'orcamentoServicos/' +
            this.userLocalProvider.company.id_empresa, requestOptions);


    }

    setSelected(civilWork) {
        this.selected = civilWork;
        return this.storage.set("civilWork", civilWork);
    }

    getSelected() {
        return this.storage.get("civilWork");
    }

    removeSelected() {
        this.selected = null;
        return this.storage.remove("civilWork");
    }

    getDeleted(listOfIds, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.post(this.daoConfig.getAPIUrl() + 'obrasEmpresaDeleteds/', listOfIds, requestOptions);
    }

}
