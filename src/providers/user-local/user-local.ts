import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import {Storage} from "@ionic/storage";
import {Platform} from "ionic-angular";
import {CivilWorkProvider} from "../civil-work/civil-work";


declare var emit;

/*
  Generated class for the UserLocalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.


*/
@Injectable()
export class UserLocalProvider {

    userDb;
    user;
    company;

    constructor(
        private storage: Storage,
                public platform: Platform) {
        PouchDB.plugin(cordovaSqlitePlugin);
        PouchDB.plugin(PouchDBFind);
        this.userDb = new PouchDB('user.db',
            {adapter: 'cordova-sqlite', auto_compaction: true});

        this.getSelectedCompany().then((company) => {
            this.company = company;
        });

        this.getUserFromLocalDb().then((user) => {
            this.user = user;
        });
        this.getSelectedCompany().then((company) => {
            this.company = company;
        });
    }

    getAll() {
        return this.userDb.allDocs({include_docs: true})
            .then(docs => {

                let objects = docs.rows.map(row => {
                    return row.doc;
                });

                return objects;
            });
    }

    add(object) {
        return this.userDb.post(object);
    }

    edit(object) {
        return this.userDb.put(object);
    }

    addOrEdit(object) {
        return this.getByEmail(object).then((users) => {
            if (!users || users.length == 0) {
                return this.add(object);
            }
            else {
                console.log("found users", users);
                object._id = users[0]._id;
                object._rev = users[0]._rev;
                return this.edit(object);
            }
        });

    }

    getById(object) {
        object._id = object.id_usuario.toString();
        return this.userDb.get(object._id);
    }

    getByEmail(object) {

        let item = {
            find: {login: object.login}
        };

        return this.userDb.find({
            selector: item.find //{name:"teste"}
        }).then(function (data) {

            let objects = data.docs.map(row => {
                return row;
            });

            return objects;
        }).catch(function (err) {
            return {"err": "Error find docs"}
        });

    }

    setUserOnLocalDb(user) {

        this.user = user;
        return this.storage.set('user', user);
    }

    setSelectedCompany(company) {

        this.company = company;
        return this.storage.set('company', company);
    }

    getSelectedCompany() {
        return this.storage.get('company');
    }

    getUserFromLocalDb() {
        return this.storage.get('user');
    }

    removeUserFromLocalDb() {
        this.user = null;
        this.company = null;
        this.storage.remove("user");
        return this.storage.remove("company");
    }

}
