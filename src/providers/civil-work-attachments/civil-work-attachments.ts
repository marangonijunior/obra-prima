import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ApiProvider} from "../api/api";
import {Network} from "@ionic-native/network";
import {UserProvider} from "../user/user";
import {Observable} from "rxjs/Observable";

/*
  Generated class for the CivilWorkAttachmentsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CivilWorkAttachmentsProvider extends ApiProvider {

    constructor(public network: Network,
                private userProvider: UserProvider,
                public http: HttpClient) {
        super(http);
    }

    get(id, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };


        return this.http.get(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + id, requestOptions);
    }

    add(file, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        delete file.img;
        return this.http.post(this.daoConfig.getAPIUrl() + 'diaDiaAnexos', file, requestOptions);
    }

    edit(file, showLoading?) {

        let headers = new HttpHeaders();

        const requestOptions = {
            headers: headers
        };

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        delete file.img;
        return this.http.put(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + file.id_anexo, file);
    }

    delete(file, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        delete file.img;
        return this.http.delete(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + file.id_anexo, requestOptions);
    }

    getDeleted(listOfIds, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.post(this.daoConfig.getAPIUrl() + 'diaDiaAnexosDeleteds/', listOfIds, requestOptions);
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'diaDiaAnexosDeleteds/', listOfIds, requestOptions);
        }
    }

}
