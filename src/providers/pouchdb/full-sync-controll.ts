import {Injectable, Injector} from '@angular/core';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import {CivilWorkDayProvider} from "../civil-work-day/civil-work-day";
import {PouchdbOff} from "./pouchdb-off";
import {RequestHelpersProvider} from "../request-helpers/request-helpers";
import {PouchControll} from "./pouchdb-controll";
import {CivilWorkOrderProvider} from "../civil-work-order/civil-work-order";
import {CivilWorkGalleryProvider} from "../civil-work-gallery/civil-work-gallery";
import {Network} from "@ionic-native/network";
import {Platform} from "ionic-angular";
import * as _ from 'underscore';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import 'rxjs/add/operator/toPromise';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import {concat} from 'rxjs/operators';
import {CivilWorkOrderItensProvider} from "../civil-work-order-itens/civil-work-order-itens";
import {CivilWorkAttachmentsProvider} from "../civil-work-attachments/civil-work-attachments";
import {CivilWorkProvider} from "../civil-work/civil-work";
import {share} from "rxjs/operators";
import {fromPromise} from "rxjs/observable/fromPromise";
import {PouchdbSyncControlSingle} from "./pouchdb-sync-control-single";
import {ApiProvider} from "../api/api";
import {UserProvider} from "../user/user";
import {GeneralProvider} from "../general/general";
import {UserLocalProvider} from "../user-local/user-local";
import {GenericLocalDataProvider} from "../generic-local-data/generic-local-data";

@Injectable()
export class FullSyncControll {
    syncTimer;
    shouldSync = true;
    syncInterval = 10000;
    syncActive; // true if a single sync is happening now
    backgroundSync; // true if sync timer is active
    types = [];
    typeIndex = 0;
    syncing = true;


    syncBehavior :  () => void;


    constructor(public injector: Injector,
                public network: Network,
                public userProvider: UserProvider,
                public userLocalProvider: UserLocalProvider,
                public generalProvider: GeneralProvider,
                public genericLocalDataProvider: GenericLocalDataProvider,
                public platform: Platform,
                public apiProvider: ApiProvider,
                public pouchdbSyncControlSingle: PouchdbSyncControlSingle,
                public requestHelpersProvider: RequestHelpersProvider) {


        this.syncBehavior = () => {

            console.log("FullSyncControll", "ending syncing");
            this.syncActive = false;
            var $this = this;

            if (this.backgroundSync) {
                setTimeout(() => {
                    $this.syncOnBackground();
                }, $this.syncInterval);
            }

        }
    }

    startSyncIfNeed() {
        if (!this.backgroundSync && this.syncing) {
            this.backgroundSync = true;
            this.syncOnBackground();
        }
    }

    stopSyncIfNeed() {
        if (this.backgroundSync) {
            this.backgroundSync = false;
        }
    }

    syncOnBackground() {

        if (!this.shouldSync) {
            return;
        }

        if (this.syncActive) {
            return;
        }

        // var sync = () => {
        //     return this.doSync();
        // }

        this.syncActive = true;
        console.log("FullSyncControll", "start syncing");
        this.doSync().subscribe((data) => {

            console.log("FullSyncControll", "ending syncing success");
            this.syncActive = false;
            var $this = this;

            if (this.backgroundSync) {
                setTimeout(() => {
                    $this.syncOnBackground();
                }, $this.syncInterval);
            }


        }, (error) => {

            console.log("FullSyncControll", "ending syncing error");
            this.syncActive = false;
            var $this = this;

            if (this.backgroundSync) {
                setTimeout(() => {
                    $this.syncOnBackground();
                }, $this.syncInterval);
            }

        })

    }

    doSync(syncOnly?) {

        if (syncOnly) {
            return this.syncDataRequest();
        }


        var deletedCivilWorks = this.getDeletedItens("civilWork", "id_obra", this.injector.get(CivilWorkProvider));
        var deletedCivilWorkDays = this.getDeletedItens("civilWorkDay", "id_diaDia", this.injector.get(CivilWorkDayProvider));
        var deletedCivilWorkOrders = this.getDeletedItens("civilWorkOrder", "id_solicitacao", this.injector.get(CivilWorkOrderProvider));
        var deletedCivilWorkGalleries = this.getDeletedItens("civilWorkGallery", "id_galeria", this.injector.get(CivilWorkGalleryProvider));
        var deletedCivilWorksOrderItens = this.getDeletedItens("civilWorkOrderItems", "id_item", this.injector.get(CivilWorkOrderItensProvider));
        var deletedCivilWorkDayAttchments = this.getDeletedItens("attchments", "id_anexo", this.injector.get(CivilWorkAttachmentsProvider));

        return Observable.concat(deletedCivilWorks, deletedCivilWorkDays, deletedCivilWorkOrders,
            deletedCivilWorkGalleries, deletedCivilWorksOrderItens, deletedCivilWorkDayAttchments).toArray()
            .switchMap((data) => {
                return this.getDataFromApiRequest()
                    .switchMap((data) => {
                        return this.syncDataRequest().first();
                    }).catch((error) => {
                        console.log(error);
                        return Observable.throw(error);

                        //  return Observable.of<any>(null);
                    });
            }).catch((error) => {
                console.log(error);
              //  return Observable.throw(error);

                 return Observable.of<any>({});
            });
    }

    syncDataRequest() {
        return fromPromise(this.pouchdbSyncControlSingle.doSync(this.pouchdbSyncControlSingle.types[0]).then((data) => {
            return {};
        })
            .catch((error) => {
            console.log("catch syncDataRequest()", error);
                return error;
            }))

        //     .switchMap((data) => {
        //     return Observable.of({});
        // });

    }

    getDataFromApiRequest() {


        return fromPromise(this.userLocalProvider.getSelectedCompany().catch(error => {
            return error;
        }))
            .switchMap((company: any) => {

                let req1 = fromPromise(this.pouchdbSyncControlSingle.pouchControll.syncAll("civilWork", this.apiProvider.daoConfig.getAPIUrl() + 'obras/', company.id_empresa, false)
                    .catch(error => {
                        return error;
                    })
                );
                let req2 = fromPromise(this.pouchdbSyncControlSingle.pouchControll.syncAll("civilWorkDay", this.apiProvider.daoConfig.getAPIUrl() + 'diaDias/', company.id_empresa, false)
                    .catch(error => {
                        return error;
                    }));
                let req3 = fromPromise(this.pouchdbSyncControlSingle.pouchControll.syncAll("civilWorkGallery", this.apiProvider.daoConfig.getAPIUrl() + 'galerias/', company.id_empresa, false)
                    .catch(error => {
                        return error;
                    }));
                let req4 = fromPromise(this.pouchdbSyncControlSingle.pouchControll.syncAll("civilWorkOrder", this.apiProvider.daoConfig.getAPIUrl() + 'solicitacoes/', company.id_empresa, false)
                    .catch(error => {
                        return error;
                    }));
                let req5 = this.generalProvider.getAbout(false);
                let req6 = this.userProvider.getCompanies(this.userLocalProvider.user, false);


                let fasesReq = this.injector.get(CivilWorkProvider).getFasesByCompany(false).switchMap((data: any) => {
                    if (this.requestHelpersProvider.successAndHasObject(data)) {

                        return fromPromise(this.genericLocalDataProvider.addOrEditPush("fasesEmpresa_" +
                            this.userLocalProvider.company.id_empresa, data.objects)
                            .catch((error) => {
                                return error
                            }));
                    }
                    else {
                        return Observable.of(data);
                    }

                });

                let servicosReq = this.injector.get(CivilWorkProvider).getServicosByCompany(false).switchMap((data: any) => {
                    if (this.requestHelpersProvider.successAndHasObject(data)) {

                        return fromPromise(this.genericLocalDataProvider.addOrEditPush("servicosEmpresa_" +
                            this.userLocalProvider.company.id_empresa, data.objects)
                            .catch((error) => {
                                return error
                            }));
                    }
                    else {
                        return Observable.of(data);
                    }

                });

                let situacoesReq = this.injector.get(CivilWorkOrderProvider).getSituacoes(false).switchMap((data: any) => {
                    if (this.requestHelpersProvider.successAndHasObject(data)) {

                        return fromPromise(this.genericLocalDataProvider.addOrEditPush("situacoesOrder", data.objects, true)
                            .catch((error) => {
                                return error
                            }));
                    }
                    else {
                        return Observable.of(data);
                    }

                });


                return Observable.concat(req1, req2, req3, req4, req5, req6, fasesReq, servicosReq, situacoesReq).toArray();
            }).switchMap((data: any) => {

                return Observable.of(data);
            }).catch((e) => {

                return Observable.throw(e);
            }).pipe(share());
    }

    getDataFromLocalDatabase(type) {
        let item = {
            type: type,
            find: {}
        };
        return this.pouchdbSyncControlSingle.pouchControll.pouchdbOff.getFind(item).then((data: any) => {
            // console.log("Work Day List getDataFromLocalDatabase ### ", data)

            let objects = data.docs.map(row => {
                return row;
            });

            return objects;

        }).catch(error => {

        })

    }

    getDeletedItens(type, idField, provider) {


        return fromPromise(this.getDataFromLocalDatabase(type)).switchMap((list: Array<any>) => {

            let idList = list

                .filter((item) => {
                    return item[idField];
                })
                .map((item) => {
                    return item[idField];
                })


            return provider.getDeleted(idList, false).switchMap((data: any) => {
                if (this.requestHelpersProvider.successAndHasObject(data)) {


                    var item = {
                        type: type,
                        selector: {$or: []}
                    };

                    var array = data.object ? data.object : data.objects ? data.objects : [];

                    array.forEach((id, index) => {

                        var dataObj = {};
                        dataObj[idField] = id;
                        item.selector.$or.push(dataObj);

                    })


                    return fromPromise(this.pouchdbSyncControlSingle.pouchControll.pouchdbOff.customFind(item).then((data) => {

                        if (data.docs && data.docs.length) {
                            data = data.docs.map((item, index) => {
                                item._deleted = true;
                                return item;
                            })

                            return this.pouchdbSyncControlSingle.pouchControll.pouchdbOff.bulkDoc({
                                type: type,
                                obj: data
                            });
                        }
                        else {
                            return null;
                        }


                    }).catch((error) => {
                        return error
                    }));

                }
                else {
                    return Observable.of(null);
                }

            }, (error) => {
                console.log("error deleting", error);
                return error;
            });

        });


    }


}