import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import {Observable} from 'rxjs';
import {PouchControll} from "./pouchdb-controll";


@Injectable()
export class PouchdbOff {
    public civilWork;
    public civilWorkDay;
    public civilWorkGallery;
    public civilWorkOrder;
    public attchments;
    public civilWorkOrderItems;
    public findDate;


    constructor() {
        PouchDB.plugin(PouchDBFind);
    }

    createPouchDB() {
        PouchDB.plugin(cordovaSqlitePlugin);
        this.civilWork = new PouchDB('civilWork.db',
            {adapter: 'cordova-sqlite', auto_compaction: true});
        this.civilWorkDay = new PouchDB('civilWorkDay.db',
            {adapter: 'cordova-sqlite', auto_compaction: true});
        this.civilWorkGallery = new PouchDB('civilWorkGallery.db',
            {adapter: 'cordova-sqlite', auto_compaction: true});
        this.civilWorkOrder = new PouchDB('civilWorkOrder.db',
            {adapter: 'cordova-sqlite', auto_compaction: true});
        this.attchments = new PouchDB('attchments.db',
            {adapter: 'cordova-sqlite', auto_compaction: true});
        this.civilWorkOrderItems = new PouchDB('civilWorkOrderItems.db',
            {adapter: 'cordova-sqlite', auto_compaction: true});
    }

    getGallery(item) {
        return this.civilWorkGallery.find({
            selector: {
                id_obra: item.id_obra
            }
        }).then(function (result) {

            return result;

        }).catch(function (err) {
            return {"err": "Error find docs"}
        });
    }

    getAll(type) {
        switch (type) {
            case "civilWork":
                return this.civilWork.allDocs({include_docs: true, attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "civilWorkDay":
                return this.civilWorkDay.allDocs({include_docs: true, attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "civilWorkGallery":
                return this.civilWorkGallery.allDocs({include_docs: true, attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "civilWorkOrder":
                return this.civilWorkOrder.allDocs({include_docs: true, attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "civilWorkOrderItems":
                return this.civilWorkOrderItems.allDocs({
                    include_docs: true,
                    attachments: true
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "attchments":
                return this.attchments.allDocs({include_docs: true, attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            default:
                return {"err": "Error get all docs"}
        }
    }

    customFind(item: any) {
        switch (item.type) {
            case "civilWork":
                return this.civilWork.find({
                    selector: item.selector
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkDay":
                return this.civilWorkDay.find({
                    selector: item.selector
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkOrder":
                return this.civilWorkOrder.find({
                    selector: item.selector
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkOrderItems":
                return this.civilWorkOrderItems.find({
                    selector: item.selector
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkGallery":
                return this.civilWorkGallery.find({
                    selector: item.selector
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "attchments":
                return this.attchments.find({
                    selector: item.selector
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            default:
                return {"err": "No possible."}
        }
    }

    findWithDate(item) {
        switch (item.type) {
            case "civilWorkDay":
                return this.civilWorkDay.find({
                    selector: {
                        $and: item.findDate
                    } //[{name:"teste"}]
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkOrder":
                return this.civilWorkOrder.find({
                    selector: {
                        $and: item.findDate
                    } //[{name:"teste"}]
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkGallery":
                return this.civilWorkGallery.find({
                    selector: {
                        $and: item.findDate
                    }//[{name:"teste"}]
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            default:
                return {"err": "No possible."}
        }
    }

    findNoDate(item) {
        switch (item.type) {
            case "civilWork":
                return this.civilWork.find({
                    selector: {
                        $or: item.findNoDate
                    } //[{name:"teste"}]
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkDay":
                return this.civilWorkDay.find({
                    selector: {
                        $or: item.findNoDate
                    } //[{name:"teste"}]
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkOrder":
                return this.civilWorkOrder.find({
                    selector: {
                        $or: item.findNoDate
                    } //[{name:"teste"}]
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkGallery":
                return this.civilWorkGallery.find({
                    selector: {
                        $or: item.findNoDate
                    } //[{name:"teste"}]
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            default:
                return {"err": "No possible."}
        }
    }

    //return by search
    getFind(item) {
        switch (item.type) {
            case "civilWork":
                return this.civilWork.find({
                    selector: item.find //{name:"teste"}
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkDay":
                return this.civilWorkDay.find({
                    selector: item.find //{name:"teste"}
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkGallery":
                return this.civilWorkGallery.find({
                    selector: item.find //{name:"teste"}
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkOrder":
                return this.civilWorkOrder.find({
                    selector: item.find //{name:"teste"}
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "civilWorkOrderItems":
                return this.civilWorkOrderItems.find({
                    selector: item.find //{name:"teste"}
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            case "attchments":
                return this.attchments.find({
                    selector: item.find //{name:"teste"}
                }).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error find docs"}
                });
            default:
                return {"err": "Error get all docs"}
        }
    }

    get(item) {
        switch (item.type) {
            case "civilWork":
                return this.civilWork.get(item.obj, {attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "civilWorkDay":
                return this.civilWorkDay.get(item.obj, {attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "civilWorkGallery":
                return this.civilWorkGallery.get(item.obj, {attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "civilWorkOrder":
                return this.civilWorkOrder.get(item.obj, {attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            case "civilWorkOrderItems":
                return this.civilWorkOrder.get(item.obj, {attachments: true}).then(function (result) {
                    return result;
                }).catch(function (err) {
                    return {"err": "Error get all docs"}
                });
            default:
                return {"err": "Error get all docs"}
        }

    }

    //Update/Insert specific object
    update(item) {
        switch (item.type) {
            case "civilWork":
                return this.civilWork.put(item.obj);
            case "civilWorkDay":
                return this.civilWorkDay.put(item.obj);
            case "civilWorkGallery":
                return this.civilWorkGallery.put(item.obj);
            case "civilWorkOrder":
                return this.civilWorkOrder.put(item.obj);
            case "civilWorkOrderItems":
                return this.civilWorkOrder.put(item.obj);
            case "attchments":
                return this.attchments.put(item.obj);
            default:
                return {"err": "Error update docs"}
        }
    }

    //Postspecific object
    insert(item) {
        switch (item.type) {
            case "civilWork":
                if (!item.obj._id) {
                    return this.civilWork.post(item.obj);
                } else {
                    return this.civilWork.put(item.obj);
                }
            case "civilWorkDay":
                if (!item.obj._id) {
                    return this.civilWorkDay.post(item.obj);
                } else {
                    return this.civilWorkDay.put(item.obj);
                }
            case "civilWorkGallery":
                if (!item.obj._id) {
                    return this.civilWorkGallery.post(item.obj);
                } else {
                    return this.civilWorkGallery.put(item.obj);
                }
            case "civilWorkOrder":
                if (!item.obj._id) {
                    return this.civilWorkOrder.post(item.obj);
                } else {
                    return this.civilWorkOrder.put(item.obj);
                }
            case "civilWorkOrderItems":
                if (!item.obj._id) {
                    return this.civilWorkOrderItems.post(item.obj);
                } else {
                    return this.civilWorkOrderItems.put(item.obj);
                }
            default:
                return {"err": "Error get all docs"}
        }
    }

    getFile(id_anexo) {
        this.attchments.getAttachment(id_anexo).catch(function (err) {
            return {"err": "Error insert file", "msg": err}
        });
    }

    updateFile(item) {
        this.attchments.put({
            _id: item.obj.id_anexo,
            _attachments: {
                filename: {
                    type: item.obj.file.type,
                    data: item.obj.file
                }
            }
        }).catch(function (err) {
            return {"err": "Error update file", "msg": err}
        });
    }

    //If necessary delete specific object
    delete(item) {
        switch (item.type) {
            case "civilWork":
                return this.civilWork.remove(item.obj);
            case "attchments":
                return this.attchments.remove(item.obj);
            case "civilWorkDay":
                return this.civilWorkDay.remove(item.obj);
            case "civilWorkGallery":
                return this.civilWorkGallery.remove(item.obj);
            case "civilWorkOrder":
                return this.civilWorkOrder.remove(item.obj);
            case "civilWorkOrderItems":
                return this.civilWorkOrderItems.remove(item.obj);
            default:
                return {"err": "Error get all docs"}
        }
    }

    //Destroy all DB
    remove() {
        new PouchDB('civilWork.db').destroy().then(function () {
            new PouchDB('civilWorkDay.db').destroy().then(function () {
                new PouchDB('civilWorkGallery.db').destroy().then(function () {
                    new PouchDB('civilWorkOrder.db').destroy().then(function () {
                        new PouchDB('attchments.db').destroy().then(function () {
                            new PouchDB('civilWorkOrderItems.db').destroy().then(function () {
                                return true;
                            }).catch(function (err) {
                                return {"err": "Error destroy", "msg": err}
                            })
                        }).catch(function (err) {
                            return {"err": "Error destroy", "msg": err}
                        })
                    }).catch(function (err) {
                        return {"err": "Error destroy", "msg": err}
                    })
                }).catch(function (err) {
                    return {"err": "Error destroy", "msg": err}
                })
            }).catch(function (err) {
                return {"err": "Error destroy", "msg": err}
            })
        }).catch(function (err) {
            return {"err": "Error destroy", "msg": err}
        })
    }

    bulkDoc(item) {

        switch (item.type) {
            case "civilWork":
                return this.civilWork.bulkDocs(item.obj);
            case "civilWorkDay":
                return this.civilWorkDay.bulkDocs(item.obj);
            case "civilWorkGallery":
                return this.civilWorkGallery.bulkDocs(item.obj);
            case "civilWorkOrder":
                return this.civilWorkOrder.bulkDocs(item.obj);
            case "civilWorkOrderItems":
                return this.civilWorkOrderItems.bulkDocs(item.obj);
            case "attchments":
                return this.attchments.bulkDocs(item.obj);
            default:
                return {"err": "Error get all docs"}
        }
    }



}