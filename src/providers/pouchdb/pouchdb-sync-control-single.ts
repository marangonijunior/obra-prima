import {Injectable, Injector} from '@angular/core';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import {CivilWorkDayProvider} from "../civil-work-day/civil-work-day";
import {PouchdbOff} from "./pouchdb-off";
import {RequestHelpersProvider} from "../request-helpers/request-helpers";
import {PouchControll} from "./pouchdb-controll";
import {CivilWorkOrderProvider} from "../civil-work-order/civil-work-order";
import {CivilWorkGalleryProvider} from "../civil-work-gallery/civil-work-gallery";
import {Network} from "@ionic-native/network";
import {Platform} from "ionic-angular";
import * as _ from 'underscore';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import 'rxjs/add/operator/toPromise';
import {share} from "rxjs/operators";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/catch';

@Injectable()
export class PouchdbSyncControlSingle {
    syncActive;
    types = [];
    typeIndex = 0;


    constructor(private injector: Injector,
                private network: Network,
                private platform: Platform,
                private requestHelpersProvider: RequestHelpersProvider,
                public pouchControll: PouchControll) {
        this.setSyncTypes();
    }

    // retorna o campo de id, id_obra, id_solicitacao, etc.
    public getDefaultIdName(type) {
        switch (type) {
            case "civilWork":
                return "id_obra";
            case "civilWorkDay":
                return "id_diaDia";
            case "civilWorkGallery":
                return "id_galeria";
            case "civilWorkOrder":
                return "id_solicitacao";
            case "attchments":
                return "id_anexo";
            case "civilWorkOrderItems":
                return "id_item";
            default:
                return "id_obra";
        }
    }

    setSyncTypes() {
        var self = this;
        this.types = [
            {
                type: "civilWorkDay",
                selector: {syncDate: false},
                afterSave: (object, dataToSync?) => {


                    return new Promise((resolve, reject) => {
                        let item = {
                            type: "attchments",
                            find: {id_diaDia: object._id}
                        };
                        return this.pouchControll.pouchdbOff.getFind(item).then((data: any) => {
                            // console.log("Work Order getDataFromLocalDatabase ### ", data)

                            let files = data.docs.map(file => {
                                file.id_diaDia = object.id_diaDia;
                                return file;
                            });

                            return this.pouchControll.bulkDoc({type: "attchments", obj: files}).then((data) => {
                                resolve([object, dataToSync]);
                            })

                        }).catch((error) => {
                            reject();
                        })

                    });
                }
            },
            {
                type: "attchments",
                selector: {syncDate: false},
                afterSave: (object, dataToSync?) => {


                    return new Promise((resolve, reject) => {
                        let item = {
                            type: "civilWorkDay",
                            find: {id_diaDia: object.id_diaDia}
                        };
                        return this.pouchControll.pouchdbOff.getFind(item).then((data: any) => {
                            // console.log("Work Order getDataFromLocalDatabase ### ", data)

                            let objects = data.docs.map(doc => {
                                return doc;
                            });

                            if (objects && objects.length) {
                                let diaADia = objects[0];
                                diaADia.anexos = diaADia.anexos + 1;
                                return this.pouchControll.bulkDoc({
                                    type: "civilWorkDay",
                                    obj: [diaADia]
                                }).then((data) => {
                                    resolve([object, dataToSync]);
                                })
                            }


                        }).catch((error) => {
                            reject();
                        })

                    });
                }
            },
            {
                type: "civilWorkOrder",
                selector: {
                    syncDate: false,
                    'situacao.id_situacao': {$ne: 'draftApp'}
                },
                shouldSave: (object) => {
                    return new Promise((resolve, reject) => {
                        resolve(true);
                    }).catch((error) => {
                        return error;
                    })
                },
                beforeSave: (object, dataToSync?) => {
                    return new Promise((resolve, reject) => {
                        resolve([object, dataToSync]);
                    }).catch((error) => {
                        return error;
                    })
                },
                afterSave: (object, dataToSync?) => {


                    return new Promise((resolve, reject) => {
                        let item = {
                            type: "civilWorkOrderItems",
                            find: {id_solicitacao: object._id}
                        };
                        return this.pouchControll.pouchdbOff.getFind(item).then((data: any) => {
                            // console.log("Work Order getDataFromLocalDatabase ### ", data)

                            let files = data.docs.map(file => {
                                file.id_solicitacao = object.id_solicitacao;
                                return file;
                            });

                            return this.pouchControll.bulkDoc({
                                type: "civilWorkOrderItems",
                                obj: files
                            }).then((data) => {
                                resolve([object, dataToSync]);
                            })

                        }).catch((error) => {
                            reject();
                        })

                    });
                }
            },
            {
                type: "civilWorkOrderItems",
                selector: {
                    syncDate: false,
                    draftApp: {'$exists': false}
                }
            },
            {
                type: "civilWorkGallery",
                selector: {syncDate: false},
                beforeSave: (object, dataToSync?) => {
                    if (!object.pastaMae || object.pastaMae.toString().length < 10) {
                        return Promise.resolve([object, dataToSync]);
                    }
                    else {
                        let item = {
                            type: "civilWorkGallery",
                            find: {_id: object.pastaMae}
                        };
                        return this.pouchControll.pouchdbOff.getFind(item).then((data: any) => {
                            // console.log("Work Order getDataFromLocalDatabase ### ", data)

                            let files = data.docs.map(file => {
                                return file;
                            });

                            if (files.length && files[0].id_galeria) {
                                object.pastaMae = files[0].id_galeria;
                            }

                            return [object, dataToSync];

                        }).catch((error) => {
                            return Promise.resolve([object, dataToSync]);
                        })
                    }


                },
                afterSave: (object, dataToSync?) => {


                    return new Promise((resolve, reject) => {
                        let item = {
                            type: "civilWorkGallery",
                            find: {pastaMae: object._id}
                        };
                        return this.pouchControll.pouchdbOff.getFind(item).then((data: any) => {
                            // console.log("Work Order getDataFromLocalDatabase ### ", data)

                            let files = data.docs.map(file => {
                                file.pastaMae = object.id_galeria;
                                return file;
                            });

                            return this.pouchControll.bulkDoc({
                                type: "civilWorkOrderItems",
                                obj: files
                            }).then((data) => {

                                if (dataToSync) {
                                    dataToSync = dataToSync.map((item, index) => {
                                        if (item.pastaMae && item.pastaMae == object._id) {
                                            item.pastaMae = object.id_galeria;
                                        }
                                        return item;
                                    })
                                }

                                resolve([object, dataToSync]);
                            })

                        }).catch((error) => {
                            reject();
                        })

                    });
                }
            }
        ]


    }

    syncNextType() {
        if (this.types.length > this.typeIndex + 1) {
            this.typeIndex = this.typeIndex + 1;
            return this.doSync(this.types[this.typeIndex]);
        }
        else {
            console.log("offlineSync", "finished all sync types");
            this.syncActive = false;
            this.typeIndex = 0;
            return Promise.resolve(null);
        }
    }

    doSync(item) {

        if (this.platform.is("cordova") && this.network.type == 'none') {
            console.log("offlineSync", "no internet, finished syncing");
            this.syncActive = false;
            this.typeIndex = 0;
            return Promise.resolve(null);
        }

        this.syncActive = true;

        return this.pouchControll.pouchdbOff.customFind(item).then((dataToSync) => {
            dataToSync = dataToSync.docs;
            if (dataToSync) {
                dataToSync = dataToSync.reverse();
            }

            if (!dataToSync || dataToSync.length == 0) {
                console.log("offlineSync", "no records to sync " + item.type);
                return this.syncNextType();
            }
            else {

                if (dataToSync[0].data) {
                    console.log("offlineSync", "sorting dataToSync by data");
                    dataToSync = _.sortBy(dataToSync, function (node: any) {
                        var dateTime = new Date(node.data).getTime();
                        return dateTime;
                    });
                }

                console.log("offlineSync", "starting sync");
                console.log("offlineSync", dataToSync);

                var index = 0;
                var onSyncSuccess = (retrievedItem: any) => {

                    if (!retrievedItem) {
                        console.log("offlineSync", "error saving file to api" + item.type);

                        if (dataToSync.length > index + 1) {
                            index = index + 1;
                            return this.sync({
                                type: item.type,
                                obj: dataToSync[index]
                            }, onSyncSuccess).first().toPromise().then((data) => {
                                return onSyncSuccess(data);
                            })
                                .catch((error) => {
                                    console.log("error", error);
                                })
                        }
                        else {
                            return this.syncNextType();
                        }

                    }

                    return this.pouchControll.bulkDoc({
                        type: retrievedItem.type,
                        obj: [retrievedItem.obj]
                    }).then((data) => {


                        if (!data || !data.length) {
                            console.log("offlineSync", "error saving file to localDb after sync " + item.type + " " + item.obj);
                            index = dataToSync.length >= index + 1 ? index + 1 : index;
                            return this.syncNextObject(dataToSync, item, index, onSyncSuccess);
                            //return this.syncNextType();
                        }

                        if (item.afterSave) {
                            return item.afterSave(retrievedItem.obj, dataToSync).then((data) => {
                                dataToSync = data[1];
                                index = dataToSync.length >= index + 1 ? index + 1 : index;
                                return this.syncNextObject(dataToSync, item, index, onSyncSuccess);

                            }).catch((error) => {
                                index = dataToSync.length >= index + 1 ? index + 1 : index;
                                return this.syncNextObject(dataToSync, item, index, onSyncSuccess);
                            })
                        }
                        else {
                            index = dataToSync.length >= index + 1 ? index + 1 : index;
                            return this.syncNextObject(dataToSync, item, index, onSyncSuccess);
                        }

                    }).catch((error) => {
                        // maybe here return syncNextObject;
                        return error;
                    })


                };

                if (item.beforeSave) {
                    return item.beforeSave(dataToSync[index], dataToSync).then((data) => {
                        dataToSync = data[1];
                        return this.sync({
                            type: item.type,
                            obj: data[0]
                        }, onSyncSuccess).first().toPromise().then((data) => {
                            return onSyncSuccess(data);
                        })
                            .catch((error) => {
                                console.log("error", error);
                            })
                    });
                }
                else {
                    return this.sync({
                        type: item.type,
                        obj: dataToSync[index]
                    }, onSyncSuccess).first().toPromise().then((data) => {
                        return onSyncSuccess(data);
                    })
                        .catch((error) => {
                            console.log("error", error);
                            return error;
                        })
                }


            }
        }).catch((error) => {
            console.log(error);
            console.log("offlineSync", "sync error");
            this.syncActive = false;
            return Promise.resolve(null);
        })
    }


    syncNextObject(dataToSync, item, index, onSyncSuccess: (item: any) => void) {
        if (dataToSync.length > index) {

            if (item.beforeSave) {
                return item.beforeSave(dataToSync[index], dataToSync).then((data) => {
                    dataToSync = data[1];
                    return this.sync({
                        type: item.type,
                        obj: data[0]
                    }, onSyncSuccess).first().toPromise().then((data) => {
                        return onSyncSuccess(data);
                    });
                });
            }
            else {
                console.log("offlineSync", "success syncing single item from " + item.type);
                return this.sync({
                    type: item.type,
                    obj: dataToSync[index]
                }, onSyncSuccess).first().toPromise().then((data) => {
                    return onSyncSuccess(data);
                });
            }


        }
        else {
            return this.syncNextType();
        }
    }

    sync(item: {
        type: any,
        obj: any
    }, onSyncSuccess: (item: any) => void) {


        // var shouldSync = item.shouldSave ? item.shouldSave(obj)

        // var request = this.getRequestForType(item).pipe(share());

        return this.getRequestForType(item).switchMap((data: any) => {

            if (this.requestHelpersProvider.successAndHasObject(data)) {
                console.log(data);
                data.object._id = item.obj._id;
                data.object._rev = item.obj._rev;
                data.object.syncDate = data.object.data ? data.object.data : true;
                item.obj = data.object;
                item.obj.localEditing = true;

                //onSyncSuccess(item);
                return Observable.of(item).catch((error) => {
                    // handle e and return a safe value or rethrow
                    // if (isCritical(e)) {
                    //     return Observable.throw(e);
                    // }

                    return Observable.of<any>(null);
                });
            }
            else {
                // onSyncSuccess(null);
                return Observable.of(null);
            }

        }).catch((error) => {
            // handle e and return a safe value or rethrow
            // if (isCritical(e)) {
            //     return Observable.throw(e);
            // }

            console.log("offlineSync http error", error);
            this.syncActive = false;
            this.typeIndex = 0;

            return Observable.of<any>(null);
        });
    }

    getRequestForType(item: {
        obj: any,
        type: string
    }): Observable<any> {

        var requests = {
            civilWorkDay: {
                getProvider: () => {
                    return this.injector.get(CivilWorkDayProvider);
                },
                POST: () => {
                    var provider = requests.civilWorkDay.getProvider();
                    return provider.add(item.obj, false);
                },
                PUT: () => {
                    var provider = requests.civilWorkDay.getProvider();
                    return provider.edit(item.obj, false);
                },
                DELETE: () => {
                    var provider = requests.civilWorkDay.getProvider();
                    return provider.remove(item.obj, false);
                }
            },
            civilWorkOrder: {
                getProvider: () => {
                    return this.injector.get(CivilWorkOrderProvider);
                },
                POST: () => {
                    var provider = requests.civilWorkOrder.getProvider();
                    return provider.add(item.obj, false);
                },
                PUT: () => {
                    var provider = requests.civilWorkOrder.getProvider();
                    return provider.edit(item.obj, false);
                },
                DELETE: () => {
                    var provider = requests.civilWorkOrder.getProvider();
                    return provider.remove(item.obj, false);
                }
            },
            civilWorkGallery: {
                getProvider: () => {
                    return this.injector.get(CivilWorkGalleryProvider);
                },
                POST: () => {
                    var provider = requests.civilWorkGallery.getProvider();
                    return provider.add(item.obj, false);
                },
                PUT: () => {
                    var provider = requests.civilWorkGallery.getProvider();
                    return provider.edit(item.obj, false);
                },
                DELETE: () => {
                    var provider = requests.civilWorkGallery.getProvider();
                    return provider.remove(item.obj, false);
                }
            },
            attchments: {
                getProvider: () => {
                    return this.injector.get(CivilWorkDayProvider);
                },
                POST: () => {
                    var provider = requests.attchments.getProvider();
                    return provider.addAttachment(item.obj, false);
                },
                PUT: () => {
                    var provider = requests.attchments.getProvider();
                    return provider.editAttachment(item.obj, false);
                },
                DELETE: () => {
                    var provider = requests.attchments.getProvider();
                    return provider.deleteAttachment(item.obj, false);
                }
            },
            civilWorkOrderItems: {
                getProvider: () => {
                    return this.injector.get(CivilWorkOrderProvider);
                },
                POST: () => {
                    var provider = requests.civilWorkOrder.getProvider();
                    return provider.addItem(item.obj, false);
                },
                PUT: () => {
                    var provider = requests.civilWorkOrder.getProvider();
                    return provider.editItem(item.obj, false);
                },
                DELETE: () => {
                    var provider = requests.civilWorkOrder.getProvider();
                    return provider.removeItem(item.obj, false);
                }
            }
        }


        try {
            if (!item.obj.deleted && item.obj[this.getDefaultIdName(item.type)]) {
                return requests[item.type]["PUT"]();
            }
            else if (!item.obj.deleted && !item.obj[this.getDefaultIdName(item.type)]) {
                return requests[item.type]["POST"]();
            }
            else if (item.obj.deleted && item.obj[this.getDefaultIdName(item.type)]) {
                return requests[item.type]["DELETE"]();
            }
            else if (item.obj.deleted && !item.obj[this.getDefaultIdName(item.type)]) {


                var response = {
                    response: true,
                    deletedOff: true,
                    object: item.obj
                }

                return new BehaviorSubject(response);

                // return Observable.create(observer => {
                //
                //     delete item.obj.syncDate;
                //
                //     var response = {
                //         response: true,
                //         object: item.obj
                //     }
                //
                //     observer.next(response);
                //
                // })


            }
            else {
                throw Observable.throw(null);
            }

        }
        catch (error) {
            throw Observable.throw(null);
        }


    }

}