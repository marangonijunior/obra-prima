import {Injectable, Injector} from '@angular/core';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import {Observable} from "rxjs/Observable";
import {CivilWorkDayProvider} from "../civil-work-day/civil-work-day";
import {PouchdbOff} from "./pouchdb-off";


@Injectable()
export class PouchdbSync {
    public pouchdbSync;
    syncTimer;
    syncInterval = 5000;
    syncActive;


    constructor(private injector: Injector,
                private pouchdbOff: PouchdbOff,) {

    }

    createPouchDB() {
        PouchDB.plugin(cordovaSqlitePlugin);
        this.pouchdbSync = new PouchDB('pouchdbSync.db', {adapter: 'cordova-sqlite', auto_compaction: true});
    }


    startSync() {
        console.log("offlineSync", "start sync");
        var self = this;
        this.syncTimer = setInterval(() => {
            self.onSync();
        }, this.syncInterval);
    }

    stopSync() {
        console.log("offlineSync", "stop sync");
        clearInterval(this.syncTimer);
        this.syncActive = false;
    }

    onSync() {
        if (this.syncActive) {
            return;
        }
        this.syncActive = true;

        this.getAll().then((data) => {

            if (!data || data.length == 0) {
                console.log("offlineSync", "no records to sync");
                this.syncActive = false;
                return;
            }
            else {
                console.log("offlineSync", "starting sync");
                console.log("offlineSync", data);

                var index = 0;
                var onSyncSuccess = (item: any) => {


                    this.pouchdbOff.get({type: item.type, obj: item.obj.objectId}).then((data) => {
                        console.log("offlineSync", "found offline object");
                        if (data) {
                            item.obj._id = data._id;
                            item.obj._rev = data._rev;

                            this.pouchdbOff.insert({
                                obj: item,
                                type: "civilWorkDay"
                            }).then((data) => {

                            })

                        }
                    }).catch((error) => {

                    });

                    this.delete(item).then((data) => {
                        console.log("offlineSync", "sync success");

                        if (data.length > index + 1) {
                            index = index + 1;
                            this.sync(data[index].doc, onSyncSuccess);
                        }
                        else {
                            this.syncActive = false;
                        }

                    }).catch((error) => {

                        console.log("offlineSync", "delete error");

                    })


                };

                var item = data[index].doc;
                this.sync(item, onSyncSuccess);
            }
        }).catch((error) => {
            console.log(error);
            console.log("offlineSync", "sync error");
            this.syncActive = false;
        })
    }

    sync(item: {
        crud: string,
        obj: any,
        type: string
    }, onSyncSuccess: (item: any) => void) {

        return this.getRequestForType(item).subscribe((data) => {
            console.log(data);
            //data.object.objectId = item.objectId;
            item.obj = data.object;
            onSyncSuccess(item);
        }, (error) => {
            console.log("offlineSync http error", error);
            this.syncActive = false;
        });

    }

    getRequestForType(item: {
        crud: string,
        obj: any,
        type: string
    }): Observable<any> {

        var requests = {
            civilWorkDay: {
                getProvider: () => {
                    return this.injector.get(CivilWorkDayProvider);
                },
                POST: () => {
                    var provider = requests.civilWorkDay.getProvider();
                    return provider.add(item.obj, false);
                },
                PUT: () => {
                    var provider = requests.civilWorkDay.getProvider();
                    return provider.add(item.obj, false);
                },
                DELETE: () => {
                    var provider = requests.civilWorkDay.getProvider();
                    return provider.add(item.obj, false);
                }
            }
        }


        try {
            return requests[item.type][item.crud]();
        }
        catch (error) {
            throw Observable.throw(null);
        }


    }


    getAll() {
        return this.pouchdbSync.allDocs({
            include_docs: true,
            attachments: true
        }).then(function (result) {
            return result.rows;
        }).catch(function (err) {
            return {"err": "Error get all docs"}
        });
    }

    get(item) {
        this.pouchdbSync.get(item.obj, {attachments: true}).then(function (result) {
            return result;
        }).catch(function (err) {
            return {"err": "Error get all docs"}
        });
    }

    //Postspecific object
    insert(item) {
        if (!item.obj.file) {
            return this.pouchdbSync.post(item);
        } else {
            this.pouchdbSync.post(item);
            this.pouchdbSync.put({
                _id: item._id,
                _attachments: {
                    filename: {
                        type: item.obj.file.type,
                        data: item.obj.file
                    }
                }
            }).catch(function (err) {
                return {"err": "Error insert file", "msg": err}
            });

        }
    }

    delete(item) {
        return this.pouchdbSync.remove(item);
    }


}