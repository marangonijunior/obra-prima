import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ToastController} from 'ionic-angular';
import {ApiProvider} from "../api/api";
import {Observable} from "rxjs/Observable";
import moment from "moment-timezone";
import {PouchdbOff} from "./pouchdb-off";
import {PouchdbSync} from "./pouchdb-sync";
import {Storage} from "@ionic/storage";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/mergeMap';
import "rxjs/add/observable/of";
import {RequestHelpersProvider} from "../request-helpers/request-helpers";
import {GenericLocalDataProvider} from "../generic-local-data/generic-local-data";
import {UserLocalProvider} from "../user-local/user-local";  // For RxJs  5.0+

@Injectable()
export class PouchControll extends ApiProvider {


    // usado para salvar o item e passar para a promisse debaixo
    item;

    constructor(public http: HttpClient,
                protected toastController: ToastController,
                private localStorage: Storage,
                protected requestHelpersProvider: RequestHelpersProvider,
                public userLocalProvider: UserLocalProvider,
                protected genericLocalDataProvider: GenericLocalDataProvider,
                public pouchdbOff: PouchdbOff,
                public pouchdbSync: PouchdbSync) {
        super(http);
    }

    showMessage(message, duration?) {
        let toast = this.toastController.create({
            message: message,
            duration: duration ? duration : 2000
        });
        toast.present();
    }

    setSyncDate(url) {
        //let date = new Date().toISOString();  moment().tz("America/Sao_Paulo").format();
        let date = moment().tz("America/Sao_Paulo").subtract(5, 'minutes').format();

        if (this.userLocalProvider.user) {
            this.genericLocalDataProvider.addOrEdit("syncDate_" + this.userLocalProvider.user.id_usuario + "_" + url, date);
        }

    }

    getSyncDate(url) {
        if (!this.userLocalProvider.user) {
            return Promise.resolve(null);
        }

        return this.genericLocalDataProvider.getByKey("syncDate_" + this.userLocalProvider.user.id_usuario + "_" + url).then((objects) => {
            return objects && objects.length && objects[0].data ? objects[0].data : null;
        })
    }

    // retorna o campo de id, id_obra, id_solicitacao, etc.
    public getDefaultIdName(type) {
        switch (type) {
            case "civilWork":
                return "id_obra";
            case "civilWorkDay":
                return "id_diaDia";
            case "civilWorkGallery":
                return "id_galeria";
            case "civilWorkOrder":
                return "id_solicitacao";
            case "attchments":
                return "id_anexo";
            case "civilWorkOrderItems":
                return "id_item";
            default:
                return "id_obra";
        }
    }

    getGalleriesRecursive(objects) {

        var allGalleries = [];

        objects.forEach((item, index) => {
            allGalleries.push(item);
            if (item.galeriasFilhas) {
                allGalleries = allGalleries.concat(this.getGalleriesRecursive(item.galeriasFilhas));
            }
        })

        return allGalleries;
    }

    syncAll(type, url, id_empresa, showLoading) {
        // console.log("syncAll " + url);
        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'empresa');
        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        var $this = this;

        return $this.http.get(url + id_empresa, requestOptions).map((resp: any) => {
            if (this.requestHelpersProvider.successAndHasObject(resp)) {


                if (type == "civilWorkGallery") {
                    resp.objects.forEach((item, index) => {
                        resp.objects[index].root = true;
                    });

                    resp.objects = $this.getGalleriesRecursive(resp.objects);
                }

                let item = {type: type, obj: resp.objects}
                // console.log(item);
                return item;
            }
            else {
                return null;
            }

        }, err => {
            console.log("pouchdboff item fetch from api null");
            return Observable.throw(err);

            //    reject(error);
        }).mergeMap((item) => {
            if (!item) {
                return Observable.of(false);
            }
            console.log("mergeMap", item)
            return Observable.fromPromise($this.bulkDoc(item).then(resp => {
                // console.log("pouchdboff true");
                return item;
                //resolve(true)
            }, err => {
                // console.log("pouchdboff error");
                return null;
                //reject(err);
            }));
        })

            .toPromise();

    }

    insert(item: {
        obj: any,
        type: string
    }) {
        // let item = {
        //     crud: "post",
        //     obj: obj,
        //     type: bd
        // };

        if (!Array.isArray(item.obj)) {
            item.obj.syncDate = false;
            if (item.obj._id) {
                item.obj.localEditing = true;
            }
        }
        else {
            item.obj = item.obj.map((obj, index) => {
                obj.syncDate = false;
                if (obj._id) {
                    obj.localEditing = true;
                }
                return obj;
            });
        }


        return this.bulkDoc({type: item.type, obj: Array.isArray(item.obj) ? item.obj : [item.obj]}).then((data) => {
            return data;
        })

        // var changeOnLocalDb = item.crud != "DELETE" ? this.pouchdbOff.insert(item) : this.pouchdbOff.delete(item);
        //
        // return changeOnLocalDb.then(resp_a => {
        //     console.log("changeOnLocalDb", resp_a);
        //     item.objectId = resp_a.id;
        //     return this.pouchdbSync.insert(item).then(resp_b => {
        //         return true;
        //     });
        // }).catch((error) => {
        //
        // });
    }

    update(bd, url, obj) {
        let item = {
            crud: "put",
            obj: obj,
            url: url,
            type: bd
        };
        return new Promise(function (resolve, reject) {
            this.pouchdbOff.update(item).subscribe(
                resp => {
                    this.pouchdbSync.insert(item).subscribe(
                        resp_ => {
                            resolve(true);
                        }, err => {
                            reject(err);
                        }
                    )
                }, err => {
                    reject(err);
                }
            );
        });
    }

    find(bd, find) {
        let item = {
            type: bd,
            find: find
        };
        return new Promise(function (resolve, reject) {
            this.pouchdbOff.getFind(item).subscribe(
                resp => {
                    resolve(resp);
                }, err => {
                    reject(err);
                }
            );
        });
    }

    findFile(id) {
        return new Promise(function (resolve, reject) {
            this.pouchdbOff.getFile(id).subscribe(
                resp => {
                    resolve(resp);
                }, err => {
                    reject(err);
                }
            );
        });
    }

    setFile(url, obj) {
        let item = {
            url: url,
            obj: obj
        };
        return new Promise(function (resolve, reject) {
            this.pouchdbOff.updateFile(item).subscribe(
                resp => {
                    resolve(resp);
                }, err => {
                    reject(err);
                }
            );
        });
    }

    get(bd, obj) {
        let item = {
            obj: obj,
            type: bd
        };
        return new Promise(function (resolve, reject) {
            this.pouchdbOff.get(item).subscribe(
                resp => {
                    resolve(resp);
                }, err => {
                    reject(err);
                }
            );
        });
    }

    getAll(bd) {
        return new Promise(function (resolve, reject) {
            this.pouchdbOff.getAll(bd).subscribe(
                resp => {
                    resolve(resp);
                }, err => {
                    reject(err);
                }
            );
        });
    }

    remove(bd, url, obj) {
        let item = {
            crud: "delete",
            obj: obj,
            url: url,
            type: bd
        };
        return new Promise(function (resolve, reject) {
            this.pouchdbOff.delete(item).subscribe(
                resp => {
                    this.pouchdbSync.insert(item).subscribe(
                        resp_b => {
                            resolve(true);
                        }, err => {
                            reject(err);
                        }
                    )
                }, err => {
                    reject(err);
                }
            )
        });

    }


    bulkDoc(item) {

        console.log(item);

        return this.pouchdbOff.getAll(item.type).then((data) => {

            if (!data || data.err) {
                return [];
            }

            item.obj.map((objectFromItem, index) => {
                if (!objectFromItem.data) {
                    objectFromItem.data = moment().tz("America/Sao_Paulo").format();
                }
                if (objectFromItem.dt_alteracao && moment(objectFromItem.dt_alteracao).isBefore(moment(objectFromItem.data))) {
                    objectFromItem.dt_alteracao = objectFromItem.data;
                }

                return objectFromItem;
            });

            let objectsFromDb = data.rows.map(row => {
                return row.doc;
            });

            item.obj = item.obj.map((objectFromItem, index) => {

                var campoDeId = this.getDefaultIdName(item.type);

                if (objectFromItem[campoDeId] && !objectFromItem._id) {
                    objectFromItem._id = objectFromItem[campoDeId].toString();
                }
                if (!objectFromItem.localEditing) {
                    if (objectFromItem[campoDeId]) {
                        objectsFromDb.forEach((objectFromDb, index) => {

                            if (objectFromDb[campoDeId] == objectFromItem[campoDeId]) {

                                let newObjectDateIsAfterBdObjectDate = moment(objectFromItem.dt_alteracao ? objectFromItem.dt_alteracao : objectFromItem.data)
                                    .isAfter(objectFromDb.dt_alteracao ? objectFromDb.dt_alteracao : objectFromDb.data);

                                if (objectFromDb.syncDate === false && !newObjectDateIsAfterBdObjectDate) {
                                    objectFromItem = objectFromDb;
                                }
                                else {
                                    objectFromItem._id = objectFromDb._id.toString();
                                    objectFromItem._rev = objectFromDb._rev;
                                    if (objectFromDb.syncDate) {
                                        objectFromItem.syncDate = objectFromDb.syncDate;
                                    }
                                }

                            }
                        });
                    }
                }

                delete objectFromItem.localEditing;


                return objectFromItem;
            });

            return this.pouchdbOff.bulkDoc(item);

        }).catch((error) => {
            console.log(error);
        })


    }

    // private setObjectIds(item): any {
    //     var campoDeId = this.getDefaultIdName(item.type);
    //     if (item.obj && Array.isArray(item.obj)) {
    //         item.obj = item.obj.map((singleObj) => {
    //             if (!singleObj._id) {
    //                 singleObj._id = singleObj[campoDeId];
    //             }
    //             return singleObj;
    //         });
    //
    //         console.log("set id for " + item.type + " array");
    //         return item;
    //     }
    //     else if (item.obj && !Array.isArray(item.obj)) {
    //         item.obj._id = item.obj._id ? item.obj._id : item.obj[campoDeId];
    //         console.log("set id for " + item.type + " single obj");
    //         return item;
    //     }
    //     else {
    //         console.log("set id failed for " + item.type);
    //         return item;
    //     }
    //
    // }


}
