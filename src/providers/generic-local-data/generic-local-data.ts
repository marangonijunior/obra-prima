import {HttpClient} from '@angular/common/http';
import {Injectable, Injector} from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import {Storage} from "@ionic/storage";
import {UserLocalProvider} from "../user-local/user-local";
import * as _ from 'underscore';
import {CivilWorkProvider} from "../civil-work/civil-work";

declare var emit;

/*
  Generated class for the GenericLocalDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GenericLocalDataProvider {

    db;

    constructor(

        private userLocalProvider: UserLocalProvider,
        public injector: Injector,
        private storage: Storage
    ) {
        PouchDB.plugin(cordovaSqlitePlugin);
        PouchDB.plugin(PouchDBFind);
        this.db = new PouchDB('genericdata.db',
            {adapter: 'cordova-sqlite', auto_compaction: true});
    }

    getAll() {
        return this.db.allDocs({include_docs: true})
            .then(docs => {

                let objects = docs.rows.map(row => {
                    return row.doc;
                });

                return objects;
            });
    }

    add(type, data) {
        var object = this.formatAsGenericObject(type, data);
        console.log("add add add", object);
        return this.db.post(object).catch((error) => {
            console.log("add error", error);
        });
    }

    edit(object) {
        console.log("edit edit edit", object);
        return this.db.bulkDocs([object]).catch((error) => {
            console.log("edit error", error);
        });
    }

    addOrEdit(type, data) {
        console.log("adding type", type);
        console.log("adding type object", data);
        return this.getByKey(type).then((objects) => {
            console.log(objects);
            if (!objects || objects.length == 0) {
                return this.add(type, data);
            }
            else {
                var object = this.formatAsGenericObject(type, data);
                object._rev = objects[0]._rev;
                object._id = objects[0]._id;
                return this.edit(object);
            }
        });

    }

    addOrEditPush(type, data, replace?) {
        console.log("adding type", type);
        console.log("adding type object", data);
        return this.getByKey(type).then((objects) => {
            console.log(objects);
            if (!objects || objects.length == 0) {
                return this.add(type, data);
            }
            else {
                if (objects[0].data) {
                    data = !replace ? objects[0].data.concat(data) : data;
                }

                var object = this.formatAsGenericObject(type, data);
                object._rev = objects[0]._rev;
                object._id = objects[0]._id;
                return this.edit(object);
            }
        });

    }


    getByKey(type) {


        return this.db.find({
            selector: {genericType: type}
        }).then(function (result) {
            return result && result.docs ? result.docs : [];
        }).catch(function (err) {
            return [];
        });
        //
        // return this.db.query(function (doc) {
        //     emit(doc.genericType)
        // }, {startkey: type, include_docs: true}).then(docs => {
        //
        //     let objects = docs.rows.map(row => {
        //         return row.doc;
        //     });
        //
        //     return objects;
        // });
    }

    formatAsGenericObject(type, data): any {
        return {
            _id: type,
            genericType: type,
            data: data
        };
    }


    // SAVED DATA

    getSituacoes() {
        return this.getByKey("situacoesOrder").then((objects) => {
            if (!objects || objects.length == 0) {
                return [];
            }
            else {
                return objects[0].data;

            }
        })
    }


    getFasesByCivilWork(id_obra?) {

        id_obra = id_obra ? id_obra : this.injector.get(CivilWorkProvider).selected.id_obra;
        return this.getByKey("fasesEmpresa_" + this.userLocalProvider.company.id_empresa).then((objects) => {
            if (!objects || objects.length == 0) {
                return [];
            }
            else {
                return _.where(objects[0].data, {id_obra: id_obra});

            }
        })
    }

    getServicosByFase(id_fase, id_obra?) {

        id_fase = parseInt(id_fase);

        id_obra = id_obra ? id_obra : this.injector.get(CivilWorkProvider).selected.id_obra;
        return this.getByKey("servicosEmpresa_" + this.userLocalProvider.company.id_empresa).then((objects) => {
            if (!objects || objects.length == 0) {
                return [];
            }
            else {
                return _.where(objects[0].data, {id_obra: id_obra, id_fase: id_fase});

            }
        })
    }

}

