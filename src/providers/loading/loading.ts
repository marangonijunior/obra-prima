import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Loading, LoadingController} from "ionic-angular";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

/*
  Generated class for the LoadingProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LoadingProvider {

    loading: Loading;

    isLoading: boolean = false;

    constructor(private loadingCtrl: LoadingController) {

    }

    public dismiss() {
        console.log("dismiss()");
        // var $this = this;
        // $this.isLoading = false;
        try {
            this.loading.dismissAll();
            this.loading = null;
        }
        catch (error) {
            console.log(error);
        }
    }

    public present() {


        if (this.loading) {
            try {
                this.loading.dismiss().then(() => {
                    var $this = this;

                    $this.showLoading();

                });
            }
            catch (error) {

            }

        }
        else {
            this.showLoading();
        }
    }

    private showLoading() {
        try {
            this.loading = this.loadingCtrl.create({
                content: 'Carregando'
                // spinner: 'hide',
                // content: `
                // <div class="custom-spinner-container">
                //   <div class="custom-spinner-box"></div>
                // </div>`
            });
            //
            // this.loading.onDidDismiss(() => {
            //     console.log('Dismissed loading');
            // });

            this.loading.present();
        }
        catch (error) {

        }
    }

}
