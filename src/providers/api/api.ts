import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class ApiProvider {

    daoConfig = {
        isDev: false,
        buildType: "dev", // beta release
        buildTypes: ["dev", "prod", "release"], // beta release
        //devUrl: "http://192.168.0.50/wordpress/wp-json/wp/v2/",
        devUrl: "http://localhost:8100/api/",
        prodUrl: "http://op-des.01tec.com.br/api/",
        releaseUrl: "https://obraprimaweb.com.br/api/",
        getAPIUrl: () => {
            switch (this.daoConfig.buildType) {
                case "dev":
                    return this.daoConfig.devUrl;
                case "prod":
                    return this.daoConfig.prodUrl;
                case "release":
                    return this.daoConfig.releaseUrl;
                default:
                    return this.daoConfig.devUrl;
            }
        }

    };

    constructor(protected http: HttpClient) {
        console.log('Hello ApiProvider Provider');
    }


}
