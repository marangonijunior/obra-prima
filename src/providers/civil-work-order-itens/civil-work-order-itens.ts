import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {UserProvider} from "../user/user";
import {CivilWorkProvider} from "../civil-work/civil-work";


import {PouchControll} from "../pouchdb/pouchdb-controll";
import {Network} from '@ionic-native/network';
import {Observable} from 'rxjs';
import {UserLocalProvider} from "../user-local/user-local";

/*
  Generated class for the CivilWorkOrderItensProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CivilWorkOrderItensProvider extends ApiProvider {

    constructor(public network: Network,
                public pouchControll: PouchControll,
                public http: HttpClient,
                public userProvider: UserProvider,
                public userLocalProvider: UserLocalProvider,
                public civilWorkProvider: CivilWorkProvider) {
        super(http);
    }


    getAll(order, showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'solicitacao');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + order.id_solicitacao, requestOptions);
    }

    add(item, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.post(this.daoConfig.getAPIUrl() + 'solicitacaoItens/', item, requestOptions);
    }

    edit(item, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.put(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + item.id_item, item, requestOptions);
    }

    remove(item, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.delete(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + item.id_item, requestOptions);
    }

    getById(id, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + id, requestOptions);
    }

    getDeleted(listOfIds, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.post(this.daoConfig.getAPIUrl() + 'solicitacaoItensDeleteds/', listOfIds, requestOptions);
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'solicitacaoItensDeleteds/', listOfIds, requestOptions);
        }
    }

}
