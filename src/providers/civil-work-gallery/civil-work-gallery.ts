import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {UserProvider} from "../user/user";
import {CivilWorkProvider} from "../civil-work/civil-work";
import {Observable} from "rxjs/Observable";

import {PouchControll} from "../pouchdb/pouchdb-controll";
import {Network} from '@ionic-native/network';

/*
  Generated class for the CivilWorkGalleryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CivilWorkGalleryProvider extends ApiProvider {

    constructor(public network: Network,
                public pouchControll: PouchControll,
                public http: HttpClient, public userProvider: UserProvider, public civilWorkProvider: CivilWorkProvider) {
        super(http);
    }

    remove(object, showLoading) {

        let headers = new HttpHeaders();

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        headers = headers.set('arquivo', object.arquivo || object.caminhoArquivo ? 'true' : 'false');

        const requestOptions = {
            headers: headers
        };

        return this.http.delete(this.daoConfig.getAPIUrl() + 'galerias/' + object.id_galeria, requestOptions);
    }

    getAllByCivilWork(civilWork, showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'obra');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };


        return this.http.get(this.daoConfig.getAPIUrl() + 'galerias/' + civilWork.id_obra, requestOptions);
    }

    getAllByCompany(company, showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'empresa');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'galerias/' + company.id_empresa, requestOptions);
    }

    getById(id, showLoading?) {
        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'pasta');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'galerias/' + id, requestOptions);
    }

    edit(gallery, showLoading?) {
        let headers = new HttpHeaders();

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }
        headers = headers.set('arquivo', gallery.arquivo || gallery.caminhoArquivo ? 'true' : 'false');

        const requestOptions = {
            headers: headers
        };


        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            //  throw Observable.throw(null);
            return this.http.put(this.daoConfig.getAPIUrl() + 'galerias/' + gallery.id_galeria, gallery, requestOptions);
            // this.pouchControll.update("civilWorkGallery",this.daoConfig.getAPIUrl()+'galerias/'+ gallery.id_galeria,gallery).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.put(this.daoConfig.getAPIUrl() + 'galerias/' + gallery.id_galeria, gallery, requestOptions);
        }
    }

    add(gallery, showLoading?) {
        let headers = new HttpHeaders();

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }
        headers = headers.set('arquivo', gallery.arquivo ? 'true' : 'false');

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            //  throw Observable.throw(null);
            return this.http.post(this.daoConfig.getAPIUrl() + 'galerias/', gallery, requestOptions);
            // this.pouchControll.insert("civilWorkGallery",this.daoConfig.getAPIUrl()+'galerias/',gallery).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'galerias/', gallery, requestOptions);
        }
    }

    getDeleted(listOfIds, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.post(this.daoConfig.getAPIUrl() + 'galeriasDeleteds/', listOfIds, requestOptions);
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'galeriasDeleteds/', listOfIds, requestOptions);
        }
    }

}
