import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {UserProvider} from "../user/user";
import {CivilWorkProvider} from "../civil-work/civil-work";


import {Network} from '@ionic-native/network';
import {Observable} from 'rxjs';
import {UserLocalProvider} from "../user-local/user-local";

/*
  Generated class for the CivilWorkOrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CivilWorkOrderProvider extends ApiProvider {

    constructor(public network: Network,
                public http: HttpClient,
                public userProvider: UserProvider,
                public userLocalProvider: UserLocalProvider,
                public civilWorkProvider: CivilWorkProvider) {
        super(http);
    }

    getAllByCivilWork(civilWork, showLoading) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'obra');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };


        return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacoes/' + civilWork.id_obra, requestOptions);
    }

    getAllByCompany(company, showLoading) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'empresa');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacoes/' + company.id_empresa, requestOptions);
    }

    getItems(order, showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'solicitacao');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + order.id_solicitacao, requestOptions);
        } else {
            return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + order.id_solicitacao, requestOptions);
        }
    }

    getById(id, showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'solicitacao');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacoes/' + id, requestOptions);
    }

    edit(order, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };



        order.id_obra = this.civilWorkProvider.selected.id_obra;
        order.id_usuario = this.userLocalProvider.user.id_usuario;
        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            //   throw Observable.throw(null);
            return this.http.put(this.daoConfig.getAPIUrl() + 'solicitacoes/' + order.id_solicitacao, order, requestOptions);
            // this.pouchControll.update("civilWorkOrder",this.daoConfig.getAPIUrl()+'solicitacoes/'+ order.id_solicitacao,order).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.put(this.daoConfig.getAPIUrl() + 'solicitacoes/' + order.id_solicitacao, order, requestOptions);
        }
    }

    add(order, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        order.id_obra = this.civilWorkProvider.selected.id_obra;
        order.id_usuario = this.userLocalProvider.user.id_usuario;
        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            //  throw Observable.throw(null);
            return this.http.post(this.daoConfig.getAPIUrl() + 'solicitacoes/', order, requestOptions);
            // this.pouchControll.insert("civilWorkOrder",this.daoConfig.getAPIUrl()+'solicitacoes/',order).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'solicitacoes/', order, requestOptions);
        }
    }

    addItem(item, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.post(this.daoConfig.getAPIUrl() + 'solicitacaoItens/', item, requestOptions);
    }

    editItem(item, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.put(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + item.id_item, item, requestOptions);
    }

    removeItem(item, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.delete(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + item.id_item, requestOptions);
            // this.pouchControll.remove("civilWorkOrderItems",this.daoConfig.getAPIUrl()+'solicitacaoItens/' + item.id_item, item).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.delete(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + item.id_item, requestOptions);
        }
    }

    getItemById(id, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacaoItens/' + id, requestOptions);
    }

    remove(order, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
           // throw Observable.throw(null);
            return this.http.delete(this.daoConfig.getAPIUrl() + 'solicitacoes/' + order.id_solicitacao, requestOptions);
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.delete(this.daoConfig.getAPIUrl() + 'solicitacoes/' + order.id_solicitacao, requestOptions);
        }
    }

    getSituacoes(showLoading?) {

        let headers = new HttpHeaders();
       // headers = headers.set('tipo', 'obra');

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };
        return this.http.get(this.daoConfig.getAPIUrl() + 'solicitacaoSituacoes/', requestOptions);

    }

    getDeleted(listOfIds, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.post(this.daoConfig.getAPIUrl() + 'solicitacoesDeleteds/', listOfIds, requestOptions);
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'solicitacoesDeleteds/', listOfIds, requestOptions);
        }
    }

}
