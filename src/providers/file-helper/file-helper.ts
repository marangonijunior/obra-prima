import
{
    HttpClient
}
    from
        '@angular/common/http';
import
{
    Injectable
}
    from
        '@angular/core';
import
{
    FileChooser
}
    from
        "@ionic-native/file-chooser";
import
{
    FilePath
}
    from
        "@ionic-native/file-path";
import
{
    File
}
    from
        "@ionic-native/file";
import
{
    LoadingController
}
    from
        "ionic-angular";
import
{
    Camera, CameraOptions
}
    from
        "@ionic-native/camera";
import
{
    IOSFilePicker
}
    from
        '@ionic-native/file-picker';
import
{
    Platform
}
    from
        'ionic-angular';

/*
 Generated class for the FileHelperProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@
    Injectable()
export class FileHelperProvider {

    loading;
    currentFile: any;

    constructor(private file: File, public camera: Camera, public platform: Platform, private filePath: FilePath, private filePicker: IOSFilePicker, private fileChooser: FileChooser, private loadingCtrl: LoadingController) {
        console.log('Hello FileHelperProvider Provider');
    }

    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: 'Carregando'
            // spinner: 'hide',
            // content: `
            // <div class="custom-spinner-container">
            //   <div class="custom-spinner-box"></div>
            // </div>`
        });

        this.loading.present();
    }

    dismissLoading() {
        console.log("dismiss()");
        // var $this = this;
        // $this.isLoading = false;
        try {
            this.loading.dismissAll();
            this.loading = undefined;
        }
        catch (error) {
            console.log(error);
        }
    }


    private getFileTitleFromUri(uri) {
        uri = encodeURI(uri);

        return this.filePath.resolveNativePath(uri)
            .then(filePath => {

                let fileName = filePath.substr(filePath.lastIndexOf('/') + 1);
                console.log(fileName);
                console.log(uri);
                return fileName;
            }).catch((error) => {
            console.log("getFileTitleFromUri", error);
                throw new Error("unable to get file as base64");
            })

    }

    loadImage(selectedSourceType: number) {


        if (selectedSourceType == this.camera.MediaType.PICTURE) {

            window["imagePicker"].getPictures(
                function(results) {
                    for (var i = 0; i < results.length; i++) {
                        console.log('Image URI: ' + results[i]);
                    }
                }, function (error) {
                    console.log('Error: ' + error);
                }
            );

            return;
        }

        let cameraOptions: CameraOptions = {
            sourceType: selectedSourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            targetHeight: 800,
            targetWidth: 800,
            quality: 100,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
        }
        return this.camera.getPicture(cameraOptions).then((uri) => {

            if (this.platform.is("ios")) {

                let filePath = uri;
                let title = filePath.substr(filePath.lastIndexOf('/') + 1);
                let path = filePath.substring(0, filePath.lastIndexOf('/'));

                console.log(filePath);
                console.log(path);
                console.log(title);

                this.currentFile = {nome: title, type: "image"};
                return this.file.readAsDataURL(path, title).then((data) => {

                    console.log("FILE LOADED");
                    this.dismissLoading();
                    if (data) {
                        this.currentFile.arquivo = data.split(',')[1];
                        return this.currentFile;
                    }
                    else {
                        throw new Error("unable to get file as base64");
                    }

                }, (error) => {
                    console.log("readAsDataURL error", error);
                    return error;
                })
            }
            else {

                return this.getFileTitleFromUri(uri).then((fileName) => {
                    this.currentFile = {nome: fileName, type: "image"};
                    this.showLoading();
                    return this.getFileAsBase64FromUri(uri).then((data) => {
                        this.dismissLoading();
                        if (data) {
                            this.currentFile.arquivo = data.split(',')[1];
                            this.currentFile.img = data;
                            return this.currentFile;
                        }
    
                    });
                });

            }


        


        }).catch((err) => {
            console.log("error", err);
            throw err;
        });
    }

    private isValidFileType(fileTitle) {
        var fileExt = fileTitle.split('.').pop();

        let validTypes: Array<string> = ['bmp', "dib", "doc", "docx", "dwg", "gif", "jpg", "jpeg", "odt", "ott",
            "ods", "ots", "odp", "otp", "pdf", "png", "pps", "ppsx", "ppt", "psd", "rar", "rtf", "tif", "tiff",
            "txt", "xls", "xlsx", "zip", "mp4", "flv", "avi", "mov", "mpeg", "mkv", "avchd", "wmv", "mp3", "aac", "ogg", "wma", "flac", "alac"];

        return validTypes.indexOf(fileExt) != -1;
    }

    private getFileAsBase64FromUri(uri) {
        uri = decodeURIComponent(uri);

        return this.filePath.resolveNativePath(uri)
            .then(filePath => {
                let title = filePath.substr(filePath.lastIndexOf('/') + 1).replace("%20", " ");
                let path = filePath.substring(0, filePath.lastIndexOf('/'));
                console.log(filePath);
                console.log(path);
                console.log(title);
                return this.file.readAsDataURL(path, title).then((data) => {
                    console.log("readAsDataURL");
                    return data;
                }, (error) => {
                    console.log(error);
                    throw error;
                })
            });
    }


    loadFileIos() {
        return this.filePicker.pickFile()
            .then(uri => {
                console.log(uri);

                let filePath = uri;


                // let title = filePath.substr(filePath.lastIndexOf('/') + 1).replace("%20", " ");
                let title = filePath.substr(filePath.lastIndexOf('/') + 1);
                //let path = filePath.substring(0, filePath.lastIndexOf('/'));
                let path = this.file.tempDirectory + "com.tec01.obraprima-Inbox/";
                console.log(path);
                console.log(title);


                if (!this.isValidFileType(title)) {
                    throw new Error("Tipo de arquivo inválido");
                }


//                       /private/var/mobile/Containers/Data/Application/5C061AF3-B8C9-4A93-BA6B-4E49CC7715D2/tmp/com.tec01.obraprima-Inbox/2018.03.10 - Atestado de Capacidade Técnica - 01Tecnologia - Assinado.pdf
//                       cordova.js:1732

//                       return this.file.resolveLocalFilesystemUrl(uri).then((entry) => {
//
//                               console.log("entry", entry);
//                               console.log("entry.toURL()", entry.toURL());
//                               console.log("entry.fullPath", entry.fullPath);
//                               //console.log("entry.toInternalURL()", entry.toInternalURL());
//                             //  var fileEntry = entry as FileEntry;
////                               var fileEntry = entry as FileEntry;
////                               fileEntry.file(success => {
////                                   var mimeType = success.type;
////                               }, error => {
////                                   // no mime type found;
////                               });
//
//                       }).catch((error) => {
//                               console.log("resolveLocalFilesystemUrl", error);
//
//                               throw error;
//                        })

                this.currentFile = {nome: title, type: "file"};
                return this.file.readAsDataURL(path, title).then((data) => {

                    console.log("FILE LOADED");
                    this.dismissLoading();
                    if (data) {
                        this.currentFile.arquivo = data.split(',')[1];
                        return this.currentFile;
                    }
                    else {
                        throw new Error("unable to get file as base64");
                    }

                }, (error) => {
                    console.log("readAsDataURL error", error);
                    return error;
                })


            })
            .catch(error => {

                console.log("error catch geral", error);

                var $this = this;
                setTimeout(() => {
                    $this.dismissLoading();
                }, 1000);

                throw error;

            });
    }


    loadFileAndroid() {

        return this.fileChooser.open()
            .then(uri => {

                this.showLoading();
                return this.getFileTitleFromUri(uri).then((fileName) => {

                    if (!fileName) {
                        throw new Error("unable to get fileName");
                    }
                    else if (!this.isValidFileType(fileName)) {
                        throw new Error("Tipo de arquivo inválido");
                    }


                    this.currentFile = {nome: fileName, type: "file"};
                    return this.getFileAsBase64FromUri(uri).then((data) => {
                        console.log("FILE LOADED");
                        this.dismissLoading();
                        if (data) {
                            this.currentFile.arquivo = data.split(',')[1];
                            return this.currentFile;
                        }
                        else {
                            throw new Error("unable to get file as base64");
                        }

                    });
                });

            })
            .catch((error) => {
                console.log("error catch geral", error);

                var $this = this;
                setTimeout(() => {
                    $this.dismissLoading();
                }, 1000);

                throw error;
            });

    }


    loadFile() {

        if (this.platform.is("ios")) {
            return this.loadFileIos();
        }
        else {
            return this.loadFileAndroid();
        }


    }


}
