import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {RequestHelpersProvider} from "../request-helpers/request-helpers";
import {GenericLocalDataProvider} from "../generic-local-data/generic-local-data";
import 'rxjs/add/operator/map';

/*
  Generated class for the GeneralProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeneralProvider extends ApiProvider {

    constructor(protected http: HttpClient,
                public genericLocalDataProvider: GenericLocalDataProvider,
                protected requestHelpersProvider: RequestHelpersProvider) {
        super(http);
        console.log('Hello GeneralProvider Provider');
    }

    getSlides(showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'empresa');

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'sliderApp', requestOptions).map((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                this.genericLocalDataProvider.addOrEdit("slides", data.object);

                return data;
            }
            else {
                return data;
            }
        });

    }

    getAbout(showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'empresa');

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'sobreApp', requestOptions).map((data: any) => {
            if (this.requestHelpersProvider.successAndHasObject(data)) {
                this.genericLocalDataProvider.addOrEdit("about", data.object);

                return data;
            }
            else {
                return data;
            }

        });
    }

}
