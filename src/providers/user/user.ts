import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {ApiProvider} from "../api/api";
import {fromPromise} from "rxjs/observable/fromPromise";
import {App, Platform} from "ionic-angular";
import {UserLocalProvider} from "../user-local/user-local";
import {Observable} from "rxjs/Observable";
import {RequestHelpersProvider} from "../request-helpers/request-helpers";
import * as _ from 'underscore';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider extends ApiProvider {

    constructor(protected http: HttpClient,
                protected userLocalProvider: UserLocalProvider,
                protected requestHelpersProvider: RequestHelpersProvider,
                private storage: Storage,
                public platform: Platform) {
        super(http);

    }

    login(user) {
        return this.http.post(this.daoConfig.getAPIUrl() + 'login', user);
    }

    forgetPassword(email : string) {

        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json; charset=utf-8');
        headers = headers.set('noSyncDate', 'true');

        const requestOptions = {
            headers: headers
        };

        email = "'" + email + "'";

        return this.http.post(this.daoConfig.getAPIUrl() + 'esqueciSenha', email, requestOptions);
    }


    getCompanies(user, showLoading?) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'usuario');
        headers = headers.set('noSyncDate', 'true');

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };


        return this.http.get(this.daoConfig.getAPIUrl() + 'empresas/' + user.id_usuario, requestOptions).switchMap((data: any) => {

            if (this.requestHelpersProvider.successAndHasObject(data)) {

                if (this.userLocalProvider.company) {
                    var selectedCompany = _.findWhere(data.objects, {id_empresa: this.userLocalProvider.company.id_empresa});
                    if (selectedCompany) {
                        this.userLocalProvider.setSelectedCompany(selectedCompany);
                    }
                }


            }

            return Observable.of(data);
        })


    }

    getUserDetails(id) {
        return this.http.get(this.daoConfig.getAPIUrl() + 'pessoas/' + id);
    }

}
