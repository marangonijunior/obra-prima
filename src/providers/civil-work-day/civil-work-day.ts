import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiProvider} from "../api/api";
import {UserProvider} from "../user/user";

import {Network} from '@ionic-native/network';
import {identifierName} from '@angular/compiler';
import {Observable} from "rxjs/Observable";
import {PouchControll} from "../pouchdb/pouchdb-controll";

/*
  Generated class for the CivilWorkDayProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CivilWorkDayProvider extends ApiProvider {

    constructor(public network: Network,
                private userProvider: UserProvider,
                public http: HttpClient) {
        super(http);
    }

    getAllByCompany(company, showLoading) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'empresa');

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'diaDias/' + company.id_empresa, requestOptions);

    }


    getAllByCivilWork(civilWork, showLoading) {

        let headers = new HttpHeaders();
        headers = headers.set('tipo', 'obra');

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        return this.http.get(this.daoConfig.getAPIUrl() + 'diaDias/' + civilWork.id_obra, requestOptions);
    }

    getById(id, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };


        return this.http.get(this.daoConfig.getAPIUrl() + 'diaDias/' + id, requestOptions);
    }

    getAttachments(id, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };


        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.get(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + id, requestOptions);
            // this.pouchControll.findFile(id).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.get(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + id, requestOptions);
        }
    }

    addAttachment(file, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        delete file.img;
        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.post(this.daoConfig.getAPIUrl() + 'diaDiaAnexos', file, requestOptions);
            // this.pouchControll.setFile(this.daoConfig.getAPIUrl() + 'diaDiaAnexos',file).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'diaDiaAnexos', file, requestOptions);
        }
    }

    editAttachment(file, showLoading?) {

        let headers = new HttpHeaders();

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };


        delete file.img;
        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.put(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + file.id_anexo, file);
            // this.pouchControll.setFile(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + file.id_anexo,file).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.put(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + file.id_anexo, file);
        }
    }

    deleteAttachment(file, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        delete file.img;
        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
           // throw Observable.throw(null);

            return this.http.delete(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + file.id_anexo, requestOptions);

            // this.pouchControll.remove("attchments",this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + file.id_anexo, file).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.delete(this.daoConfig.getAPIUrl() + 'diaDiaAnexos/' + file.id_anexo, requestOptions);
        }
    }

    add(civilWorkDay, showLoading?) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };


        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            //  throw Observable.throw(null);
            return this.http.post(this.daoConfig.getAPIUrl() + 'diaDias', civilWorkDay, requestOptions);
            // this.pouchControll.insert("civilWorkDay",this.daoConfig.getAPIUrl()+'diaDias',civilWorkDay).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'diaDias', civilWorkDay, requestOptions);
        }
    }


    edit(civilWorkDay, showLoading?) {


        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        delete civilWorkDay.files;
        delete civilWorkDay.anexosArray;
        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.put(this.daoConfig.getAPIUrl() + 'diaDias/' + civilWorkDay.id_diaDia, civilWorkDay, requestOptions);
            // this.pouchControll.update("civilWorkDay",this.daoConfig.getAPIUrl()+'diaDias/'+ civilWorkDay.id_diaDia,civilWorkDay).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.put(this.daoConfig.getAPIUrl() + 'diaDias/' + civilWorkDay.id_diaDia, civilWorkDay, requestOptions);
        }
    }

    remove(civilWorkDay, showLoading) {

        let headers = new HttpHeaders();

        if (showLoading === false) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        delete civilWorkDay.files;
        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
           // throw Observable.throw(null);

            return this.http.delete(this.daoConfig.getAPIUrl() + 'diaDias/' + civilWorkDay.id_diaDia, requestOptions);

            // this.pouchControll.remove("civilWorkDay",this.daoConfig.getAPIUrl()+'diaDias/'+ civilWorkDay.id_diaDia, civilWorkDay).then(
            //   resp =>{
            //     return resp;
            //   }
            // ).catch( err => {
            //   this.pouchControll.showMessage("'Houve um erro, por favor tente novamente.'")
            // });
        } else {
            return this.http.delete(this.daoConfig.getAPIUrl() + 'diaDias/' + civilWorkDay.id_diaDia, requestOptions);
        }
    }

    getDeleted(listOfIds, showLoading) {

        let headers = new HttpHeaders();

        if (!showLoading) {
            headers = headers.set('noLoading', 'true');
        }

        const requestOptions = {
            headers: headers
        };

        if (this.userProvider.platform.is("cordova") && this.network.type == 'none') {
            return this.http.post(this.daoConfig.getAPIUrl() + 'diaDiasDeleteds/', listOfIds, requestOptions);
        } else {
            return this.http.post(this.daoConfig.getAPIUrl() + 'diaDiasDeleteds/', listOfIds, requestOptions);
        }
    }

}
