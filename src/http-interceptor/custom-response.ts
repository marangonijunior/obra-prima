export var loginResponse = {
  success: {
    status: 200,
    body: {
      response: "success",
      object:
        {
          id: 45,
          name:
            "Renato Probst",
          email:
            "renato@rockapps.com.br"
        }
    }
  },

  error: {
    status: 200,
    body: {
      response: "error",
      error: {
        message: "Por favor verifique seus dados e tente novamente"
      }
    }
  }

}


export var civilWorkResponse = {
  success: {
    status: 200,
    body: {
      response: "success",
      objects: [
        {
          id: 1,
          title: "Casa de Campo",
          img: "assets/imgs/civilwork/placeholder.jpg",
          description: "Construção casa",
          status: "Em andamento",
          place: "Curitiba - PR",
          customer: "Renato Probst"
        },
        {
          id: 2,
          title: "Casa de Campo",
          featured: true,
          notifications: 4,
          img: "assets/imgs/civilwork/placeholder.jpg",
          description: "Construção casa",
          status: "Em andamento",
          place: "Curitiba - PR",
          customer: "Renato Probst"
        },
        {
          id: 3,
          title: "Casa de Campo",
          img: "assets/imgs/civilwork/placeholder.jpg",
          description: "Construção casa",
          status: "Em andamento",
          place: "Curitiba - PR",
          customer: "Renato Probst"
        }
      ]

    }
  },

  error: {
    status: 200,
    body: {
      response: "error",
      error: {
        message: "Por favor verifique seus dados e tente novamente"
      }
    }
  }

}
export var companiesResponse = {
  success: {
    status: 200,
    body: {
      "response": true,
      "objects": [
        {
          "idEmpresa": 1,
          "nome": "Matriz",
          "telefone": "41 3039-3939",
          "foto": "https://obraprimaweb.com.br/app/empresa/logo/logo.jpg",
          "matriz": true
        },
        {
          "idEmpresa": 2,
          "nome": "Matriz",
          "telefone": "41 3039-3939",
          "foto": "https://obraprimaweb.com.br/app/empresa/logo/logo.jpg",
          "matriz": true
        },
      ]
    }
  }

}
export var sliderResponse = {
  success: {
    status: 200,
    body: {
      "response": true,
      "objects": [
        {
          "ordem": 1,
          "titulo": "Lorem ipsulum sit amet 2",
          "texto": "Lorem Ipsum",
          "imagem": "assets/imgs/intro/placeholder.png"
        },
        {
          "ordem": 2,
          "titulo": "Lorem ipsulum sit amet 2",
          "texto": "Lorem Ipsum",
          "imagem": "assets/imgs/intro/placeholder.png"
        },
        {
          "ordem": 3,
          "titulo": "Lorem ipsulum sit amet 2",
          "texto": "Lorem Ipsum",
          "imagem": "assets/imgs/intro/placeholder.png"
        }
      ]
    }
  }

}
export var aboutResponse = {
  success: {
    status: 200,
    body: {
      "response": true,
      "object": {
        "ordem": 1,
        "texto": "Lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet lorem ipsulum sit amet",
        "imagem": "https://obraprimaweb.com.br/app/slider/img.jpg",
      }
    }
  }
}
