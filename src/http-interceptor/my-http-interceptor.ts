import {Injectable, Injector} from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/observable/throw'
import moment from "moment-timezone";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import {fromPromise} from "rxjs/observable/fromPromise";
import {LoadingProvider} from "../providers/loading/loading";
import {Events, Nav} from "ionic-angular";
import {UserProvider} from "../providers/user/user";
import {aboutResponse, civilWorkResponse, companiesResponse, loginResponse, sliderResponse} from "./custom-response";
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/materialize';
import 'rxjs/add/operator/dematerialize';
import {UserLocalProvider} from "../providers/user-local/user-local";
import {PouchControll} from "../providers/pouchdb/pouchdb-controll";
import {ApiProvider} from "../providers/api/api";


@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {

    token;

    constructor(public events: Events, private userProvider: UserLocalProvider, private loadingProvider: LoadingProvider,
                private apiProvider: ApiProvider,
                private pouchControll: PouchControll) {


    }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // this.userProvider.getUser().then((user) => {
        //   if (user) {
        //     this.token = user.token;
        //   }
        //   else {
        //
        //   }
        // });

        // return Observable.of(new HttpResponse({ status: 200, body: {} }));

        console.log("intercepted request ... ");

        try {
            return fromPromise(this.userProvider.getUserFromLocalDb())
                .switchMap(user => {
                    return Observable.fromPromise(this.pouchControll.getSyncDate(req.url)).switchMap((lastSyncDate: any) => {



                        var customReturn: Observable<HttpResponse<any>> = null;

                        console.log("this.loadingProvider.present();");


                        var showLoading = !req.headers.has("noLoading");
                        showLoading = showLoading && req.url.indexOf("/api/") != -1;

                        if (showLoading && req.url.indexOf(this.apiProvider.daoConfig.getAPIUrl()) != -1) {
                            this.loadingProvider.present();
                        }

                        var url = req.url;
                        var body = req.body;
                        if (user) {
                            var headers = req.headers.set("token", "TQBEXhcSDHxbonG").set("id_usuario", user.id_usuario.toString());
                        }
                        else {
                            var headers = req.headers.set("token", "TQBEXhcSDHxbonG");
                        }

                        if (lastSyncDate && user && !req.headers.has("noSyncDate")) {
                            console.log("lastSyncDate", lastSyncDate);
                            headers = headers.set("dt_ult_sinc", lastSyncDate);
                        }

                        if (url.indexOf("/api/") != -1) {
                            console.log("lastSyncDate set", url);
                            this.pouchControll.setSyncDate(url);
                        }



                        // if (url.indexOf("/login") !== -1) {
                        //   try {
                        //
                        //
                        //
                        //
                        //     customReturn = Observable.of(body.login == "renato@rockapps.com.br" && body.senha == "123456" ? new HttpResponse(loginResponse.success, headers) :
                        //       new HttpResponse(loginResponse.error));
                        //   }
                        //   catch (error) {
                        //     customReturn = Observable.of(new HttpResponse(loginResponse.error));
                        //   }
                        // }
                        // else if (url.indexOf("obras") !== -1) {
                        //   console.log("civilWorkResponse.success");
                        //   customReturn = Observable.of(new HttpResponse(civilWorkResponse.success));
                        // } else if (url.indexOf("empresasusuario/") !== -1) {
                        //   console.log("companiesResponse.success");
                        //   customReturn = Observable.of(new HttpResponse(companiesResponse.success));
                        // } else if (url.indexOf("sliderApp") !== -1) {
                        //   console.log("sliderResponse.success");
                        //   customReturn = Observable.of(new HttpResponse(sliderResponse.success));
                        // } else if (url.indexOf("sobreApp") !== -1) {
                        //   console.log("aboutResponse.success");
                        //   customReturn = Observable.of(new HttpResponse(aboutResponse.success));
                        // }


                        // return this.whenUrl(req, "user/login", (request : HttpRequest) => {
                        //   return new HttpResponse(loginResponse.error);
                        //
                        // });
                        // }).switchMap((req: HttpResponse) => {
                        //   return new HttpResponse(loginResponse.error);
                        //
                        //   });


                        // return Observable.of(this.generateCustomResponse(req)).delay(2000).do((event) => {
                        //   if (event instanceof HttpResponse) {
                        //
                        //     this.loadingProvider.dismiss();
                        //   }
                        // });

                        var returnObservable: Observable<any> = customReturn ? customReturn : next.handle(req.clone({headers: headers}));


                        return returnObservable.do(event => {
                            console.log("do(event ");
                            console.log(event);
                            if (event instanceof HttpResponse) {
                                console.log("event instanceof HttpResponse");
                                if (showLoading) {
                                    this.loadingProvider.dismiss();
                                }
                            }
                        })
                            .catch((error, caught) => {

                                console.log("http error");
                                console.log(error);

                                this.loadingProvider.dismiss();

                                return Observable.throw(error);
                            }) as any;

                    });
                });
        }
        catch (error) {
            console.log("good error");
            console.log(error);

            this.loadingProvider.dismiss();
        }


//     var headers = req.headers;
//     const authReq = req.clone({headers: headers});
//
//     console.log("Sending request with new header now ...");
//
// //send the newly created request
//     return next.handle(authReq)
//       .catch((error, caught) => {
//         return Observable.throw(error);
//       }) as any;
//   }
    }


    logOff() {
        this.events.publish('user:logOff', {});
    }

    whenUrl(request: HttpResponse<any>, url, callback: (request: HttpResponse<any>) => {}) {
        if (request.url.indexOf(url) != -1) {
            return Observable.of(callback(request));
        }
        else {
            return Observable.of(request);
        }
    }

    // generateCustomResponse(response: HttpResponse) {
    //
    //
    //   console.log("response.url");
    //   console.log(response.url);
    //
    //   var url = response.url.replace(dev_API_URL, "");
    //   var body = response.body;
    //   switch (url) {
    //     case "user/login":
    //       try {
    //         if (body.email == "renato@rockapps.com.br" && body.password == "123456") {
    //           console.log("success response");
    //           return new HttpResponse(loginResponse.success);
    //         }
    //         else {
    //           console.log("error response");
    //           return new HttpResponse(loginResponse.error);
    //         }
    //       }
    //       catch (error) {
    //         return new HttpResponse(loginResponse.error);
    //       }
    //
    //       break;
    //     default:
    //       return response;
    //       break;
    //   }
    // }
}
